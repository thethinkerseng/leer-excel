﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.WebApi.ViewModels
{

    public class TerceroEstadoViewModel
    {
        public int Id { get; set; }
        public string Estado { get; set; }
    }
    public class TerceroViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Tipo de identificación")]
        public string TipoIdentidad { get; set; }

        [Required]
        [Display(Name = "Identificación")]
        public string Identificacion { get; set; }

        public string DigitoVerificacion { get; set; }
        public DateTime? FechaExpedicion { get; set; }
        public string TipoPersona { get; set; }
        public string Tipo { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NombreContacto { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }
        public string Movil1 { get; set; }
        public string Movil2 { get; set; }
        public string Movil3 { get; set; }
        public string Direccion { get; set; }
        public string Pais { get; set; }
        public string Departamento { get; set; }
        public string Ciudad { get; set; }
        public string Email { get; set; }
        public string Regimen { get; set; }
        public string CodDane { get; set; }
        public string Imagen { get; set; }
        public string Estado { get; set; }
        public string NombreCompleto { get; set; }
    }
    public class TerceroResumenViewModel
    {
        public int Id { get; set; }
        public string TipoIdentidad { get; set; }
        public string Identificacion { get; set; }
        public string DigitoVerificacion { get; set; }
        public string TipoPersona { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NombreContacto { get; set; }
        public string NombreCompleto { get; set; }
        public string RazonSocial { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Estado { get; set; }
    }
}