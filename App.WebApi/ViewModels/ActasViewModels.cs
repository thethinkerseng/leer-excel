﻿using App.Core.Entities.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.WebApi.ViewModels
{

    public class ExcelViewModels
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan Hora { get; set; }
        public string ArchivoNombre { get; set; }
        public string ArchivoExtension { get; set; }
        public string ArchivoTipo { get; set; }
        public int ArchivoLong { get; set; }
        public string Estado { get; set; }
    }
    public class ExcelViewModelsResquest : ExcelViewModels
    {
        public bool Borrador { get; set; }
    } 
}