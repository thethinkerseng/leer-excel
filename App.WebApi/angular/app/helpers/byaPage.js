﻿var byaPage = {
    loading: "",
    loadingmenu: "",
    mostrarMensajes: true,
    LoadingMenu: function (Titulo, Mensaje) {
        if (Titulo == null) Titulo = "Por favor espere un momento...";
        if (Mensaje == null) Mensaje = "";
        this.loadingmenu = bootbox.dialog({
            title: Titulo,
            message: '<div class="row text-center"><div class="col-xs-12"><img class="align-center"  style="width:100px; height:100px;" src="/angular/img/loading11.gif" /></div><div class="col-xs-12 text-center"><h4>' + Mensaje + '</h4></div></div>',
            closeButton: false
        });
    },
    Loading: function (Titulo, Mensaje) {
        if (Titulo == null) Titulo = "Por favor espere un momento...";
        if (Mensaje == null) Mensaje = "";
        this.loading = bootbox.dialog({
            title: Titulo,
            message: '<div class="row text-center"><div class="col-xs-12"><img class="align-center"  style="width:100px; height:100px;" src="/angular/img/loading11.gif" /></div><div class="col-xs-12 text-center"><h4>' + Mensaje + '</h4></div></div>',
            closeButton: false
        });
    },
    EndLoading: function () {
        if (this.loading != null && this.loading)
        {
            this.loading.modal('hide');
        }
    },
    EndLoadingMenu: function () {
        if (this.loadingmenu != null && this.loadingmenu) {
            this.loadingmenu.modal('hide');
        }
    },
    errorPeticion: function (error) {
        console.log(error);
        byaPage.EndLoading();
        byaPage.EndLoadingMenu();
        byaPage.mostarMensaje(error);
    },
    errorPeticionAjax: function (error) {
        console.log(error);
        byaPage.mostarMensaje(error);
    },
    mostarMensaje: function (error) {
        if (error.status != 0) {
            if (typeof error.data === 'object' && error.data !== null) {
                if (error.data.error_description || error.data.Message) {
                    toaster.error((error.data.error_description ? error.data.error_description : error.data.Message));
                } else {
                    bootbox.dialog({
                        title: byaSite.getAplicacion(),
                        message: byaPage.getErrorHtml(error)
                    });
                }
            } else {
                if (error.status == 404) {
                    toaster.error("No se encontraron los datos");
                } else {
                    bootbox.dialog({
                        title: byaSite.getAplicacion(),
                        message: byaPage.getErrorHtml(error)
                    });
                }
            }
        } else {
            bootbox.dialog({
                title: byaSite.getAplicacion(),
                message: "No se puedo conectar con el servidor"
            });
        }
    },
    getErrorHtml: function (error) {
        return '<p>'+error.statusText+'</p>'+
            '<button type="button" class="btn btn-link btn-info" style="float: right" data-toggle="collapse" data-target="#demo"><i class="fa fa-info-circle"></i> Ver Detalles</button><div class="clearfix"></div>' +
            '<div id="demo" class="collapse">' + (typeof error.data === 'string' ? error.data : (error.data === null ? "Ocurrio un inconveniente al procesar la solictud" : "Error")) + "<br /> Se ha enviado un correo electrónico a soporte técnico" + '</div>'
    },
    console: function (value) {
        if (this.mostrarMensajes) {
            console.log(value);
        }
    },
    alert: function (value) {
        if (this.mostrarMensajes) {
            alert(value);
        }
    },
    stringToDate: function(_date, _format, _delimiter) {
        var formatLowerCase = _format.toLowerCase();
        var formatItems = formatLowerCase.split(_delimiter);
        var dateItems = _date.split(_delimiter);
        var monthIndex = formatItems.indexOf("mm");
        var dayIndex = formatItems.indexOf("dd");
        var yearIndex = formatItems.indexOf("yyyy");
        var month = parseInt(dateItems[monthIndex]);
        month -= 1;
        var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
        return formatedDate;
    },
     printDiv: function (element) {
        var objeto = document.getElementById(element);  //obtenemos el objeto a imprimir
        var ventana = window.open('', '_blank');  //abrimos una ventana vacía nueva
        //var url = "/jqwidgets/styles/jqx.metro.css";
        ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
        //ventana.document.find('head').append('<link rel="stylesheet" href="' + url + '" media="screen" />');
        ventana.document.close();  //cerramos el documento
        ventana.print();  //imprimimos la ventana
        ventana.close();  //cerramos la ventana
    },
    calculaDigitoVerificacion : function(nit)
    {
        if (nit != undefined && nit != "") {
            li_peso = new Array();
            li_peso[0] = 71;
            li_peso[1] = 67;
            li_peso[2] = 59;
            li_peso[3] = 53;
            li_peso[4] = 47;
            li_peso[5] = 43;
            li_peso[6] = 41;
            li_peso[7] = 37;
            li_peso[8] = 29;
            li_peso[9] = 23;
            li_peso[10] = 19;
            li_peso[11] = 17;
            li_peso[12] = 13;
            li_peso[13] = 7;
            li_peso[14] = 3;

            li_dif = 15 - nit.length;
            li_suma = 0;
            for (i = 0; i < nit.length; i++) {
                li_suma += nit.substring(i, i + 1) * li_peso[li_dif + i];
            }
            digito_chequeo = li_suma % 11;
            if (digito_chequeo >= 2) {
                digito_chequeo = 11 - digito_chequeo;
            }

            return digito_chequeo;
        }
    }
};