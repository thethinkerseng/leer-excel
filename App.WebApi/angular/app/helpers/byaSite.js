﻿var byaSite = {
    aplicacion: "UPEXCEL BYA",
    appVersion: "app_version",
    descripcion: ": function ",
    usuario: 'app_user',
    nombreUsuario: 'app_nom',
    tipoIdentificacion: 'app_tid',
    tokenAdm: 'app_token_admin',
    version: '20181112',
    setUsuario: function (username) {
        var encode = Base64.encode(username);
        localStorage.setItem(byaSite.usuario, encode);
    },
    ByADateTime: {
        Now: new Date()
    },
    getUsuario: function () {
        var user = localStorage.getItem(byaSite.usuario);
        if (user !== null && user !== "") {
            var userDecode = Base64.decode(user);
            return userDecode;
        } else {
            window.location.href = "/#/login";
        }
    },
    getNombreUsuario: function () {
        var user = localStorage.getItem(byaSite.nombreUsuario);
        if (user !== null && user !== "") {
            var userDecode = Base64.decode(user);
            return userDecode;
        } else {
            return "";
        }
    },
    getTipoIdentificacion: function () {
        var user = localStorage.getItem(byaSite.tipoIdentificacion);
        if (user !== null && user !== "") {
            var userDecode = Base64.decode(user);
            return userDecode;
        } else {
            return "";
        }
    },
    setTokenAdm: function (token) {
        localStorage.setItem(byaSite.tokenAdm, token);
    },
    setNombreUsuario: function (nombre) {
        localStorage.setItem(byaSite.nombreUsuario, Base64.encode(nombre));
    },
    setTipoIdentificacion: function (nombre) {
        localStorage.setItem(byaSite.tipoIdentificacion, Base64.encode(nombre));
    },
    getTokenAdm: function () {
        var token = localStorage.getItem(byaSite.tokenAdm);
        if (token !== null && token !== "") {
            return token;
        } else {
            window.location.href = "/#/login";
        }
    },
    getAplicacion: function () {
        return byaSite.aplicacion;
    },
    getAplicacionDescripcion: function () {
        return byaSite.descripcion;
    },
    getVersion: function () {
        var _version = byaSite.readCookie(byaSite.appVersion);
        return _version ? _version : byaSite.version;
    },
    removeSesionAdm: function () {
        localStorage.removeItem(byaSite.usuario);
        localStorage.removeItem(byaSite.tokenAdm);
        localStorage.removeItem(byaSite.nombreUsuario);
        window.location.href = "/#/login";
    },
    createCookie: function (name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    readCookie: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },
    eraseCookie: function (name) {
        createCookie(name, "", -1);
    }
};

$.urlParam = function (name) {
    // este codigo funciona en reset password
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
};