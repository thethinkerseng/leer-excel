﻿app.service("ActasService", function (HttpService) {
    var url = "api/excel/";
    this.ConsultarTodas = function () {
        return HttpService.get(url);
    };
    this.ConsultarActas = function (filtro) {
        return HttpService.getParams(url + "Filtrar", filtro);
    };
    this.ConsultarRegistros = function (id) {
        return HttpService.get(url + id + "/Items/Filtrar");
    };
    this.Consultar = function (id) {
        return HttpService.get(url + id);
    };
    this.Guardar = function (Request) {
        return HttpService.postFile(url, Request);
    };

    this.Modificar = function (Request) {
        return HttpService.postFile(url + "Modificar", Request);
    };
    this.ConsultarFuncionario = function (id) {
        return HttpService.get(url + "Funcionario/" + id);
    };
    this.ConsultarVehiculo = function (id) {
        return HttpService.get(url + "Vehiculo/" + id);
    };
    this.ConsultarEstablecimiento = function (id) {
        return HttpService.get(url + "Establecimiento/" + id);
    };
    this.ConsultarResponsable = function (id) {
        return HttpService.get(url + "Responsable/" + id);
    };
    this.ConsultarProductos = function (clase, subclase, descripcion) {
        return HttpService.getParams(url + "Productos", { clase: clase, subclase: subclase, descripcion: descripcion });
    };
    this.ConsultarFuncionario = function (id) {
        return HttpService.get(url + "Funcionario/" + id);
    };
    this.ConsultarConductor = function (id) {
        return HttpService.get(url + "Conductor/" + id);
    };
    this.ConsultarDatosFormulario = function () {
        return HttpService.get(url + "datos");
    };
    this.ValidarNumero = function (numero) {
        return HttpService.get(url + "NumeroActa/" + numero);
    };
});

app.service("ConsultaParametrizadaService", function (HttpService) {
    var url = "api/anotaciones/"
    this.ConsultarAnotaciones = function (RequestConsulta) {
        return HttpService.post(url + "consulta/parametrizada", RequestConsulta);
    };
    this.ConsultarDatosFormulario = function () {
        return HttpService.get(url+"consulta/parametrizada/datos");
    };
});

app.service("EstadosSistema", function (HttpService) {
    var url = "api/estados";
    this.ConsultarEstadosSolicitudesLibranzaLibrador = function () {
        return HttpService.get(url + "/solicitudes/libranza/librador");
    };
    this.ConsultarEstadosSolicitudesLibranzaRevisorTesoreria = function () {
        return HttpService.get(url + "/solicitudes/libranza/revisor");
    };
    this.ConsultarEstadosSolicitudesLibranzaTesoreria = function () {
        return HttpService.get(url + "/solicitudes/libranza/tesoreria");
    };
});
app.factory("ByATimeDate", function () {
    var ret = function () { }
    function Ahora() {
        return new Date();
    };
    function Fecha(fecha) {
        if (fecha) {
            return new Date(fecha);
        } else {
            return Ahora();
        }
    };
    ret.Now = Ahora;
    ret.Fecha = Fecha;
    return ret;
});
app.service("menuService", function ($http) {
    var base = "/api/Menu/";
    this.getMenu = function () {

        var pet = {
            method: 'GET',
            url: base + byaSite.getUsuario(),
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
                'Content-Type': 'application/json'
            }
        };
        var req = $http(pet);

        return req;
    };
});
app.service("HttpService", function ($http) {
    this.post_tokenPago = function (url, obj) {

        var pet = {
            method: 'POST',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenPagos(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    }
    this.get_tokenPago = function (url) {

        var pet = {
            method: 'GET',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenPagos(),
            }
        };
        var req = $http(pet);
        return req;
    }
    this.post = function (url, obj) {

        var pet = {
            method: 'POST',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    this.postFile = function (url, obj) {
        var pet = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': undefined,
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm()
            },
            transformRequest: angular.identity,
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    
    this.put = function (url, obj) {

        var pet = {
            method: 'PUT',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    this.delete = function (url, obj) {

        var pet = {
            method: 'DELETE',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    this.get = function (url, cache) {
        cache = (typeof cache === 'undefined') ? false : cache;
        var pet = {
            method: 'GET',
            cache: cache,
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
                'Content-Type': 'application/json'
            }
        };
        var req = $http(pet);
        return req;
    };
    this.getParams = function (url, data) {
        var pet = {
            method: 'GET',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
                'Content-Type': 'application/json'
            },
            params: data
        };
        var req = $http(pet);
        return req;
    };
});
app.service("RolService", function ($http) {
    var base = "/api/roles/";
    this.getRoles = function (userName) {
        var req = $http.get(base + "ConsultaRoles/" + userName);
        return req;
    };
    this.addRolToUserName = function (obj) {
        var req = $http.post(base + "AddUserToRoles",obj);
        return req;
    };
    this.removeRolToUserName = function (obj) {
        var req = $http.post(base + "RemoveUserFromRoles", obj);
        return req;
    };
});
app.service("UsuarioService", function (HttpService) {
    var url = "/api/Usuarios/";
    this.Obtener = function (filtro) {
        return HttpService.get(url + "Filtro/" + filtro);
    };
    this.UsuarioRegistrado = function (usuario) {
        return HttpService.post(url + "UsuarioRegistrado", usuario);
    };

    this.PostForzar = function (obj) {
        return HttpService.post(url + "Password", obj);
    };
    this.Guardar = function (obj) {
        return HttpService.post(url, obj);
    };

    this.GuardarRoles = function (username, obj) {
        return HttpService.post(url + username + "/Roles", obj);
    };
    this.GetRolesPersona = function (username) {
        return HttpService.get(url + username + "/Roles");
    };

    this.ChangePassword = function (obj) {
        return HttpService.post(url + "ChangePassword", obj);
    };
    this.ResetPassword = function (cuenta) {
        return HttpService.post(url + "public/user/ResetPassword", cuenta);
    };
    this.ForgotPassword = function (email) {
        return HttpService.post(url + "public/user/ForgotPassword", email);
    };
    this.ReSendConfirmationEmail = function (user) {
        return HttpService.get(url + "ReSendConfirmationEmail/" + user);
    };

    this.UpdateSesionUsuario = function (username) {
        return HttpService.post(url + "UpdateSesionUsuario/" + username);
    };
});
app.service("TerceroService", function (HttpService) {
    var url = "/api/tercero/";
    this.ObtenerTodos = function () {
        return HttpService.get(url);
    };
    this.Obtener = function (filtro) {
        return HttpService.get(url + filtro);
    };
    this.Guardar = function (obj) {
        return HttpService.post(url, obj);
    };
    this.Modificar = function (obj) {
        return HttpService.post(url + "Modificar", obj);
    };
    this.EstadoTercero = function (obj) {
        return HttpService.post(url + "Estado", obj);
    };
});
app.service("AccountService", function ($http) {
    var base = "/api/accounts/";

    this.createUsuarioTerceroFinanciero = function (obj) {
        var req = $http.post(base + 'createUserFinanciero', obj);
        return req;
    };

    this.createUsuario = function (obj) {
        var req = $http.post(base + 'create', obj);
        return req;
    };
    this.UpdateUsuario = function (obj) {
        var req = $http.post(base + 'UpdateUser', obj);
        return req;
    };
    this.ForgotPassword = function (obj) {
        var req = $http.post(base + 'ForgotPassword', obj);
        return req;
    };

    this.ResetPassword = function (obj) {
        var req = $http.post(base + 'ResetPassword', obj);
        return req;
    };
    this.AdminResetPassword = function (obj) {
        var req = $http.post(base + 'AdminResetPassword', obj);
        return req;
    };

    this.ChangePassword = function (obj) {
        var req = $http.post(base + 'ChangePassword', obj);
        return req;
    };
    this.ReenviarEmailActivacion = function (obj) {
        var req = $http.post(base + 'ReenvioEmailActivacion', obj);
        return req;
    };
});
app.service("loginService", function ($http) {
    this.getToken = function (username, password) {
        var dat = "username=" + username + "&password=" + password + "&grant_type=password";
        var pet = {
            method: 'POST',
            url: '/Token',
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: dat
        }
        var req = $http(pet);
        return req;
    };
});
app.service("LogosService", function ($http, HttpService) {
    var base = '/api/logos/';
    this.postLogo = function (fd) {
        var req = $http.post(base, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        });
        return req;
    };
   
});
app.service("MenuService", ["HttpService", function (HttpService) {
    var uri = "";
    this.getMenuPadres = function () {
        return HttpService.get(uri + '/api/Menu/Padres');
    };
    this.ObtenerMenus = function (usuario, modulo) {
        return HttpService.get(uri + '/api/Menu/Usuario/' + usuario + '/Modulo/' + modulo);
    };
}]);

app.service("EnvioDatosService", function () {
    return {
        data: {},
        embargo:{}
    };
});
app.service("AuditoriaService", function (HttpService) {
    var url = "api/Auditoria/";
    this.ObtenerDatos = function () {
        return HttpService.get(url + "datos");
    };
    this.Consultar = function (parametros) {
        return HttpService.post(url + "Parametrizada", parametros);
    };
    this.Detalles = function (TablaName, RecordID) {
        return HttpService.get(url + "TablaName/" + TablaName + "/RecordID/" + RecordID);
    }
});