var app =
    angular.module('app')
        .config(
        [
            '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function($controllerProvider, $compileProvider, $filterProvider, $provide) {
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ]);


app.config(function($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: '<div><div class="pull-left"><ul class="breadcrumb"><li><i class="fa fa-home"></i><a href="#">Home</a></li><li ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li></ul></div>'+
        '<div class="pull-right" style="width: 125px;"><div class="page-header"><div class="header-buttons"><a href="" class="sidebar-toggler"></a><a href="" class="refresh"></a><a href="" class="fullscreen"></a></div></div></div></div>'
    });
});

app.filter('sumByKey', function () {
    return function (data, key) {
        if (typeof (data) === 'undefined' || typeof (key) === 'undefined') {
            return 0;
        }
        var sum = 0;
        for (var i = data.length - 1; i >= 0; i--) {
            sum += parseFloat(data[i][key]);
        }

        return sum;
    };
});


app.filter('ssn', function () {
    return function (text) {
        return ("" + text).replace(/(\d\d\d)(\d\d)(\d\d\d\d)/, '$1-$2-$3');
    }
});

app.run(function ($rootScope) {
    var lastDigestRun = new Date();
    $rootScope.$watch(function detectIdle() {
        var now = new Date();
        if (now - lastDigestRun > 10 * 30 * 60 * 3600) {
            console.log(new Date());
            // logout here, like delete cookie, navigate to login ...removeSesionAdm
            if (localStorage.getItem("Admin")) {
                byaSite.removeSesionAdm();
                localStorage.removeItem("Admin")
            }
            if (localStorage.getItem("Pagos")) {
                byaSite.removeSesionPagos();
                localStorage.removeItem("Pagos");
            }
            //byaSite.removeSesionAdm();
            //byaSite.removeSesionPagos();
        }
        lastDigestRun = now;
    });
});