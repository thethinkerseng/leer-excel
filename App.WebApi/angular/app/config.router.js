﻿'use strict';
angular.module('app')
    .run(
        [
            '$rootScope', '$state', '$stateParams',
            function($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
                $rootScope.MenuApp = [];
                $rootScope.CerrarSesion = function () {
                    byaSite.removeSesionAdm();
                    localStorage.removeItem("Tercero");
                };
                $rootScope.CerrarSesionUpago = function () {
                    byaSite.removeSesionPagos();
                };
                $rootScope._VerificarPermiso = function (Rol) {

                };
            }
        ]
    )
    .config(
        [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app', {
                    abstract: true,
                    url: '/app',
                    templateUrl: '/angular/views/layout.html?' + byaSite.getVersion()

                })
                .state('menu', {
                    abstract: true,
                    url: '/menu',
                    templateUrl: '/angular/views/Banners/Banner.html?' + byaSite.getVersion()
                })
                .state('app.inicio', {
                    url: '/inicio',
                    templateUrl: '/angular/views/inicio.html?' + byaSite.getVersion(),
                    ncyBreadcrumb: {
                        label: 'Inicio',
                        description: ''
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        '/angular/lib/jquery/charts/sparkline/jquery.sparkline.js',
                                        '/angular/lib/jquery/charts/easypiechart/jquery.easypiechart.js',
                                        '/angular/lib/jquery/charts/flot/jquery.flot.js',
                                        '/angular/lib/jquery/charts/flot/jquery.flot.resize.js',
                                        '/angular/lib/jquery/charts/flot/jquery.flot.pie.js',
                                        '/angular/lib/jquery/charts/flot/jquery.flot.tooltip.js',
                                        '/angular/lib/jquery/charts/flot/jquery.flot.orderBars.js',
                                        '/angular/app/controllers/dashboard.js',
                                        '/angular/app/directives/realtimechart.js'
                                    ]
                                });
                            }
                        ]
                    }
                })

                .state('app.ChangePassword', {
                    url: '/admin_pagos/change_password',
                    templateUrl: '/angular/views/Accounts/change_password.html?' + byaSite.getVersion(),
                    ncyBreadcrumb: {
                        label: 'Cambiar contraseña',
                        description: ''
                    }
                })
                .state('ResetPassword', {
                    url: '/views/Accounts/reset_password.html',
                    templateUrl: '/angular/views/Accounts/reset_password.html?' + byaSite.getVersion(),
                    ncyBreadcrumb: {
                        label: 'Restablecer la contraseña'
                    }
                })
                .state('Error', {
                    url: '/Error',
                    templateUrl: '/angular/views/Error/Error.html?' + byaSite.getVersion(),
                    ncyBreadcrumb: {
                        label: 'Error'
                    }
                })
                .state('login', {
                    url: '/login',
                    templateUrl: '/angular/views/login.html?' + byaSite.getVersion(),
                    ncyBreadcrumb: {
                        label: 'Login'
                    }
                })
                .state('app.general',
                    {
                        //url: '/admin_pagos/Configuracion/Correo',
                        url: '/:modulo/:opcion/:pagina',
                        templateUrl: function (a) {
                            //return '/angular/views_bya/admin_pagos/Configuracion/Correo.html'
                            return "/angular/views_bya/" + a.modulo + "/" + a.opcion + "/" + a.pagina + ".html?" + byaSite.getVersion();
                        }, ncyBreadcrumb: { label: "{{stateLabel}}" }
                    });
        }
    ]
); 
