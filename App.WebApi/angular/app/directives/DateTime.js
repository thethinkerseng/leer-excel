﻿app.directive('dateTime', function ($window, $templateCache, $compile, $http) {
    return {
        //template: '/tpl/blocks/busqueda-tercero.html',
        template: '<div class="modal fade">' +
       '<div class="modal-dialog">' +
         '<div class="modal-content">' +
           '<div class="modal-header">' +
             '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
             '<h4 class="modal-title">{{ title }}</h4>' +
           '</div>' +
           '<div class="modal-body" ng-transclude></div>' +
         '</div>' +
       '</div>' +
     '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        //transclude: true, // we want to insert custom content inside the directive
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;


            scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
            };

        }
        //template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
        //templateUrl: 'my-customer.html'
        //templateUrl: scope.templateUser
    };
});