﻿app.factory("servicioGeneral", ["$rootScope", "UsuarioService", function ($rootScope, UsuarioService) {
    var ret = function () { }
    function Ahora() {
        return new Date();
    };
    function Fecha(fecha) {
        if (fecha) {
            return new Date(fecha);
        } else {
            return Ahora();
        }
    };
    function ConvertFechaToString(fecha) {
        return fecha.getFullYear() + "-" + ((fecha.getMonth() < 9 ? '0' : '') + (fecha.getMonth() + 1)) + "-" + (fecha.getDate() < 10 ? '0' + fecha.getDate() : fecha.getDate());
    };
    function FechaMinima() {
        var _ahora = Ahora();
        _ahora.getMonth() === 0
        return new Date((_ahora.getMonth() === 0 ? _ahora.getFullYear() - 1 : _ahora.getFullYear()), (_ahora.getMonth() === 0 ? 11 : _ahora.getMonth() - 1), _ahora.getDate(), 0, 0, 0, 0);
    };
    function FechaEneroActual() {
        var _ahora = Ahora();
        _ahora.getMonth() === 0
        return new Date(_ahora.getFullYear(), 0, 1, 0, 0, 0, 0);
    };
    function FechaDiciembreActual() {
        var _ahora = Ahora();
        _ahora.getMonth() === 0
        return new Date(_ahora.getFullYear(), 11, 31, 0, 0, 0, 0);
    };
    ret.establecerNombre;
    ret.TerceroActual = {};
    ret.GetTercero = function () {
        if (ret.Identificacion) {
            return ret.TerceroActual;
        } else {
            return { Identificacion: byaSite.getUsuario(), NombreCompleto: byaSite.getNombreUsuario(),TipoIdentificacion:byaSite.getTipoIdentificacion() };
        }
    };
    ret.SetTercero = function (tercero) {
        ret.TerceroActual = tercero;
    };
    ret.buscarEnRoles = function (rol) {
        return rol.Roles === this.RolPage;
    };
    ret._verificarPermiso = function (RolPage, sinPermiso) {
        if (ret.Roles) {
            ret.verificandoPermiso(RolPage, angular.copy(ret.Roles), sinPermiso);
        } else {
            ret.buscarRoles(RolPage, sinPermiso);
        }
    };
    ret.verificandoPermiso = function (RolPage, Roles, accion) {
        var ban = Roles.find(ret.buscarEnRoles, { RolPage: RolPage });
        if (!ban) {
            if (!accion) window.location.href = "/#/login";
            else accion();
        };
    };
    ret.buscarRoles = function (RolPage, accion) {
        var resultado = UsuarioService.GetRolesPersona(byaSite.getUsuario());
        resultado.then(function (pl) {
            ret.Roles = pl.data;
            ret.verificandoPermiso(RolPage, pl.data, accion);
        }, byaPage.errorPeticion);
    };
    ret._nombreTitle = function (nombre) {
        $rootScope.stateLabel = nombre;
        setTimeout(function () {
            $("#header-title").html(nombre);
            $(".page-title").html(byaSite.getAplicacion() + " :: " + nombre);
        }, 850);
    };
    ret.addAlert = function (type, msg) {
        switch (type) {
            case "danger":
                toaster.options.timeOut = 10000;
                toaster.error(msg, "Error");
                break;
            case "warning":
                toaster.options.timeOut = 6000;
                toaster.warning(msg, "Advertencia");
                break;
            case "success":
                toaster.options.timeOut = 3000;
                toaster.success(msg, "Completado");
                break;
        }
    };
    ret.cerrarModal;
    ret.Now = Ahora;
    ret.Fecha = Fecha;
    ret.ConvertFechaToString = ConvertFechaToString;
    ret.FechaMinima = FechaMinima;
    ret.FechaEneroActual = FechaEneroActual;
    ret.FechaDiciembreActual = FechaDiciembreActual;
    return ret;
}]);

app.service("funcionService", [function () {
    this.DigitoVerificacion = function (nit) {
        var liPeso = [];
        liPeso[0] = 71;
        liPeso[1] = 67;
        liPeso[2] = 59;
        liPeso[3] = 53;
        liPeso[4] = 47;
        liPeso[5] = 43;
        liPeso[6] = 41;
        liPeso[7] = 37;
        liPeso[8] = 29;
        liPeso[9] = 23;
        liPeso[10] = 19;
        liPeso[11] = 17;
        liPeso[12] = 13;
        liPeso[13] = 7;
        liPeso[14] = 3;

        var liDif = 15 - nit.length;
        var liSuma = 0;
        for (var i = 0; i < nit.length; i++) {
            liSuma += nit.substring(i, i + 1) * liPeso[liDif + i];
        }
        var digitoChequeo = liSuma % 11;
        if (digitoChequeo >= 2) {
            digitoChequeo = 11 - digitoChequeo;
        }
        return digitoChequeo;
    };

    this.arrayObjectIndexOf = function (myArray, searchTerm, property) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][property] === searchTerm) return i;
        }
        return -1;
    }

    this.TipoRepresentantes = function () {
        var representate = [
            {
                "Sigla": "RL",
                "Descripcion": "REPRESENTANTE LEGAL"
            },
            {
                "Sigla": "CN",
                "Descripcion": "CONTADOR"
            },
            {
                "Sigla": "RF",
                "Descripcion": "REVISOR FISCAL"
            },
            {
                "Sigla": "RL",
                "Descripcion": "REPRESENTANTE LEGAL"
            },
            {
                "Sigla": "RLS",
                "Descripcion": "REPRESENTNATE LEGAL SUPLENTE"
            },
            {
                "Sigla": "FDCDF",
                "Descripcion": "FUNCIONARIO DELEGADO PARA CUMPLIR DEBERES FORMALES"
            },
            {
                "Sigla": "LI",
                "Descripcion": "LIQUIDADOR"
            },
            {
                "Sigla": "LIS",
                "Descripcion": "LIQUIDADOR SUPLENTE"
            },
            {
                "Sigla": "APE",
                "Descripcion": "APODERADO ESPECIAL"
            },
            {
                "Sigla": "APG",
                "Descripcion": "APODERADO GENERAL"
            },
            {
                "Sigla": "AGO",
                "Descripcion": "AGENTE OFICIOSO"
            },
            {
                "Sigla": "ALB",
                "Descripcion": "ALBACEA"
            }, {
                "Sigla": "ADJ",
                "Descripcion": "ADMINISTRADOR JUDICIAL"
            },
             {
                 "Sigla": "ADP",
                 "Descripcion": "ADMINISTRADOR PRIVADO"
             }


        ];
        return representate;
    };

    this.Estrato = function () {

    }

    this.diferenciaDias = function (fecha1, fecha2) {
        var f1 = moment(fecha1.setHours(0, 0, 0, 0));
        var f2 = moment(fecha2.setHours(0, 0, 0, 0));
        return f2.diff(f1, "days", true);
    }

}]);