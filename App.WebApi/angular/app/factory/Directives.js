﻿app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
app.directive('setFocusIf', function () {
    return {
        link: function ($scope, $element, $attr) {
            $scope.$watch($attr.setFocusIf, function (value) {
                if (value) { $element[0].focus(); }
            });
        }
    };
});
app.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
            element.on('focus', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
}]);
app.directive("form", ['$parse', function ($parse) {
    return {
        require: 'form',
        restrict: 'E',
        link: function (scope, elem, attrs, form) {
            form.doSubmit = function () {
                var valido = true;
                angular.forEach(form, function (field, name) {
                    if (typeof name === 'string' && !name.match('^[\$]')) {
                        if (!field.$valid) {
                            console.log(field)
                        }
                        valido = valido && field.$valid;
                        if (field.$pristine) {
                            return field.$setViewValue(field.$modelValue);
                        }
                    }
                });
                console.log("entra - " + form.$valid)
                if (valido) {
                    console.log("aqui")
                    form.$setSubmitted();
                    return scope.$eval(attrs.ngSubmit);
                }


                /*if (form.$valid) {
                    form.$setSubmitted();
                    return scope.$eval(attrs.ngSubmit);
                } else {

                    form.$setSubmitted();
                    return scope.$eval(attrs.ngSubmit);
                }*/
            };
        }
    };
}]);
app.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                scope.$emit("fileSelected", { file: files[0] });
                //iterate files since 'multiple' may be specified on the element
            });
        }
    };
})