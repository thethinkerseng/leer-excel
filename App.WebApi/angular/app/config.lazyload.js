angular.module('app')
    .config([
        '$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                debug: true,
                events: true,
                modules: [
                 /*   {
                        name: 'toaster',
                        files: [
                            '/angular/lib/modules/angularjs-toaster/toaster.css',
                            '/angular/lib/modules/angularjs-toaster/toaster.js'
                        ]
                    },*/
                    {
                        name: 'ui.select',
                        files: [
                            '/angular/lib/modules/angular-ui-select/select.css',
                            '/angular/lib/modules/angular-ui-select/select.js'
                        ]
                    },
                    {
                        name: 'ngTagsInput',
                        files: [
                            '/angular/lib/modules/ng-tags-input/ng-tags-input.js'
                        ]
                    },
                  {
                        name: 'daterangepicker',
                        serie: true,
                        files: [
                            '/angular/lib/modules/angular-daterangepicker/moment.js',
                            '/angular/lib/modules/angular-daterangepicker/daterangepicker.js',
                            '/angular/lib/modules/angular-daterangepicker/angular-daterangepicker.js'
                        ]
                    },
                    {
                        name: 'vr.directives.slider',
                        files: [
                            '/angular/lib/modules/angular-slider/angular-slider.min.js'
                        ]
                    },
                    {
                        name: 'minicolors',
                        files: [
                            '/angular/lib/modules/angular-minicolors/jquery.minicolors.js',
                            '/angular/lib/modules/angular-minicolors/angular-minicolors.js'
                        ]
                    },
                    //{
                    //    name: 'textAngular',
                    //    files: [
                    //        '/angular/lib/modules/text-angular/textAngular-sanitize.min.js',
                    //        '/angular/lib/modules/text-angular/textAngular-rangy.min.js',
                    //        '/angular/lib/modules/text-angular/textAngular.min.js'
                    //    ]
                    //},
                    {
                        name: 'ng-nestable',
                        files: [
                            '/angular/lib/modules/angular-nestable/jquery.nestable.js',
                            '/angular/lib/modules/angular-nestable/angular-nestable.js'
                        ]
                    },
                    {
                        name: 'angularBootstrapNavTree',
                        files: [
                            '/angular/lib/modules/angular-bootstrap-nav-tree/abn_tree_directive.js'
                        ]
                    },
                    {
                        name: 'ui.calendar',
                        files: [
                            '/angular/lib/jquery/fullcalendar/jquery-ui.custom.min.js',
                            '/angular/lib/jquery/fullcalendar/moment.min.js',
                            '/angular/lib/jquery/fullcalendar/fullcalendar.js',
                            '/angular/lib/modules/angular-ui-calendar/calendar.js'
                        ]
                    },
                    {
                        name: 'ngGrid',
                        files: [
                            '/angular/lib/modules/ng-grid/ng-grid.min.js',
                            '/angular/lib/modules/ng-grid/ng-grid.css'
                        ]
                    },
                    {
                        name: 'dropzone',
                        files: [
                            '/angular/lib/modules/angular-dropzone/dropzone.min.js',
                            '/angular/lib/modules/angular-dropzone/angular-dropzone.js'
                        ]
                    }
                ]
            });
        }
    ]);