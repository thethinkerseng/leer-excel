app.service("ActasService", function (HttpService) {
    var url = "api/excel/";
    this.ConsultarTodas = function () {
        return HttpService.get(url);
    };
    this.ConsultarActas = function (filtro) {
        return HttpService.getParams(url + "Filtrar", filtro);
    };
    this.ConsultarRegistros = function (id) {
        return HttpService.get(url + id + "/Items/Filtrar");
    };
    this.Consultar = function (id) {
        return HttpService.get(url + id);
    };
    this.Guardar = function (Request) {
        return HttpService.postFile(url, Request);
    };

    this.Modificar = function (Request) {
        return HttpService.postFile(url + "Modificar", Request);
    };
    this.ConsultarFuncionario = function (id) {
        return HttpService.get(url + "Funcionario/" + id);
    };
    this.ConsultarVehiculo = function (id) {
        return HttpService.get(url + "Vehiculo/" + id);
    };
    this.ConsultarEstablecimiento = function (id) {
        return HttpService.get(url + "Establecimiento/" + id);
    };
    this.ConsultarResponsable = function (id) {
        return HttpService.get(url + "Responsable/" + id);
    };
    this.ConsultarProductos = function (clase, subclase, descripcion) {
        return HttpService.getParams(url + "Productos", { clase: clase, subclase: subclase, descripcion: descripcion });
    };
    this.ConsultarFuncionario = function (id) {
        return HttpService.get(url + "Funcionario/" + id);
    };
    this.ConsultarConductor = function (id) {
        return HttpService.get(url + "Conductor/" + id);
    };
    this.ConsultarDatosFormulario = function () {
        return HttpService.get(url + "datos");
    };
    this.ValidarNumero = function (numero) {
        return HttpService.get(url + "NumeroActa/" + numero);
    };
});

app.service("ConsultaParametrizadaService", function (HttpService) {
    var url = "api/anotaciones/"
    this.ConsultarAnotaciones = function (RequestConsulta) {
        return HttpService.post(url + "consulta/parametrizada", RequestConsulta);
    };
    this.ConsultarDatosFormulario = function () {
        return HttpService.get(url+"consulta/parametrizada/datos");
    };
});

app.service("EstadosSistema", function (HttpService) {
    var url = "api/estados";
    this.ConsultarEstadosSolicitudesLibranzaLibrador = function () {
        return HttpService.get(url + "/solicitudes/libranza/librador");
    };
    this.ConsultarEstadosSolicitudesLibranzaRevisorTesoreria = function () {
        return HttpService.get(url + "/solicitudes/libranza/revisor");
    };
    this.ConsultarEstadosSolicitudesLibranzaTesoreria = function () {
        return HttpService.get(url + "/solicitudes/libranza/tesoreria");
    };
});
app.factory("ByATimeDate", function () {
    var ret = function () { }
    function Ahora() {
        return new Date();
    };
    function Fecha(fecha) {
        if (fecha) {
            return new Date(fecha);
        } else {
            return Ahora();
        }
    };
    ret.Now = Ahora;
    ret.Fecha = Fecha;
    return ret;
});
app.service("menuService", function ($http) {
    var base = "/api/Menu/";
    this.getMenu = function () {

        var pet = {
            method: 'GET',
            url: base + byaSite.getUsuario(),
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
                'Content-Type': 'application/json'
            }
        };
        var req = $http(pet);

        return req;
    };
});
app.service("HttpService", function ($http) {
    this.post_tokenPago = function (url, obj) {

        var pet = {
            method: 'POST',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenPagos(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    }
    this.get_tokenPago = function (url) {

        var pet = {
            method: 'GET',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenPagos(),
            }
        };
        var req = $http(pet);
        return req;
    }
    this.post = function (url, obj) {

        var pet = {
            method: 'POST',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    this.postFile = function (url, obj) {
        var pet = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': undefined,
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm()
            },
            transformRequest: angular.identity,
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    
    this.put = function (url, obj) {

        var pet = {
            method: 'PUT',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    this.delete = function (url, obj) {

        var pet = {
            method: 'DELETE',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
            },
            data: obj
        };
        var req = $http(pet);
        return req;
    };
    this.get = function (url, cache) {
        cache = (typeof cache === 'undefined') ? false : cache;
        var pet = {
            method: 'GET',
            cache: cache,
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
                'Content-Type': 'application/json'
            }
        };
        var req = $http(pet);
        return req;
    };
    this.getParams = function (url, data) {
        var pet = {
            method: 'GET',
            url: url,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + byaSite.getTokenAdm(),
                'Content-Type': 'application/json'
            },
            params: data
        };
        var req = $http(pet);
        return req;
    };
});
app.service("RolService", function ($http) {
    var base = "/api/roles/";
    this.getRoles = function (userName) {
        var req = $http.get(base + "ConsultaRoles/" + userName);
        return req;
    };
    this.addRolToUserName = function (obj) {
        var req = $http.post(base + "AddUserToRoles",obj);
        return req;
    };
    this.removeRolToUserName = function (obj) {
        var req = $http.post(base + "RemoveUserFromRoles", obj);
        return req;
    };
});
app.service("UsuarioService", function (HttpService) {
    var url = "/api/Usuarios/";
    this.Obtener = function (filtro) {
        return HttpService.get(url + "Filtro/" + filtro);
    };
    this.UsuarioRegistrado = function (usuario) {
        return HttpService.post(url + "UsuarioRegistrado", usuario);
    };

    this.PostForzar = function (obj) {
        return HttpService.post(url + "Password", obj);
    };
    this.Guardar = function (obj) {
        return HttpService.post(url, obj);
    };

    this.GuardarRoles = function (username, obj) {
        return HttpService.post(url + username + "/Roles", obj);
    };
    this.GetRolesPersona = function (username) {
        return HttpService.get(url + username + "/Roles");
    };

    this.ChangePassword = function (obj) {
        return HttpService.post(url + "ChangePassword", obj);
    };
    this.ResetPassword = function (cuenta) {
        return HttpService.post(url + "public/user/ResetPassword", cuenta);
    };
    this.ForgotPassword = function (email) {
        return HttpService.post(url + "public/user/ForgotPassword", email);
    };
    this.ReSendConfirmationEmail = function (user) {
        return HttpService.get(url + "ReSendConfirmationEmail/" + user);
    };

    this.UpdateSesionUsuario = function (username) {
        return HttpService.post(url + "UpdateSesionUsuario/" + username);
    };
});
app.service("TerceroService", function (HttpService) {
    var url = "/api/tercero/";
    this.ObtenerTodos = function () {
        return HttpService.get(url);
    };
    this.Obtener = function (filtro) {
        return HttpService.get(url + filtro);
    };
    this.Guardar = function (obj) {
        return HttpService.post(url, obj);
    };
    this.Modificar = function (obj) {
        return HttpService.post(url + "Modificar", obj);
    };
    this.EstadoTercero = function (obj) {
        return HttpService.post(url + "Estado", obj);
    };
});
app.service("AccountService", function ($http) {
    var base = "/api/accounts/";

    this.createUsuarioTerceroFinanciero = function (obj) {
        var req = $http.post(base + 'createUserFinanciero', obj);
        return req;
    };

    this.createUsuario = function (obj) {
        var req = $http.post(base + 'create', obj);
        return req;
    };
    this.UpdateUsuario = function (obj) {
        var req = $http.post(base + 'UpdateUser', obj);
        return req;
    };
    this.ForgotPassword = function (obj) {
        var req = $http.post(base + 'ForgotPassword', obj);
        return req;
    };

    this.ResetPassword = function (obj) {
        var req = $http.post(base + 'ResetPassword', obj);
        return req;
    };
    this.AdminResetPassword = function (obj) {
        var req = $http.post(base + 'AdminResetPassword', obj);
        return req;
    };

    this.ChangePassword = function (obj) {
        var req = $http.post(base + 'ChangePassword', obj);
        return req;
    };
    this.ReenviarEmailActivacion = function (obj) {
        var req = $http.post(base + 'ReenvioEmailActivacion', obj);
        return req;
    };
});
app.service("loginService", function ($http) {
    this.getToken = function (username, password) {
        var dat = "username=" + username + "&password=" + password + "&grant_type=password";
        var pet = {
            method: 'POST',
            url: '/Token',
            headers: {
                'Accept': 'application/x-www-form-urlencoded',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: dat
        }
        var req = $http(pet);
        return req;
    };
});
app.service("LogosService", function ($http, HttpService) {
    var base = '/api/logos/';
    this.postLogo = function (fd) {
        var req = $http.post(base, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        });
        return req;
    };
   
});
app.service("MenuService", ["HttpService", function (HttpService) {
    var uri = "";
    this.getMenuPadres = function () {
        return HttpService.get(uri + '/api/Menu/Padres');
    };
    this.ObtenerMenus = function (usuario, modulo) {
        return HttpService.get(uri + '/api/Menu/Usuario/' + usuario + '/Modulo/' + modulo);
    };
}]);

app.service("EnvioDatosService", function () {
    return {
        data: {},
        embargo:{}
    };
});
app.service("AuditoriaService", function (HttpService) {
    var url = "api/Auditoria/";
    this.ObtenerDatos = function () {
        return HttpService.get(url + "datos");
    };
    this.Consultar = function (parametros) {
        return HttpService.post(url + "Parametrizada", parametros);
    };
    this.Detalles = function (TablaName, RecordID) {
        return HttpService.get(url + "TablaName/" + TablaName + "/RecordID/" + RecordID);
    }
});
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
app.directive('setFocusIf', function () {
    return {
        link: function ($scope, $element, $attr) {
            $scope.$watch($attr.setFocusIf, function (value) {
                if (value) { $element[0].focus(); }
            });
        }
    };
});
app.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
            element.on('focus', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
}]);
app.directive("form", ['$parse', function ($parse) {
    return {
        require: 'form',
        restrict: 'E',
        link: function (scope, elem, attrs, form) {
            form.doSubmit = function () {
                var valido = true;
                angular.forEach(form, function (field, name) {
                    if (typeof name === 'string' && !name.match('^[\$]')) {
                        if (!field.$valid) {
                            console.log(field)
                        }
                        valido = valido && field.$valid;
                        if (field.$pristine) {
                            return field.$setViewValue(field.$modelValue);
                        }
                    }
                });
                console.log("entra - " + form.$valid)
                if (valido) {
                    console.log("aqui")
                    form.$setSubmitted();
                    return scope.$eval(attrs.ngSubmit);
                }


                /*if (form.$valid) {
                    form.$setSubmitted();
                    return scope.$eval(attrs.ngSubmit);
                } else {

                    form.$setSubmitted();
                    return scope.$eval(attrs.ngSubmit);
                }*/
            };
        }
    };
}]);
app.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                scope.$emit("fileSelected", { file: files[0] });
                //iterate files since 'multiple' may be specified on the element
            });
        }
    };
})
app.factory("servicioGeneral", ["$rootScope", "UsuarioService", function ($rootScope, UsuarioService) {
    var ret = function () { }
    function Ahora() {
        return new Date();
    };
    function Fecha(fecha) {
        if (fecha) {
            return new Date(fecha);
        } else {
            return Ahora();
        }
    };
    function ConvertFechaToString(fecha) {
        return fecha.getFullYear() + "-" + ((fecha.getMonth() < 9 ? '0' : '') + (fecha.getMonth() + 1)) + "-" + (fecha.getDate() < 10 ? '0' + fecha.getDate() : fecha.getDate());
    };
    function FechaMinima() {
        var _ahora = Ahora();
        _ahora.getMonth() === 0
        return new Date((_ahora.getMonth() === 0 ? _ahora.getFullYear() - 1 : _ahora.getFullYear()), (_ahora.getMonth() === 0 ? 11 : _ahora.getMonth() - 1), _ahora.getDate(), 0, 0, 0, 0);
    };
    function FechaEneroActual() {
        var _ahora = Ahora();
        _ahora.getMonth() === 0
        return new Date(_ahora.getFullYear(), 0, 1, 0, 0, 0, 0);
    };
    function FechaDiciembreActual() {
        var _ahora = Ahora();
        _ahora.getMonth() === 0
        return new Date(_ahora.getFullYear(), 11, 31, 0, 0, 0, 0);
    };
    ret.establecerNombre;
    ret.TerceroActual = {};
    ret.GetTercero = function () {
        if (ret.Identificacion) {
            return ret.TerceroActual;
        } else {
            return { Identificacion: byaSite.getUsuario(), NombreCompleto: byaSite.getNombreUsuario(),TipoIdentificacion:byaSite.getTipoIdentificacion() };
        }
    };
    ret.SetTercero = function (tercero) {
        ret.TerceroActual = tercero;
    };
    ret.buscarEnRoles = function (rol) {
        return rol.Roles === this.RolPage;
    };
    ret._verificarPermiso = function (RolPage, sinPermiso) {
        if (ret.Roles) {
            ret.verificandoPermiso(RolPage, angular.copy(ret.Roles), sinPermiso);
        } else {
            ret.buscarRoles(RolPage, sinPermiso);
        }
    };
    ret.verificandoPermiso = function (RolPage, Roles, accion) {
        var ban = Roles.find(ret.buscarEnRoles, { RolPage: RolPage });
        if (!ban) {
            if (!accion) window.location.href = "/#/login";
            else accion();
        };
    };
    ret.buscarRoles = function (RolPage, accion) {
        var resultado = UsuarioService.GetRolesPersona(byaSite.getUsuario());
        resultado.then(function (pl) {
            ret.Roles = pl.data;
            ret.verificandoPermiso(RolPage, pl.data, accion);
        }, byaPage.errorPeticion);
    };
    ret._nombreTitle = function (nombre) {
        $rootScope.stateLabel = nombre;
        setTimeout(function () {
            $("#header-title").html(nombre);
            $(".page-title").html(byaSite.getAplicacion() + " :: " + nombre);
        }, 850);
    };
    ret.addAlert = function (type, msg) {
        switch (type) {
            case "danger":
                toaster.options.timeOut = 10000;
                toaster.error(msg, "Error");
                break;
            case "warning":
                toaster.options.timeOut = 6000;
                toaster.warning(msg, "Advertencia");
                break;
            case "success":
                toaster.options.timeOut = 3000;
                toaster.success(msg, "Completado");
                break;
        }
    };
    ret.cerrarModal;
    ret.Now = Ahora;
    ret.Fecha = Fecha;
    ret.ConvertFechaToString = ConvertFechaToString;
    ret.FechaMinima = FechaMinima;
    ret.FechaEneroActual = FechaEneroActual;
    ret.FechaDiciembreActual = FechaDiciembreActual;
    return ret;
}]);

app.service("funcionService", [function () {
    this.DigitoVerificacion = function (nit) {
        var liPeso = [];
        liPeso[0] = 71;
        liPeso[1] = 67;
        liPeso[2] = 59;
        liPeso[3] = 53;
        liPeso[4] = 47;
        liPeso[5] = 43;
        liPeso[6] = 41;
        liPeso[7] = 37;
        liPeso[8] = 29;
        liPeso[9] = 23;
        liPeso[10] = 19;
        liPeso[11] = 17;
        liPeso[12] = 13;
        liPeso[13] = 7;
        liPeso[14] = 3;

        var liDif = 15 - nit.length;
        var liSuma = 0;
        for (var i = 0; i < nit.length; i++) {
            liSuma += nit.substring(i, i + 1) * liPeso[liDif + i];
        }
        var digitoChequeo = liSuma % 11;
        if (digitoChequeo >= 2) {
            digitoChequeo = 11 - digitoChequeo;
        }
        return digitoChequeo;
    };

    this.arrayObjectIndexOf = function (myArray, searchTerm, property) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][property] === searchTerm) return i;
        }
        return -1;
    }

    this.TipoRepresentantes = function () {
        var representate = [
            {
                "Sigla": "RL",
                "Descripcion": "REPRESENTANTE LEGAL"
            },
            {
                "Sigla": "CN",
                "Descripcion": "CONTADOR"
            },
            {
                "Sigla": "RF",
                "Descripcion": "REVISOR FISCAL"
            },
            {
                "Sigla": "RL",
                "Descripcion": "REPRESENTANTE LEGAL"
            },
            {
                "Sigla": "RLS",
                "Descripcion": "REPRESENTNATE LEGAL SUPLENTE"
            },
            {
                "Sigla": "FDCDF",
                "Descripcion": "FUNCIONARIO DELEGADO PARA CUMPLIR DEBERES FORMALES"
            },
            {
                "Sigla": "LI",
                "Descripcion": "LIQUIDADOR"
            },
            {
                "Sigla": "LIS",
                "Descripcion": "LIQUIDADOR SUPLENTE"
            },
            {
                "Sigla": "APE",
                "Descripcion": "APODERADO ESPECIAL"
            },
            {
                "Sigla": "APG",
                "Descripcion": "APODERADO GENERAL"
            },
            {
                "Sigla": "AGO",
                "Descripcion": "AGENTE OFICIOSO"
            },
            {
                "Sigla": "ALB",
                "Descripcion": "ALBACEA"
            }, {
                "Sigla": "ADJ",
                "Descripcion": "ADMINISTRADOR JUDICIAL"
            },
             {
                 "Sigla": "ADP",
                 "Descripcion": "ADMINISTRADOR PRIVADO"
             }


        ];
        return representate;
    };

    this.Estrato = function () {

    }

    this.diferenciaDias = function (fecha1, fecha2) {
        var f1 = moment(fecha1.setHours(0, 0, 0, 0));
        var f2 = moment(fecha2.setHours(0, 0, 0, 0));
        return f2.diff(f1, "days", true);
    }

}]);
app.controller('AppCtrl', [
        '$rootScope', '$localStorage', '$state', '$timeout','UsuarioService','servicioGeneral','TerceroService',
        function ($rootScope, $localStorage, $state, $timeout, UsuarioService, servicioGeneral, TerceroService) {
            init();
            function init() {
                var usuario = byaSite.getUsuario();        
                if (usuario) {
                    _traerRolesUsuario(usuario);
                }
            }
            function _traerRolesUsuario(usuario) {
                var ser = UsuarioService.GetRolesPersona(usuario);
                ser.then(function (pl) {
                    servicioGeneral.Roles = pl.data;
                    $rootScope.lRolesPersona = pl.data;
                }, byaPage.errorPeticion);
            }
            $rootScope.settings = {
                skin: '',
                version: byaSite.getVersion(),
                color: {
                    themeprimary: '#2dc3e8',
                    themesecondary: '#fb6e52',
                    themethirdcolor: '#ffce55',
                    themefourthcolor: '#a0d468',
                    themefifthcolor: '#e75b8d'
                },
                rtl: false,
                fixed: {
                    navbar: false,
                    sidebar: false,
                    breadcrumbs: false,
                    header: false
                }
            };
            if (angular.isDefined($localStorage.settings))
                $rootScope.settings = $localStorage.settings;
            else
                $localStorage.settings = $rootScope.settings;

            $rootScope.$watch('settings', function () {
                if ($rootScope.settings.fixed.header) {
                    $rootScope.settings.fixed.navbar = true;
                    $rootScope.settings.fixed.sidebar = true;
                    $rootScope.settings.fixed.breadcrumbs = true;
                }
                if ($rootScope.settings.fixed.breadcrumbs) {
                    $rootScope.settings.fixed.navbar = true;
                    $rootScope.settings.fixed.sidebar = true;
                }
                if ($rootScope.settings.fixed.sidebar) {
                    $rootScope.settings.fixed.navbar = true;


                    //Slim Scrolling for Sidebar Menu in fix state
                    var position = $rootScope.settings.rtl ? 'right' : 'left';
                    if (!$('.page-sidebar').hasClass('menu-compact')) {
                        $('.sidebar-menu').slimscroll({
                            position: position,
                            size: '3px',
                            color: $rootScope.settings.color.themeprimary,
                            height: $(window).height() - 90
                        });
                    }
                } else {
                    if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                        $(".sidebar-menu").slimScroll({ destroy: true });
                        $(".sidebar-menu").attr('style', '');
                    }
                }

                $localStorage.settings = $rootScope.settings;
            }, true);

            $rootScope.$watch('settings.rtl', function () {
                if ($state.current.name !== "persian.dashboard" && $state.current.name !== "arabic.dashboard") {
                    switchClasses("pull-right", "pull-left");
                    switchClasses("databox-right", "databox-left");
                    switchClasses("item-right", "item-left");
                }

                $localStorage.settings = $rootScope.settings;
            }, true);

            $rootScope.$on('$viewContentLoaded',
                function (event, toState, toParams, fromState, fromParams) {
                    if ($rootScope.settings.rtl && $state.current.name !== "persian.dashboard" && $state.current.name !== "arabic.dashboard") {
                        switchClasses("pull-right", "pull-left");
                        switchClasses("databox-right", "databox-left");
                        switchClasses("item-right", "item-left");
                    }
                    if ($state.current.name === 'error404') {
                        $('body').addClass('body-404');
                    }
                    if ($state.current.name === 'error500') {
                        $('body').addClass('body-500');
                    }
                    $timeout(function () {
                        if ($rootScope.settings.fixed.sidebar) {
                            //Slim Scrolling for Sidebar Menu in fix state
                            var position = $rootScope.settings.rtl ? 'right' : 'left';
                            if (!$('.page-sidebar').hasClass('menu-compact')) {
                                $('.sidebar-menu').slimscroll({
                                    position: position,
                                    size: '3px',
                                    color: $rootScope.settings.color.themeprimary,
                                    height: $(window).height() - 90
                                });
                            }
                        } else {
                            if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                                $(".sidebar-menu").slimScroll({ destroy: true });
                                $(".sidebar-menu").attr('style', '');
                            }
                        }
                    }, 500);

                    window.scrollTo(0, 0);
                });
        }
    ]);
'use strict';

app
    // Dashboard Box controller 
    .controller('DashboardCtrl', ['$rootScope', '$scope', '$state',
        'toastr', '$filter', 'Pagination', 'servicioGeneral',
        function ($rootScope, $scope, $state, toastr, $filter, Pagination, servicioGeneral) {
        $scope.Fecha = new Date();
        $scope.Expression = false;
        $scope.Expression2 = false;
        init();
        $scope.Depend = null;
        $scope.Trami = null;
        function init() {

        }
        $scope.success = function () {
            servicioGeneral.addAlert("success", "Hola");
        }

        $scope.warning = function () {
            servicioGeneral.addAlert("warning", "Hola");
        }
        $scope.danger = function () {
            servicioGeneral.addAlert("danger", "Hola");
        }
        $scope.Refresh = function () {
            Consultar()
        }
        $scope.boxWidth = $('.box-tabbs').width() - 20;

        $scope.visitChartData = [
            {
                color: $rootScope.settings.color.themesecondary,
                label: "Direct Visits",
                data: [
                    [10, 2], [4, 5], [5, 4], [6, 11], [7, 12], [8, 11], [9, 8], [10, 14], [11, 12], [12, 16], [13, 9],
                    [14, 10], [15, 14], [16, 15], [17, 9]
                ],

                lines: {
                    show: true,
                    fill: true,
                    lineWidth: .1,
                    fillColor: {
                        colors: [
                            {
                                opacity: 0
                            }, {
                                opacity: 0.4
                            }
                        ]
                    }
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                color: $rootScope.settings.color.themeprimary,
                label: "Referral Visits",
                data: [
                    [3, 10], [4, 13], [5, 12], [6, 16], [7, 19], [8, 19], [9, 24], [10, 19], [11, 18], [12, 21], [13, 17],
                    [14, 14], [15, 12], [16, 14], [17, 15]
                ],
                bars: {
                    order: 1,
                    show: true,
                    borderWidth: 0,
                    barWidth: 0.4,
                    lineWidth: .5,
                    fillColor: {
                        colors: [
                            {
                                opacity: 0.4
                            }, {
                                opacity: 1
                            }
                        ]
                    }
                }
            },
            {
                color: $rootScope.settings.color.themethirdcolor,
                label: "Search Engines",
                data: [
                    [3, 14], [4, 11], [5, 10], [6, 9], [7, 5], [8, 8], [9, 5], [10, 6], [11, 4], [12, 7], [13, 4],
                    [14, 3], [15, 4], [16, 6], [17, 4]
                ],
                lines: {
                    show: true,
                    fill: false,
                    fillColor: {
                        colors: [
                            {
                                opacity: 0.3
                            }, {
                                opacity: 0
                            }
                        ]
                    }
                },
                points: {
                    show: true
                }
            }
        ];
        $scope.visitChartOptions = {
            legend: {
                show: false
            },
            xaxis: {
                tickDecimals: 0,
                color: '#f3f3f3'
            },
            yaxis: {
                min: 0,
                color: '#f3f3f3',
                tickFormatter: function (val, axis) {
                    return "";
                },
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 0,
                aboveData: false,
                color: '#fbfbfb'

            },
            tooltip: true,
            tooltipOpts: {
                defaultTheme: false,
                content: " <b>%x May</b> , <b>%s</b> : <span>%y</span>",
            }
        };

            //---Visitor Sources Pie Chart---//
        $scope.visitorSourcePieData = [
            {
                data: [[1, 21]],
                color: '#fb6e52'
            },
            {
                data: [[1, 12]],
                color: '#e75b8d'
            },
            {
                data: [[1, 11]],
                color: '#a0d468'
            },
            {
                data: [[1, 10]],
                color: '#ffce55'
            },
            {
                data: [[1, 46]],
                color: '#5db2ff'
            }
        ];
        $scope.visitorSourcePieOptions = {
            series: {
                pie: {
                    innerRadius: 0.45,
                    show: true,
                    stroke: {
                        width: 4
                    }
                }
            }
        };
}]);
'use strict';
app.controller('NavBarCtrl', ['$scope', 'servicioGeneral', '$filter', function ($scope, servicioGeneral, $filter) {
    _init();
    function _init() {
        var usernameGeneral = byaSite.getUsuario();
        servicioGeneral.establecerNombre = establecerNombre;
        if (usernameGeneral != "admin") {
            var tercero = servicioGeneral.GetTercero();
            establecerNombre(tercero.NombreCompleto ? tercero.NombreCompleto : usernameGeneral);
        } else {
            establecerNombre(usernameGeneral);
        }
    };
    function establecerNombre(nombre) {
        $scope.usernameGeneral = nombre;
    }
    $scope.colocarEspaciosEnBlanco = function (cadena) {
        while (cadena.length < 15) cadena = " " + cadena + " ";
        return cambiarEspaciosEnBlanco(cadena);
    }
    function cambiarEspaciosEnBlanco(cadena) {
        cadena = $filter("uppercase")(cadena);
        while (cadena.indexOf(' ') > -1) cadena = cadena.replace(' ', '&nbsp;');
        return cadena;
    };

}]);
'use strict';
app.controller('SideBarCtrl', ['$rootScope', '$scope', 'menuService', function ($rootScope, $scope, menuService) {
    _init();
    function _init() {
        _cargarMenu();
    };
    function _cargarMenu() {
        var promiseGet = menuService.getMenu();
        promiseGet.then(function (pl) {
            $scope.MenuApp = pl.data;
        }, byaPage.errorPeticion);
    };

}]);
app.controller("GestionArchivoCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "ActasService", "$modal", "servicioGeneral", "EnvioDatosService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, ActasService, $modal, servicioGeneral, EnvioDatosService, $filter) {
        $scope.Filtro = {};
        $scope.Terceros = [];
        $scope.tableTerceros = {};
        $scope.Identificacion = "";

        $scope.eliminar = function (item) {
            if (confirm("Seguro que desea eliminar el registro?")) {
                _eliminar(item);
            }
        };
        $scope.ConsultarPorFecha = function () {
            _traerActas();
        };
        $scope._CambiarEstado = function (item) {
            if (item.Estado === "IN") {
                item.Estado = "AC";
            } else {
                item.Estado = "IN";
            }
            $scope.Tercero = item;
            _guardarTerceroModificado();
        };
        $scope.verDetalles = function (item) {
            byaPage.Loading();
            var servicio = ActasService.ConsultarRegistros(item.Id);
            servicio.then(function (success) {
                byaPage.EndLoading();
                $scope.Resultado = success.data;
                $timeout(function () {
                    $("#modalRegistros").modal("show");
                }, 500);
            }, function (error) {
                byaPage.EndLoading();
                byaPage.errorPeticion(error);
            });
        };
    function _eliminar(item) {
        var send = ActasService.PostTercerosEliminar(item);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerActas();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert('danger', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    }
    _init();
    function _init() {
        servicioGeneral._nombreTitle("Cargar archivo");
        servicioGeneral._verificarPermiso("ARCHIVO_Consulta");
        _dibujarTablaTerceros();
        fechasEstablecer();
    }
    function fechasEstablecer() {
        var hoy = new Date();
        $scope.Filtro.FechaInicio = $filter("date")(new Date(hoy.getFullYear(), 0, 1), "yyyy-MM-dd", "-0500");
        $scope.Filtro.FechaFinal = $filter("date")(new Date(hoy.getFullYear(), 12, 31), "yyyy-MM-dd", "-0500");
        _traerActas();
    }
    function _guardarTerceroModificado() {
        var send = ActasService.EstadoTercero($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerActas();
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
            } else {
                _traerActas();
                servicioGeneral.addAlert('error', 'Error!', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    }
    function _traerActas() {
        byaPage.Loading();
        var servicio = ActasService.ConsultarActas($scope.Filtro);
        servicio.then(function (success) {
            byaPage.EndLoading();
            $scope.Terceros = success.data;
            $scope.tableTerceros.reload();
        }, function (error) {
            byaPage.EndLoading();
            byaPage.errorPeticion(error);
        });
    }
    function _dibujarTablaTerceros() {
        $scope.tableTerceros = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Terceros.filter(function (a) {
                    return (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                        (a.TipoIdentidad + "").toLowerCase().indexOf(c) > -1 ||
                        (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1;
                })) : f = $scope.Terceros, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()));
            }
        });
    }
}]);
app.controller("ImportarArchivoCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ActasService", "funcionService", "ByATimeDate", "servicioGeneral", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ActasService, funcionService, ByATimeDate, servicioGeneral, $filter) {
        $scope.Archivo = {};
        $scope.Departamentos = [];
        $scope.CausasMotivos = [];
        $scope.Cargando = true;
        $scope.Ruta = ""; // Ruta a la cual debe ir  desde otros impuestos.
        $scope.OcultarCampos = false;
        $scope.Origen = "Tercero";
        $scope.Direccion = {};
        $scope.MensajeOb = {};

        $scope.Boton = { "Guardar": false, "Siguiente": true, "Aceptar": false };

        $scope.regresar = function () {
            if ($.isEmptyObject($location.search())) {
                window.history.back();
            } else {
                location.href = "/#/app/archivo/excel/consulta";
            }
        };
        $scope.ConsultarConductor = function (busqueda) {
            ConsultarConductor(busqueda);
        };
        $scope.ConsultarSubClase = function (item, indice) {
            var encontrados = $filter("filter")($scope.SubClases, item.SubClase);
            if (encontrados.length === 0) {
                $scope.SubClases.push(item.SubClase);
            }
            $scope.ConsultarProductos(item, indice);
        };

        $scope.ConsultarDescripciones = function (item, indice) {
            var encontrados = $filter("filter")($scope.Descripciones, item.DescripcionMercancia);
            console.log(encontrados);
            if (encontrados.length === 0) {
                $scope.Descripciones.push(item.DescripcionMercancia);
            }
            $scope.ConsultarProductos(item, indice);
        };
        $scope.ConsultarCausal = function (item, indice) {
            var encontrados = $filter("filter")($scope.CausasMotivos, item.CausalMotivo);
            if (encontrados.length === 0) {
                $scope.CausasMotivos.push(item.CausalMotivo);
            }
            ValidarNuevo(item, indice);
        };
        $scope.ConsultarProductos = function (item, indice) {
            ValidarNuevo(item, indice);
            ConsultarProductos(item);
        };
        $scope.ConsultarResponsable = function (busqueda) {
            ConsultarResponsable(busqueda);
        };
        $scope.ConsultarEstablecimiento = function (busqueda) {
            ConsultarEstablecimiento(busqueda);
        };
        $scope.ConsultarVehiculo = function (busqueda) {
            ConsultarVehiculo(busqueda);
        };
        $scope.ConsultarFuncionario = function (busqueda) {
            ConsultarFuncionario(busqueda);
        };
        $scope.Guardar = function (borrador) {
            $scope.Archivo.Borrador = borrador;
            if ($scope.Archivo.Nueva === 'SI') guardarActa();
            else guardarActaModificada();
        };

        $scope.calculaDigitoVerificador = function () {
            if (!$scope.Archivo.Identificacion) return;
            $scope.Archivo.DVerificacion = funcionService.DigitoVerificacion($scope.Archivo.Identificacion);
        };

        $scope._volver = function () {
            history.back();
        };
        $scope._limpiar = function () {
            bootbox.confirm({
                message: "¿Seguro que desea limpiar todos los campos?",
                buttons: {
                    confirm: {
                        label: 'Aceptar',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-default'
                    }
                },
                callback: function (result) {
                    if (result === true)
                        nuevaActa();
                }
            });

        };

        $scope.ValidarNuevo = function (item, indice) {
            ValidarNuevo(item, indice);
        };
        $scope.eliminarItem = function (indice) {
            eliminarItem(indice);
        };
        $scope.ValidarNumeroActa = function () {
            ValidarNumeroActa();
        };
        $scope.actualizarArchivo = function () {
            actualizarArchivo();
        };
        init();
        function init() {
            servicioGeneral._nombreTitle("Cargar Archivo");
            servicioGeneral._verificarPermiso("ARCHIVO_Importar");
            setTimeout(function () { $(".sidebar-toggler").click(); }, 800);
            obtenerDatos();
            var datos = $location.search();
            if (!datos.Id) {
                nuevaActa();
            } else {
                var _id = datos.Id;
                consultarActa(_id);
            }
            $scope.Cargando = false;
        }

        function obtenerDatos() {
            var send = ActasService.ConsultarDatosFormulario();
            send.then(function (pl) {
                $scope.EstadoActas = pl.data.EstadoActas;
                $scope.TipoOperativos = pl.data.TipoOperativos;
                $scope.TipoEstablecimientos = pl.data.TipoEstablecimientos;
                //
                $scope.Clases = pl.data.Clases;
                $scope.Origenes = pl.data.Origenes;
                $scope.SubClases = pl.data.SubClases;
                $scope.Descripciones = pl.data.Descripciones;
                $scope.EstadosProductos = pl.data.EstadosProductos;
                $scope.EmpresaTransporte = pl.data.EmpresaTransporte;

            }, byaPage.errorPeticion);
        }

        function consultarActa(id) {
            var send = ActasService.Consultar(id);
            send.then(function (pl) {
                $scope.Archivo = pl.data;
                $scope.Archivo.Fecha = new Date($scope.Archivo.Fecha);
                $scope.Archivo.Nueva = 'NO';
                if ($.isArray($scope.Archivo.Productos)) {
                    $scope.Archivo.Productos.push({});
                } else {
                    $scope.Archivo.Productos = [{}];
                }
            }, byaPage.errorPeticion);
        }
        function ConsultarFuncionario(id) {
            var send = ActasService.ConsultarFuncionario(id);
            send.then(function (pl) {
                if (pl.data.FuncionarioIdentificacion === id) {
                    $scope.Archivo.FuncionarioIdentificacion = pl.data.FuncionarioIdentificacion;
                    $scope.Archivo.FuncionarioNombre = pl.data.FuncionarioNombre;
                    $scope.Archivo.FuncionarioCargo = pl.data.FuncionarioCargo;
                }
            }, byaPage.errorPeticion);
        }

        function ValidarNumeroActa() {
            var send = ActasService.ValidarNumero($scope.Archivo.NumeroActa);
            send.then(function (pl) {
                if (pl.data === "SI")
                    servicioGeneral.addAlert("warning", "Este número de acta ya esta registrado en el sistema");
            }, byaPage.errorPeticion);
        }

        function ConsultarVehiculo(id) {
            var send = ActasService.ConsultarVehiculo(id);
            send.then(function (pl) {
                if (pl.data.VehiculoPlaca === id) {
                    $scope.Archivo.VehiculoPlaca = pl.data.VehiculoPlaca;
                    $scope.Archivo.VehiculoMarca = pl.data.VehiculoMarca;
                    $scope.Archivo.VehiculoAfiliadoEmpresaTransporte = pl.data.VehiculoAfiliadoEmpresaTransporte;
                    $scope.Archivo.VehiculoEmpresaTransporte = pl.data.VehiculoEmpresaTransporte;
                }
            }, byaPage.errorPeticion);
        }

        function ConsultarEstablecimiento(id) {
            var send = ActasService.ConsultarEstablecimiento(id);
            send.then(function (pl) {
                if (pl.data.EstablecimientoNit === id) {
                    $scope.Archivo.EstablecimientoNit = pl.data.EstablecimientoNit;
                    $scope.Archivo.EstablecimientoNombreRazonSocial = pl.data.EstablecimientoNombreRazonSocial;
                    $scope.Archivo.EstablecimientoTipo = pl.data.EstablecimientoTipo;
                    $scope.Archivo.EstablecimientoDireccion = pl.data.EstablecimientoDireccion;
                    $scope.Archivo.EstablecimientoTelefono = pl.data.EstablecimientoTelefono;
                    $scope.Archivo.EstablecimientoCiudad = pl.data.EstablecimientoCiudad;
                    $scope.Archivo.EstablecimientoPropietarioIdentificacion = pl.data.EstablecimientoPropietarioIdentificacion;
                    $scope.Archivo.EstablecimientoPropietarioNombre = pl.data.EstablecimientoPropietarioNombre;
                    $scope.Archivo.EstablecimientoRepresentanteLegalIdentificacion = pl.data.EstablecimientoRepresentanteLegalIdentificacion;
                    $scope.Archivo.EstablecimientoRepresentanteLegalNombre = pl.data.EstablecimientoRepresentanteLegalNombre;
                }
            }, byaPage.errorPeticion);
        }

        function ConsultarResponsable(id) {
            var send = ActasService.ConsultarResponsable(id);
            send.then(function (pl) {
                if (pl.data.ResponsableTenedorNit === id) {
                    $scope.Archivo.ResponsableTenedorNit = pl.data.ResponsableTenedorNit;
                    $scope.Archivo.ResponsableTenedorNombre = pl.data.ResponsableTenedorNombre;
                    $scope.Archivo.ResponsableTenedorDireccion = pl.data.ResponsableTenedorDireccion;
                    $scope.Archivo.ResponsableTenedorTelefono = pl.data.ResponsableTenedorTelefono;
                    $scope.Archivo.ResponsableTenedorDepartamento = pl.data.ResponsableTenedorDepartamento;
                    $scope.Archivo.ResponsableTenedorCiudad = pl.data.ResponsableTenedorCiudad;
                }
            }, byaPage.errorPeticion);
        };
        function ConsultarConductor(id) {
            var send = ActasService.ConsultarConductor(id);
            send.then(function (pl) {
                if (pl.data.ConductorIdentificacion === id) {
                    $scope.Archivo.ConductorIdentificacion = pl.data.ConductorIdentificacion;
                    $scope.Archivo.ConductorNombre = pl.data.ConductorNombre;
                    $scope.Archivo.ConductorDireccion = pl.data.ConductorDireccion;
                    $scope.Archivo.ConductorTelefono = pl.data.ConductorTelefono;
                    $scope.Archivo.ConductorDepartamento = pl.data.ConductorDepartamento;
                    $scope.Archivo.ConductorCiudad = pl.data.ConductorCiudad;
                }
            }, byaPage.errorPeticion);
        }
        function ConsultarProductos(producto) {
            var send = ActasService.ConsultarProductos(producto.Clase, producto.SubClase, producto.DescripcionMercancia);
            send.then(function (pl) {
                if (!ItemVacio(pl.data)) {
                    console.log(!ItemVacio(pl.data));
                    console.log(pl.data);
                    if (pl.data.DescripcionMercancia === producto.DescripcionMercancia && pl.data.SubClase === producto.SubClase &&
                        pl.data.Clase === producto.Clase && pl.data.Origen === producto.Origen) {
                        producto.Clase = pl.data.Clase;
                        producto.SubClase = pl.data.SubClase;
                        producto.Origen = pl.data.Origen;
                        producto.DescripcionMercancia = pl.data.DescripcionMercancia;
                        producto.UnidadMedida = pl.data.UnidadMedida;
                        producto.Capacidad = pl.data.Capacidad;
                        producto.Alcohol = pl.data.Alcohol;
                        producto.Cantidad = pl.data.Cantidad;
                        producto.Estado = pl.data.Estado;
                        producto.ValorUnitario = pl.data.ValorUnitario;
                        producto.CausalMotivo = pl.data.CausalMotivo;
                    }
                }
            }, byaPage.errorPeticion);
        }
        function armarArchivo() {
            var _form = new FormData();
            _form.append('Fecha', $filter('date')($scope.Archivo.Fecha, 'YYYY-MM-DD'));
            _form.append('Hora', $filter('date')($scope.Archivo.Hora, 'HH:mm:ss'));
            _form.append('Archivo', angular.element("#Archivo").get(0).files[0]); 
            return _form;
        }
    function guardarActa() {
        $scope.Boton.Guardar = true;
        byaPage.Loading();
        var send = ActasService.Guardar(armarArchivo());
        send.then(function (pl) {
            $scope.Boton.Guardar = false;
            byaPage.EndLoading();
            if (!pl.data.Error) {
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
                $scope.Archivo.Nueva = 'NO';
                $scope.Archivo.Id = pl.data.Id;
                if (!$scope.Archivo.Borrador)
                    setTimeout(function () { history.back(); }, 500);

            } else {
                servicioGeneral.addAlert('error','Error!', pl.data.Mensaje);
            }
        }, function (error) {
            byaPage.EndLoading();
            byaPage.errorPeticion(error);
        });
    }
    function guardarActaModificada() {
        $scope.procesando = true;
        byaPage.Loading();
        var _acta = angular.copy($scope.Archivo);
        _acta.HoraAprehension = $filter("date")(_acta.HoraAprehension, 'HH:mm:dd', '-0500');
        var send = ActasService.Modificar(_acta);
        send.then(function (pl) {
            if (!pl.data.Error) {
                nuevaActa();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                $scope.Boton.Siguiente = false;
                setTimeout(function () {
                    window.location.href = "/#/app/archivo/excel/consulta";
                }, 500);

            } else {
                servicioGeneral.addAlert('error', pl.data.Mensaje);
            }
            byaPage.EndLoading();
            $scope.Boton.Guardar = false;
        }, function (error) {
            byaPage.EndLoading();
            byaPage.errorPeticion(error);
        });
    }

    function nuevaActa() {
        var hoy = new Date();
        $scope.Archivo = {
            Fecha: hoy,
            Hora: new Date(0,0,0,hoy.getHours(),hoy.getMinutes(),hoy.getSeconds()),
            Nueva: 'SI',
            Productos: [{}, {}, {}, {}, {}, {}, {}]
        };
    }
    function CampoTextoVacio(campo) {
        if (campo) {
            if (campo !== null) {
                if (campo.length > 0) {
                    if (campo.toString().trim() !== "")
                        return false;
                    else return true;
                } else {
                    return true;
                }
            }
            return true;
        }
        return true;
    }

    function CampoNumeroVacio(campo) {
        if (campo) {
            if ($.isNumeric( campo )) {
                var numero = parseFloat(campo);
                if (numero > 0) {
                    return false;
                } else {
                    return true;
                }
            }
            return true;
        }
        return true;
    }
    function ValidarNuevo(item, indice) {
        var _ultimo = $scope.Archivo.Productos.length - 1;
        if (_ultimo === indice) {
            if (!$.isEmptyObject(item)) {
                if (!ItemVacio(item)) {
                    $scope.Archivo.Productos.push({});
                }
            }
        }
    }
    function eliminarItem(indice) {
        var _ultimo = $scope.Archivo.Productos.length - 1;
        if (_ultimo !== indice) {
            $scope.Archivo.Productos.splice(indice, 1);
        }
    }
    function ItemVacio(item) {
        return CampoTextoVacio(item.Clase) && CampoTextoVacio(item.SubClase) && CampoTextoVacio(item.Origen) && CampoTextoVacio(item.DescripcionMercancia) &&
                    CampoTextoVacio(item.UnidadMedida) && CampoTextoVacio(item.Capacidad) && CampoTextoVacio(item.Alcohol) &&
                    CampoNumeroVacio(item.Cantidad) && CampoNumeroVacio(item.ValorUnitario) &&
                    CampoTextoVacio(item.Estado) && CampoTextoVacio(item.CausalMotivo);
        }
        function actualizarArchivo() {
            if (angular.element("#Archivo").get(0).files.length > 0) {
                console.log(angular.element("#Archivo").get(0).files[0]['name']);
                $scope.Archivo.ArchivoNombre = angular.element("#Archivo").get(0).files[0]['name'];
                angular.element("#ArchivoNombre").val(angular.element("#Archivo").get(0).files[0]['name']);
            }
        }
}]);

app.controller("GestionTerceroCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "TerceroService", "$modal", "servicioGeneral", "EnvioDatosService",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, TerceroService, $modal, servicioGeneral, EnvioDatosService) {
    $scope.Filtro = "";
    $scope.Terceros = [];
    $scope.tableTerceros = {};
    $scope.Identificacion = "";

    $scope.exportData = function () {
        alasql('SELECT *  INTO XLSX("Terceros.xlsx",{headers:true}) FROM ?', [$scope.Terceros]);
    };

    $scope.agregar = function () {
        window.location.href = "/#/app/datos_basicos/terceros/registro_terceros";
    };

    $scope.editar = function (item) {
        window.location.href = "/#/app/datos_basicos/terceros/registro_terceros?tercero_editar=" + Base64.encode(item.Identificacion);
    };

    $scope.eliminar = function (item) {
        if (confirm("Seguro que desea eliminar el registro?")) {
            _eliminar(item);
        }
    };
    $scope._CambiarEstado = function (item) {
        if (item.Estado === "IN") {
            item.Estado = "AC"
        } else {
            item.Estado = "IN"
        }
        $scope.Tercero = item;
        _guardarTerceroModificado()
    };
    function _eliminar(item) {
        var send = TerceroService.PostTercerosEliminar(item);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerTerceros();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert('danger', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    };
    _init();
    function _init() {
        servicioGeneral._nombreTitle("Gestión Terceros");
        servicioGeneral._verificarPermiso("DATOSBASICOS_GestionTerceros");
        _dibujarTablaTerceros();
        _traerTerceros();
        sessionStorage.removeItem("ruta");
    };
    function _guardarTerceroModificado() {
        var send = TerceroService.EstadoTercero($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerTerceros();
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
            } else {
                _traerTerceros();
                servicioGeneral.addAlert('error', 'Error!', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    };
    function _traerTerceros() {
        var send = TerceroService.ObtenerTodos();
        send.then(function (pl) {
            $scope.Terceros = pl.data;
            $scope.tableTerceros.reload();
        }, byaPage.errorPeticion);
    };
    function _dibujarTablaTerceros() {
        $scope.tableTerceros = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Terceros.filter(function (a) {
                    return (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                           (a.TipoIdentidad + "").toLowerCase().indexOf(c) > -1 ||
                           (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1
                })) : f = $scope.Terceros, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
            }
        });
    };
}]);
app.controller("RegistroTercerosCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "TerceroService","funcionService","ByATimeDate", "servicioGeneral",
    function ($scope, $rootScope, $timeout, $window, $location, TerceroService, funcionService, ByATimeDate, servicioGeneral) {
    $scope.Tercero = {};
    $scope.Departamentos = [];
    $scope.Cargando = true;
    $scope.Ruta = ""; // Ruta a la cual debe ir  desde otros impuestos.
    $scope.OcultarCampos = false;
    $scope.Origen = "Tercero";
    $scope.Direccion = {};
    $scope.MensajeOb = {};


    $scope.Boton = { "Guardar": false, "Siguiente": true , "Aceptar" : false};

    $scope.regresar = function () {
        if ($.isEmptyObject($location.search())) {
            window.history.back();
        } else {
            location.href = "/#/app/datos_basicos/terceros/gestion_terceros";
        }
    };

    $scope._CambioTipoPersona = function(){
        if ($scope.Tercero.TipoPersona === "JURIDICA"){
            $scope.Tercero.PrimerNombre = "";
            $scope.Tercero.SegundoNombre = "";
            $scope.Tercero.PrimerApellido = "";
            $scope.Tercero.SegundoApellido = "";
            $scope.Tercero.TipoIdentidad = "NIT";
        }else{
            $scope.Tercero.RazonSocial = "";
            $scope.Tercero.NombreContacto = "";
            $scope.Tercero.TipoIdentidad = "CC";
        }
    };

    $scope._guardar = function () {

        if (!$scope.Tercero.Identificacion) {
            servicioGeneral.addAlert("danger", "Ingrese su identificación.");
            return;
        }

        if (!$scope.Tercero.Email) {
            servicioGeneral.addAlert("danger", "Por favor ingrese el Email ");
            return;
        }

        if (!validarTercero()) return;

        if ($scope.Origen === "OtroImpuestos") {
            if (!$scope.Tercero.Direccion) {
                servicioGeneral.addAlert("danger", "Por favor ingrese la Dirección ");
                return;
            }
        }
        if ($scope.Tercero.Accion === "NUEVO") guardarNuevoTercero();
        else guardarTerceroModificado();
    };

    $scope.calculaDigitoVerificador = function () {
        if (!$scope.Tercero.Identificacion) return;
        $scope.Tercero.DVerificacion = funcionService.DigitoVerificacion($scope.Tercero.Identificacion);
    };

    $scope._volver = function () {
        history.back();
    };
    $scope._limpiar = function () {
        bootbox.confirm({
            message: "¿Seguro que desea limpiar todos los campos?",
            buttons: {
                confirm: {
                    label: 'Aceptar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result == true)
                    nuevoTercero();
            }
        });

    };



    init();
    function init() {

        servicioGeneral._nombreTitle("Registro Terceros");
        servicioGeneral._verificarPermiso("DATOSBASICOS_RegistroTerceros");
        var datos = $location.search();
        if (datos.tercero_editar == null) {
            nuevoTercero();
        } else {
            var identificacion = Base64.decode(datos.tercero_editar);
            getTercero(identificacion);
        }
        $scope.Cargando = false;
    };

    function initRuta() {
        var datosLocal = sessionStorage.getItem("ruta");
        if (datosLocal) {
            var obj = JSON.parse(Base64.decode(datosLocal));
            $scope.Ruta = obj.Ruta;
            $scope.OcultarCampos = true;
            $scope.Origen = "OtroImpuestos";
        }
    }
    $rootScope.$on("iniciarRuta", function (event, data) {
        initRuta();
    });


    $scope.Editar = true;;
    function inhabilitarcampos() {
        $scope.Editar = false;
    };
    function habilitarcampos() {
        $scope.Editar = true;
    };

    $scope.CerrarModal = function () {
        $('#mdMensaje').modal('hide');
        inhabilitarcampos();
    };



    function validarTercero() {
        if ($scope.Tercero.TipoIdentidad === "NIT") {
            if (!$scope.Tercero.RazonSocial || $scope.Tercero.RazonSocial === "") {
                servicioGeneral.addAlert("warning", "Ingrese la razón social.");
                return false;
            }
            if (!$scope.Tercero.NombreComercial || $scope.Tercero.NombreComercial === "") {
                servicioGeneral.addAlert("warning", "Ingrese el nombre comercial.");
                return false;
            }
            if (!$scope.Tercero.NombreContacto || $scope.Tercero.NombreContacto === "") {
                servicioGeneral.addAlert("warning", "Ingrese el nombre de contacto.");
                return false;
            }

            if (!$scope.Tercero.Email || $scope.Tercero.Email === "") {
                servicioGeneral.addAlert("warning", "Ingrese la razón social.");
                return false;
            }
        } else {
            if (!$scope.Tercero.PrimerNombre || $scope.Tercero.PrimerNombre === "") {
                servicioGeneral.addAlert("warning", "Ingrese el primer nombre.");
                return false;
            }
            if (!$scope.Tercero.PrimerApellido || $scope.Tercero.PrimerApellido === "") {
                servicioGeneral.addAlert("warning", "Ingrese el primer apellido.");
                return false;
            }
        }
        return true;
    }
   function getTercero(identificacion) {
        var send = TerceroService.Obtener(identificacion);
        send.then(function (pl) {
            if (pl.data) {
                $scope.Tercero = pl.data;
                $scope.Tercero.FechaExpedicion = moment(ByATimeDate.Fecha(pl.data.FechaExpedicion)).format(''); //new Date(pl.data.FechaExpedicion);
                $scope.Tercero.Accion = "EDITAR";
            }
        }, byaPage.errorPeticion);
    }
    function guardarNuevoTercero() {
        $scope.Boton.Guardar = true;
        byaPage.Loading();
        var send = TerceroService.Guardar($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                nuevoTercero();
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
                if ($scope.Origen === "OtroImpuestos") {
                    $scope.$parent.steps.percent = 97;
                    $scope.Boton.Aceptar = true;
                    $scope.MensajeOb.Nota1 = "Estimado Usuario(a), sus datos han sido guardados correctamente," +
                        "  por favor haga click en el botón aceptar para continuar.";
                    $("#mdCargando").modal("show");
                    $scope.Boton.Siguiente = false;

                } else {
                    setTimeout(function () { history.back(); }, 500);
                }

            } else {
                servicioGeneral.addAlert('error','Error!', pl.data.Mensaje);
            }

            $scope.Boton.Guardar = false;
            byaPage.EndLoading();
        }, byaPage.errorPeticion);
    };
    function guardarTerceroModificado() {
        $scope.procesando = true;
        byaPage.Loading();
        var send = TerceroService.Modificar($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                nuevoTercero();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                $scope.Boton.Siguiente = false;
                setTimeout(function () {
                    window.location.href = "/#/app/datos_basicos/terceros/gestion_terceros";
                }, 500);

            } else {
                servicioGeneral.addAlert('error', pl.data.Mensaje);
            }
            byaPage.EndLoading();
            $scope.Boton.Guardar = false;
        }, byaPage.errorPeticion);
    };

    function nuevoTercero() {
        $scope.Tercero = {
            TipoIdentidad: "CC",
            TipoPersona: "NATURAL",
            Pais: "COLOMBIA",
            Beneficiarios: "NO",
            Regimen: "COMUN",
            GranContribuyente: "NO",
            Accion: "NUEVO"
        };
        habilitarcampos();
    };
    $scope.CambioDepartamento = function () {
        $scope.Departamentos.Seleccionado.Municipios.Seleccionado = $scope.Departamentos.Seleccionado.Municipios[0];
        $scope.Tercero.Departamento = $scope.Departamentos.Seleccionado.NombreDepartamento;
        $scope.Tercero.Ciudad = $scope.Departamentos.Seleccionado.Municipios.Seleccionado.NombreMunicipio;
    };
    $scope.CambioMunicipio = function () {
        $scope.Tercero.Ciudad = $scope.Departamentos.Seleccionado.Municipios.Seleccionado.NombreMunicipio;
    };

    $scope.mostrarDireccion = function (objetoContenedor) {
        $("#modalDireccion").modal("show");
        nuevaDireccion();
    };

    $scope.ocultarDireccion = function () {
        $("#modalDireccion").modal("hide");
    };

    function nuevaDireccion() {
        $scope.Direccion.Generada = "";
        $scope.Direccion.Complemento = "";
        $scope.Direccion.tipoViaPrimaria = "";
        $scope.Direccion.numeroViaPrincipal = "";
        $scope.Direccion.letraViaPrincipal = "";
        $scope.Direccion.bis = "";
        $scope.Direccion.letraBis = "";
        $scope.Direccion.cuadranteViaPrincipal = "";
        $scope.Direccion.numeroViaGeneradora = "";
        $scope.Direccion.letraViaGeneradora = "";
        $scope.Direccion.numeroPlaca = "";
        $scope.Direccion.cuadranteViaGeneradora = "";
        $scope.Direccion.componenteComplemento = "";
        $scope.Direccion.valorComplemento = "";
    }
    $scope.DireccionPrincipal = function () {
        if ($scope.Direccion.tipoViaPrimaria == undefined)
            $scope.Direccion.tipoViaPrimaria = "";

        if ($scope.Direccion.numeroViaPrincipal == undefined) {
            $scope.Direccion.numeroViaPrincipal = "";
        }
        if ($scope.Direccion.letraViaPrincipal == undefined) {
            $scope.Direccion.letraViaPrincipal = "";
        }
        if ($scope.Direccion.bis == undefined) {
            $scope.Direccion.bis = "";
        }
        if ($scope.Direccion.letraBis == undefined) {
            $scope.Direccion.letraBis = "";
        }
        if ($scope.Direccion.cuadranteViaPrincipal == undefined) {
            $scope.Direccion.cuadranteViaPrincipal = "";
        }
        if ($scope.Direccion.numeroViaGeneradora == undefined) {
            $scope.Direccion.numeroViaGeneradora = "";
        }
        if ($scope.Direccion.letraViaGeneradora == undefined) {
            $scope.Direccion.letraViaGeneradora = "";
        }
        if ($scope.Direccion.numeroPlaca == undefined)
            $scope.Direccion.numeroPlaca = "";

        if ($scope.Direccion.cuadranteViaGeneradora == undefined) {
            $scope.Direccion.cuadranteViaGeneradora = "";
        }
        if ($scope.Direccion.Complemento == undefined) {
            $scope.Direccion.Complemento = "";
        }


        $scope.Direccion.Generada = $scope.Direccion.tipoViaPrimaria;

        if ($scope.Direccion.tipoViaPrimaria !== "AV") {

            if ($scope.Direccion.numeroViaPrincipal !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.numeroViaPrincipal;
            }
            if ($scope.Direccion.letraViaPrincipal !== "") {
                $scope.Direccion.Generada += $scope.Direccion.letraViaPrincipal;
            }

            if ($scope.Direccion.bis !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.bis;
            }

            if ($scope.Direccion.letraBis !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.letraBis;
            }


            if ($scope.Direccion.cuadranteViaPrincipal !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.cuadranteViaPrincipal;
            }

        }
        if ($scope.Direccion.numeroViaGeneradora != "") {
            $scope.Direccion.Generada += " " + $scope.Direccion.numeroViaGeneradora;
        }

        if ($scope.Direccion.letraViaGeneradora != "") {
            $scope.Direccion.Generada += $scope.Direccion.letraViaGeneradora;
        }

        if ($scope.Direccion.numeroPlaca != "") {
            var cero = ""
            /*if (this.document.frmDireccionNotificacion.numeroPlaca.value<10)
            {
            cero="0";
            }*/
            $scope.Direccion.Generada += " " + cero + $scope.Direccion.numeroPlaca;
        }
        if ($scope.Direccion.cuadranteViaGeneradora != "") {
            $scope.Direccion.Generada += " " + $scope.Direccion.cuadranteViaGeneradora;
        }

    };

    $scope.ValidarCuadranteViaGeneradora = function () {
        if ($scope.Direccion.cuadranteViaPrincipal == $scope.Direccion.cuadranteViaGeneradora) {
            $scope.Direccion.cuadranteViaPrincipal = "";
            $scope.Direccion.cuadranteViaGeneradora = "";
        }
        $scope.DireccionPrincipal();
    }

    $scope.ValidarCuadrantePrincipal = function () {
        if ($scope.Direccion.tipoViaPrimaria == "AC" || $scope.Direccion.tipoViaPrimaria == "CL" || $scope.Direccion.tipoViaPrimaria == "DG") {
            if ($scope.Direccion.cuadranteViaPrincipal != "SUR") {
                $scope.Direccion.cuadranteViaPrincipal = "";
            }
        } else {
            if ($scope.Direccion.cuadranteViaPrincipal != "ESTE") {
                $scope.Direccion.cuadranteViaPrincipal = "";
            }
        }
        $scope.DireccionPrincipal();
    }

    $scope.BorrarDireccion = function () {
        $scope.Direccion.Generada = "";
        $scope.Direccion.Complemento = "";
    }

    $scope.SetFocusDireccion = function () {
        $("#txtTerceroDireccion").focus();
    }
    $scope.GenerarDireccion = function () {
        $scope.Tercero.Direccion = $scope.Direccion.Generada;
        $scope.ocultarDireccion();
        $("#txtTerceroDireccion").focus();
    }

    $scope.ComplementoDireccion = function () {
        $scope.Direccion.Generada += " " + $scope.Direccion.componenteComplemento + " " + $scope.Direccion.valorComplemento;
        $scope.Direccion.valorComplemento = "";
    };
}]);


app.controller("ActualizarFirmaCtrl", ["$scope", "$timeout", "FirmaTerceroService", "servicioGeneral", "EnvioDatosService",
    function ($scope, $timeout, FirmaTerceroService, servicioGeneral, EnvioDatosService) {

        $scope.guardarFirmaTercero = function () {
            guardarFirmaTercero($scope.Firma);
        };
        $scope.buscarTercero = function () {

        };
        $scope.addImage = function (files) {
            if (!window.FileReader) {
                servicioGeneral.addAlert('danger', 'El navegador no soporta la lectura de archivos');
            }
            var file = files[0],
                imageType = /image.*/;
            if (!file.type.match(imageType)) {
                servicioGeneral.addAlert('warning', 'El archivo a adjuntar no es una imagen');
                return;
            }
            var img = new Image();
            var error = false;
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
        };
        $scope.cerrarModal = function () {
            servicioGeneral.cerrarModal();
        };
    function fileOnload(e) {
        var result = e.target.result;
        $scope.Firma.Firma = result;
        $('#firmaTercero').attr("src", result);
    };
    _init();
    function _init() {
        EnvioDatosService.data.inicializar = inicializar;
    };
    function inicializar() {
        servicioGeneral._verificarPermiso("CONFIGURACION_ActualizarFirma", regresar);
        //inicializarValidaAltoAnchoImagen();
        verificarTercero();
    }
    function inicializarValidaAltoAnchoImagen() {
        $('#firmaTercero').load(function () {
            if (this.width > 300 || this.width < 200) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                servicioGeneral.addAlert('warning', 'El ancho debe estar en el rango de 200px a 250px');
                this.removeAttribute('src');
                $scope.$apply();
            }
            else if (this.height > 160 || this.height < 70) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                servicioGeneral.addAlert('error', '¡Aviso!', 'El alto debe estar en el rango de 90px a 120px');
                this.removeAttribute('src');
                $scope.$apply();
            }
        });
    }
    function GetObjeto() {
        var formData = new FormData();
        formData.append("Identificacion", $scope.Firma.Identificacion);
        formData.append("Firma", $("#textArchivos").get(0).files[0]);
        return formData;
    }
    function guardarFirmaTercero() {
        var send = FirmaTerceroService.Guardar(GetObjeto());
        send.then(function (pl) {
            regresar();
            servicioGeneral.addAlert('success', pl.data.Mensaje);
        }, byaPage.errorPeticion);
    };

    function buscarTercero(identificacion) {
        var send = FirmaTerceroService.ObtenerTercero(identificacion);
        send.then(function (pl) {
            $scope.Firma.Identificacion = pl.data.Identificacion;
            $scope.Firma.NombreCompleto = pl.data.NombreCompleto;
        }, byaPage.errorPeticion);
    };
    function verificarTercero() {
        var tercero = EnvioDatosService.data.Tercero;
        $scope.Firma = {};
        if (tercero) {
            $scope.Firma.Identificacion = tercero.Identificacion;
            $scope.Firma.NombreCompleto = tercero.NombreCompleto;
            $scope.Firma.Deshabilitar = true;
        } else {
            regresar();
        }
    }
    function regresar() {
        servicioGeneral.cerrarModal();
    }
    
}]);
app.controller("ConsultarFirmasCtrl", ["$scope", 'TercerosService', 'ContribuyentesService', "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "TercerosService", "toaster", function ($scope, TercerosService, ContribuyentesService, $rootScope, $timeout, $window, $location, ngTableParams, TercerosService, toaster) {
    $scope.Filtro = "";
    $scope.Peticion = "false";
    $scope.Firmas = [];
    $scope.tableTerceros = {};
    $scope.Identificacion = "";

    $scope.agregar = function () {
        $scope.Firma = {};
        $('#firmaTercero').removeAttr("src");
        $("#firma-tercero input[type=text]").val('');
        $("#textArchivos").val("");
        $("#mdMensaje").modal("show");

    }
    $scope.eliminar = function (item) {
        if (confirm("Seguro que desea eliminar el registro?")) {
            _eliminar(item.TerceroFirmaID);
        }
    };
    $scope.guardar = function () {
        _guardarFirmaTercero($scope.Firma);
    }
    $scope.addImage = function (files) {
        if (!window.FileReader) {
            toaster.pop('error', '¡Aviso!', 'El navegador no soporta la lectura de archivos');
        }
        var file = files[0],
       imageType = /image.*/;
        if (!file.type.match(imageType)) {
            toaster.pop('error', '¡Aviso!', 'El archivo a adjuntar no es una imagen');
            return;
        }
        var img = new Image();
        var error = false;
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    };
    $scope.buscarTercero = function (firma) {
        //var filtro = (firma.NombreCompleto ? firma.NombreCompleto : "") + "," + (firma.Identificacion ? firma.Identificacion : "");
        var filtro = "," + (firma.Identificacion ? firma.Identificacion : "");
        _traerTercerosFiltro(filtro);
    }
    function fileOnload(e) {
        var result = e.target.result;
        $scope.Firma.Firma = result;
        $('#firmaTercero').attr("src", result);
        
    };
    function _eliminar(id) {
        var send = TercerosService.PostEliminarFirmaTerceros(id);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerFirmasTerceros();
                toaster.pop('success', '¡Eliminado!', pl.data.Mensaje);
            } else {
                toaster.pop('error', '¡Error!', pl.data.Mensaje);
            }
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    _init();
    function _init() {
        $rootScope.Page.Title = "Gestión Firmas de Terceros";
        $rootScope._verificarPermiso("CONFFirmaTerceros");
        _dibujarTablaTerceros();
        _traerFirmasTerceros();
        inicializarValidaAltoAnchoImagen();
    };
    function inicializarValidaAltoAnchoImagen() {
        $('#firmaTercero').load(function () {
            if (this.width > 300 || this.width < 200) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                toaster.pop('error', '¡Aviso!', 'El ancho debe estar en el rango de 200px a 250px');
                this.removeAttribute('src');
                $scope.$apply();
            }
            else if (this.height > 160 || this.height < 70) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                toaster.pop('error', '¡Aviso!', 'El alto debe estar en el rango de 90px a 120px');
                this.removeAttribute('src');
                $scope.$apply();
            }
        });
    }
    function _guardarFirmaTercero() {
        var send = TercerosService.PostCrearTercerosFirma($scope.Firma);
        send.then(function (pl) {
            toaster.pop(!pl.data.Error ? 'success' : 'error', !pl.data.Error ? 'Guardado!' : 'Error!', pl.data.Mensaje);
            _traerFirmasTerceros();
            if (!pl.data.Error)
                $("#mdMensaje").modal("hide");
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    function _traerFirmasTerceros() {
        $scope.Peticion = "false";
        var send = TercerosService.GetFirmaTercero();
        send.then(function (pl) {
            $scope.Firmas = pl.data;
            $scope.tableTerceros.reload();
            $scope.Peticion = "true";
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    function _traerTercerosFiltro(filtro) {
        $scope.Peticion = "false";
        var send = TercerosService.GetFiltroTercero(filtro);
        send.then(function (pl) {
            asignarTerceroEnDormulario(pl.data);
            console.log($scope.Terceros)
            $scope.Peticion = "true";
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    function asignarTerceroEnDormulario(terceros) {
        if (terceros.length > 0) {
            $scope.Firma.NombreCompleto = terceros[0].NombreCompleto;
            $scope.Firma.Identificacion = terceros[0].Identificacion;
            $scope.Firma.TerceroID = terceros[0].TerceroID;
        } else {
            toaster.pop('error', 'Información!', "El tercero no ha sido encontrado en el sistema");
        }
    }
    function _dibujarTablaTerceros() {
        $scope.tableTerceros = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            total: 1000,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Firmas.filter(function (a) {
                    return (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                           (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1
                })) : f = $scope.Firmas, f = f, a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
            }
        });
    };

    function _muestraMensaje(titulo, mensaje, tipo) {
        PNotify.prototype.options.styling = "fontawesome";
        //alert(PNotify.prototype.options.styling);
        new PNotify({
            title: titulo,
            text: mensaje,
            type: tipo
        });
    };
    
}]);
app.controller("GestionLibradoresCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "servicioGeneral", "GestionLibradorService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, servicioGeneral, GestionLibradorService, $filter) {
        $scope.tableLibradores = {};
        $scope.libradores = [];
        $scope.irAAgregarLibrador = function () {
            localStorage.removeItem("IdentificacionRegistroLibrador");
            window.location.href = "/#/app/datos_basicos/librador/registro";
        };
        $scope.editarLibrador = function (item) {
            localStorage.setItem("IdentificacionRegistroLibrador", item.Identificacion);
            window.location.href = "/#/app/datos_basicos/librador/registro";
        };
        $scope.eliminarLibrador = function (item) {
            item.EsLibrador = false;
            var send = GestionLibradorService.GuardarLibrador(item);
            send.then(function (pl) {
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                _traerLibradores();
            }, byaPage.errorPeticion);
        };

        _init();
        function _init() {
            servicioGeneral._nombreTitle("Gestión de libradores");
            servicioGeneral._verificarPermiso("DATOSBASICOS_RegistroDeLibrador");
            _dibujarTablaLibradores();
            _traerLibradores();
        };
        function _traerLibradores() {
            var send = GestionLibradorService.ObtenerLibradores();
            send.then(function (pl) {
                $scope.libradores = pl.data;
                $scope.tableLibradores.reload();
            }, byaPage.errorPeticion);
        };
        function _dibujarTablaLibradores() {
            $scope.tableLibradores = new ngTableParams({
                page: 1,
                count: 100
            }, {
                filterDelay: 50,
                getData: function (a, b) {
                    var c = b.filter().Filtro,
                        f = [];
                    c ? (c = c.toLowerCase(), f = $scope.libradores.filter(function (a) {
                        
                        return (a.TipoIdentificacion + "").toLowerCase().indexOf(c) > -1 ||
                               (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                               (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1 ||
                               (a.Telefono + "").toLowerCase().indexOf(c) > -1 
                    })) : f = $scope.libradores, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
                }
            });
        };
    }
]);
app.controller("RegistroLibradorCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "servicioGeneral", "RegistroLibradorService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, servicioGeneral, RegistroLibradorService, $filter) {
        $scope.Tercero = {};
        $scope.DatosInicialesFormulario = {};
        $scope.botonesHabilitados = {};
        
        $scope.volverAtras = function () {
            history.back();
        };
        $scope.buscarTercero = function () {
            if ($scope.Tercero.Identificacion != null && $scope.Tercero.Identificacion != "") {
                $scope.botonesHabilitados.todos = false;
                var send = RegistroLibradorService.ObtenerTercero($scope.Tercero.Identificacion);
                send.then(function (pl) {
                    $scope.botonesHabilitados.todos = true;
                    $scope.Tercero = pl.data;
                }, function (error) {
                    $scope.botonesHabilitados.todos = true;
                    byaPage.errorPeticion(error);
                });
            }
        };
        $scope.cambiarCuentaLibrador = function (index, item) {
            if (item.EsCuentaLibrador) {
                $.each($scope.Tercero.CuentasBancarias, function (index2, item2) { 
                    if (index != index2) item2.EsCuentaLibrador = false;
                });
            }
        };
        $scope.esValidoGuardarTercero = function () {
            if ($scope.Tercero.CuentasBancarias != null && $scope.Tercero.CuentasBancarias.length > 0) {
                console.log($scope.Tercero.CuentasBancarias);
                var cuentasSeleccionadas = $scope.Tercero.CuentasBancarias.filter(function (o) { return o.EsCuentaLibrador; });
                if (cuentasSeleccionadas.length == 1) return true;
                else return false;
            } else return false;
        };
        $scope.guardarTercero = function () {
            $scope.botonesHabilitados.todos = false;
            $scope.Tercero.EsLibrador = true;
            var send = RegistroLibradorService.GuardarLibrador($scope.Tercero);
            send.then(function (pl) {
                $scope.botonesHabilitados.todos = true;
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                $scope.buscarTercero();
            }, function (error) {
                $scope.botonesHabilitados.todos = true;
                byaPage.errorPeticion(error);
            });
        };

        _init();
        function _init() {
            servicioGeneral._nombreTitle("Registro librador");
            servicioGeneral._verificarPermiso("DATOSBASICOS_RegistroDeLibrador");
            _traerDatosInicialesFormulario();
            _identificarOperacionARealizar();
        };
        function _inicializarBotonesHabilitados() {
            $scope.botonesHabilitados.todos = true;
        };
        function _traerDatosInicialesFormulario() {
            $scope.botonesHabilitados.todos = false;
            var send = RegistroLibradorService.ObtenerDatosInicialesFormulario();
            send.then(function (pl) {
            $scope.botonesHabilitados.todos = true;
                $scope.DatosInicialesFormulario = pl.data;
            }, function (error) {
                $scope.botonesHabilitados.todos = true;
                byaPage.errorPeticion(error);
            });
        };
        function _identificarOperacionARealizar() {
            var identificacion = localStorage.getItem("IdentificacionRegistroLibrador");
            if (identificacion != null || identificacion != "") {
                $scope.Tercero.Identificacion = identificacion;
                $scope.buscarTercero();
            }
        };
    }]);
app.controller("ConsultaAuditoriaCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "EstadosSistema", "servicioGeneral", "AuditoriaService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, EstadosSistema, servicioGeneral, AuditoriaService, $filter) {
    $scope.Auditorias = [];
    $scope.Estados = {};
    $scope.filtros = {};
    $scope.tableAuditoria = {};
    $scope.ConsultarAuditorias = function () {
        ConsultarAuditorias($scope.filtros);
    };
    $scope.MostrarDetalles = function (item) {
        ConsultarDetalles(item.TableName, item.RecordID);
    };
    $scope.cambiarEspaciosEnBlanco = function (cadena) {
        cadena = $filter("uppercase")(cadena);
        while (cadena.indexOf(' ') > -1)
            cadena = cadena.replace(' ', '&nbsp;');
        return cadena;
    };
    $scope.mostrarObjeto = function (item) {
        $scope.objeto = { TableName: item.TableName, RecordID: item.RecordID, detalles: JSON.parse(item.NewValue) };
        $("#modalDetalles").modal("show");
    }
    $scope.verDetalles = function (item) {
        $scope.objeto.objetoActual = JSON.parse(item.NewValue);
        $scope.objeto.Ver = 'SI';
    };
    _init();
    function _init() {
        _dibujarTablaAnotaciones();
        ConsultarDatosFormulario();
        servicioGeneral._nombreTitle("Consulta de registro de auditoría");
        servicioGeneral._verificarPermiso("SEGURIDAD_ConsultaAuditoria");
    };

    //////////////////////////////////////////METODOS/////////////////////////////////////////////////////////////////////
    function ConsultarDatosFormulario() {
        var send = AuditoriaService.ObtenerDatos();
        send.then(function (pl) {
            $scope.Modulos = pl.data.Modulos;
            $scope.Acciones = pl.data.Acciones;
            $scope.Tablas = pl.data.Tablas;
        }, function (error) {
            byaPage.errorPeticion(error);
        });
    };
    function ConsultarAuditorias(Request) {
        $scope.Auditorias = [];
        var send = AuditoriaService.Consultar(Request);
        send.then(function (pl) {
            if (!pl.data.Error) {
                $scope.Auditorias = pl.data;
                $scope.tableAuditoria.reload();
            } else {
                servicioGeneral.addAlert('danger', pl.data.Mensaje);
            }
            byaPage.EndLoading();
        }, function (error) {
            $scope.tableAuditoria.reload();
            byaPage.errorPeticion(error);
        });
    }
    function ConsultarDetalles(Tabla, RecorID) {
        $scope.Detalles = [];
        var send = AuditoriaService.Detalles(Tabla, RecorID);
        send.then(function (pl) {
            $scope.objeto = { TableName: Tabla, RecordID: RecorID, Detalles: [], Ver: 'NO' };
            $("#modalDetallesRegistro").modal("show");
            $scope.objeto.detalles = pl.data;
            console.log(pl.data);
        }, function (error) {
            byaPage.errorPeticion(error);
        });
    }
    function _dibujarTablaAnotaciones() {
        $scope.tableAuditoria = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Auditorias.filter(function (a) {
                    return (a.Id + "").toLowerCase().indexOf(c) > -1 ||
                        (a.IdentificacionContratista + "").toLowerCase().indexOf(c) > -1 ||
                        (a.NombreContratista + "").toLowerCase().indexOf(c) > -1 ||
                        (a.FechaInicio + "").toLowerCase().indexOf(c) > -1 ||
                        (a.ValorLibranza + "").toLowerCase().indexOf(c) > -1 ||
                        (a.NumeroCuotas + "").toLowerCase().indexOf(c) > -1 ||
                        (a.SaldoDisponible + "").toLowerCase().indexOf(c) > -1;
                })) : f = $scope.Auditorias, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()));
            }
        });
    };
}]);
'use strict';

angular.module('app')
    .controller('ChangePasswordCtrl', ['$scope', 'AccountService', 'servicioGeneral', function ($scope, AccountService, servicioGeneral) {
        $scope.cpassword = '';
        $scope.cuenta = {
            OldPassword: undefined,
            NewPassword: undefined,
            ConfirmPassword: undefined,
            userName: byaSite.getUsuario()
        };
        $scope.EnviarCuenta = function () {
            var promiseGet = AccountService.ChangePassword($scope.cuenta);
            promiseGet.then(function (m) {
                servicioGeneral.addAlert("success", "Se ha cambiado la contraseña con exito!!.");
                console.log(m.data);
            }, byaPage.errorPeticion);
        };

    }]);
'use strict';

angular.module('app')
    .controller('ChangePasswordCtrlUpagos', ['$scope', 'AccountService', 'servicioGeneral', function ($scope, AccountService, servicioGeneral) {
        $scope.cpassword = '';
        $scope.cuenta = {
            OldPassword: undefined,
            NewPassword: undefined,
            ConfirmPassword: undefined,
            userName: byaSite.getUsuarioPagos()
        };


        $scope.EnviarCuenta = function () {
            var promiseGet = AccountService.ChangePassword($scope.cuenta);
            promiseGet.then(function (m) {
                servicioGeneral.addAlert("success", "Se ha cambiado la contraseña con exito!!.");
                console.log(m.data);
            }, byaPage.errorPeticion);
        };
    }]);
'use strict';
app.controller('LoginCtrl', ['$rootScope', '$scope', 'loginService', 'UsuarioService', 'servicioGeneral', 'AccountService',
    function ($rootScope, $scope, loginService, UsuarioService, servicioGeneral, AccountService) {
    $scope.User = {};
    $scope.alerts = [];

    $scope.login = function () {
        if (_esValido()) {
            var send = loginService.getToken($scope.User.Identificacion, $scope.User.Password);
            send.then(function (pl) {
                if (pl.data.access_token != null && pl.data.access_token != "") {
                    byaSite.setUsuario($scope.User.Identificacion);
                    byaSite.setTokenAdm(pl.data.access_token);
                    if (pl.data.Name) {
                        byaSite.setNombreUsuario(pl.data.Name);
                        byaSite.setTipoIdentificacion(pl.data.TipoIdentificacion);
                        
                    }
                    $scope.User = {};
                    _traerRolesUsuario();
                } else {
                    servicioGeneral.addAlert("danger", "Usuario o Contraseña incorrecto!!");
                }
            },  byaPage.errorPeticion);
        }
    };
    $scope.EnviarEmail = function () {
        $("#enviarReset").hide();
        var promiseGet = AccountService.ForgotPassword($scope.ForgotPasswordViewModel);
        promiseGet.then(function (m) {
            $("#enviarReset").hide();
            servicioGeneral.addAlert("success", "Se ha enviado un mensaje para reestablecer su contraseña. Por favor verifique la bandeja de entrada de su correo electrónico.");
        },  byaPage.errorPeticion);
    };
    function _esValido() {
        if ($scope.User.Identificacion != null && $scope.User.Identificacion != "" && $scope.User.Password != null && $scope.User.Password != "") {
            return true;
        } else {
            servicioGeneral.addAlert("danger", "Los campos marcados con '*' son obligatorios");
            return false;
        }
    };

    function irAOpcionPRincipal(opciones) {
        var opcion = opciones[0];
        if (opcion.Habilitado == 1) {
            return opcion.Url;
        } else {
            return "/#/app/inicio";
        }
    };

    function _traerRolesUsuario() {
        var ser = UsuarioService.GetRolesPersona(byaSite.getUsuario());
        ser.then(function (pl) {
            servicioGeneral.Roles = pl.data;
            $rootScope.lRolesPersona = pl.data;
            if ($rootScope.lRolesPersona.length > 0) {
                window.location.href = irAOpcionPRincipal($rootScope.lRolesPersona);
            } else {
                servicioGeneral.addAlert("danger","El usuario no tiene autorización para entrar");
                $rootScope.lRolesPersona = [];
            }
        }, byaPage.errorPeticion);
    };
}]);
'use strict';

angular.module('app')
    .controller('ResetPasswordCtrl', ['$scope', 'AccountService', 'toastr','servicioGeneral', function ($scope, AccountService, toastr,servicioGeneral) {
        $scope.Restablecido = false;
        $scope.cpassword = '';
        $scope.cuenta = {
            UserId: undefined,
            Code: undefined,
            Password: undefined,
            Email: undefined,
            ConfirmPassword: undefined,
        };
        

        $scope.EnviarCuenta = function () {
            $scope.Restablecido = true;
            var promiseGet = AccountService.ResetPassword($scope.cuenta);
            promiseGet.then(function (m) {
                servicioGeneral.addAlert("success", "Se ha restablecido la contraseña con exito!!.");
                console.log(m.data);
            }, byaPage.errorPeticion);
        };

        init();

        function init() {
            $scope.cuenta.UserId = $.urlParam('user');
            $scope.cuenta.Code = $.urlParam('code');
        }
}]);
app.controller('GestionUsuariosCtrl', ['$scope', '$rootScope', 'UsuarioService', 'ngTableParams', 'ByATimeDate', 'servicioGeneral', 'TerceroService',
    function ($scope, $rootScope, UsuarioService, ngTableParams, ByATimeDate, servicioGeneral, TerceroService) {
        $scope.opened = false;
    $scope.peticion = false;
    $scope.Filtro = "";
    $scope.Usuarios = [];
    $scope.tableUsuarios = {};

    $scope.tercerosCambioContraseña = {};
    $scope.Usuario = {};
    $scope.Usuario.FechaDesactivacion = ByATimeDate.Now();
    
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.cambiarPassword = function (usuario) {
        $scope.terceroCambioPassword = usuario;
        $("#modalCambiarContraseña").modal("show");
    };

    $scope.buscarTercero = function () {
        if ($scope.Usuario && $scope.Usuario.UserName)
            buscarTercero($scope.Usuario.UserName);
    };
    function buscarTercero(username) {
        var send = TerceroService.Obtener(username);
        send.then(function (pl) {
            $scope.TerceroEncontrado = pl.data;
            if ($scope.TerceroEncontrado != null) {
                $scope.Usuario.Tercero = pl.data.NombreCompleto;
                $scope.Usuario.Email = pl.data.Email;
                UsuarioRegistrado($scope.Usuario);
            } else {
                $scope.Usuario.Tercero = "";
            }
        }, byaPage.errorPeticion);
    };
    $scope.enviarCambioPassword = function () {
        $("#modalCambiarContraseña").modal("hide");
        byaPage.Loading("Cambiando contraseña", "Por favor, espere...");
        var serUsuario = UsuarioService.PostForzar($scope.terceroCambioPassword);
        serUsuario.then(function (pl) {
            if (!pl.data.Error) {
                $("#modalCambiarContraseña").modal("hide");
                byaPage.EndLoading();
                servicioGeneral.addAlert("success", pl.data.Mensaje);
            } else {
                byaPage.EndLoading();
                $("#modalCambiarContraseña").modal("show");
            }
        }, byaPage.errorPeticion);
    };
    $scope.nuevoTercero = function () {
        window.location.href = "/#/app/Datosbasicos/terceros/registro_terceros";
    };
    $scope.VerificarEmail = function () {
        UsuarioRegistrado($scope.Usuario);
    };
    $scope.nuevoUsuario = function () {
        var FechaActual = ByATimeDate.Now();
        $scope.Usuario = { FechaDesactivacion: FechaActual.getFullYear() + "-12-31" };
        $("#modalNuevoUsuario").modal("show");
    };

    $scope._cambiarFechaDesactivar = function (item) {
        $scope.peticion = true;
    };
    $scope._guardarNuevo = function () {
        $scope.peticion = true;
        if ($scope.TerceroEncontrado != null) {
            if ($scope.Usuario.Password && $scope.Usuario.Password != null) {
                if ($scope.Usuario.Password != $scope.Usuario.PasswordConfirm) {
                    servicioGeneral.addAlert("danger", "Las contraseñas no coinciden.");
                } else if ($scope.Usuario.Password.length < 6) {
                    servicioGeneral.addAlert("danger", "Las contraseñas debe poseer por lo menos 6 caracteres.");
                } else {
                    guardarNuevoUsuario();
                }
            } else {
                servicioGeneral.addAlert("danger", "No ha ingresado una contraseña válida.");
            }
        } else {
            servicioGeneral.addAlert("danger", "La identificación que ha indicado no corresponde a ningún tercero registrado en el sistema.");
        }
        $scope.peticion = false;
    };

    function UsuarioRegistrado(usuario) {
        var send = UsuarioService.UsuarioRegistrado(usuario);
        send.then(function (pl) {
            if (pl.data.Error) {
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
            } else {
                usuario.Deshabilitar = false;
            }
        }, byaPage.errorPeticion)
    };
    function guardarNuevoUsuario() {
        $("#modalNuevoUsuario").modal("hide");
        byaPage.Loading("Guardando", "Por favor, espere...");
        $scope.peticion = true;
        var serUsuario = UsuarioService.Guardar($scope.Usuario);
        serUsuario.then(function (pl) {
            if (pl.data.Error == false) {
                $("#modalNuevoUsuario").modal("hide");
                byaPage.EndLoading();
                servicioGeneral.addAlert("success", pl.data.Mensaje);
            } else {
                $scope.peticion = false;
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
                byaPage.EndLoading();
                setTimeout(function () { $("#modalNuevoUsuario").modal("show"); }, 500);
            }
            _traerUsuarios();
        }, byaPage.errorPeticion);
    }
    $scope.mostrarFecha = function (fecha, op) {
        $scope.FechaFinal = fecha;
    };
    $scope._updateSesion = function (item) {
        byaPage.Loading("Cambiando estado", "Por favor, espere...");
        var send = UsuarioService.UpdateSesionUsuario(item.UserName);
        send.then(function (pl) {
            byaPage.EndLoading();
            _traerUsuarios();
            servicioGeneral.addAlert("success", pl.data.Mensaje);
        }, byaPage.errorPeticion)
    }
    $scope.ReSendConfirmationEmail = function (item) {
        byaPage.Loading("Enviando email", "Por favor, espere...");
        var send = UsuarioService.ReSendConfirmationEmail(item.UserName);
        send.then(function (pl) {
            byaPage.EndLoading();
            if (!pl.data.Error) {
                servicioGeneral.addAlert("sussess", pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
            }
        }, byaPage.errorPeticion)
    };
    $scope._irCambiarPassword = function (item) {
        $scope.Usuario = {};
        $scope.Usuario.UserName = item.UserName;
        $scope.Usuario.Tercero = (item.Tercero ? (item.Tercero.trim().lenght == 0 ? 'No Asignado' : item.Tercero) : 'No Asignado');
        $("#modalCambiarPassword").modal("show");
    };
    $scope._irRolesUsuario = function (item) {
        localStorage.setItem("UserRoles", JSON.stringify(item));
        window.location.href = "/#/app/seguridad/usuarios/registro_roles_usuarios";
    };

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.CambiarPassword = function () {
        $scope.peticion = true;
        $("#modalCambiarPassword").modal("hide");
        byaPage.Loading("Cambiando contraseña", "Por favor, espere...");
        if ($scope.Usuario.Password != $scope.Usuario.PasswordConfirm) {
            servicioGeneral.addAlert("danger", "Las contraseñas no coinciden.");
            byaPage.EndLoading();
            setTimeout(function () { $("#modalCambiarPassword").modal("show"); }, 500);
        } else if ($scope.Usuario.Password.length < 6) {
            servicioGeneral.addAlert("danger", "Las contraseñas debe poseer por lo menos 6 caracteres.");
            byaPage.EndLoading();
            setTimeout(function () { $("#modalCambiarPassword").modal("show"); }, 500);
        } else {
            var serUsuario = UsuarioService.PostForzar($scope.Usuario);
            
            serUsuario.then(function (pl) {
                if (pl.data.Error == false) {
                    servicioGeneral.addAlert("success", pl.data.Mensaje);
                    byaPage.EndLoading();
                } else {
                    servicioGeneral.addAlert("danger", pl.data.Mensaje);
                    byaPage.EndLoading();
                    setTimeout(function () { $("#modalCambiarPassword").modal("show"); }, 500);
                }
            }, byaPage.errorPeticion);
        }

        $scope.peticion = false;
    };


    $scope.AceptarValor = function (item) {
        byaPage.Loading("Cambiando fecha de desactivación", "Por favor, espere...");
        var serUsuario = UsuarioService.PostCambiarFechaDesactivar(item);
        serUsuario.then(function (pl) {
            if (pl.data.Error == false) {
                delete item.FechaActual;
                item.Editar = false;
                servicioGeneral.addAlert("success", pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
                $scope.CancelarCambioValor(item);
            };
            byaPage.EndLoading();
        }, byaPage.errorPeticion);

    };

    $scope.CancelarCambioValor = function (item) {
        item.FechaDesactivacion = item.FechaActual;
        delete item.FechaActual;
        item.Editar = false;
    };
    $scope._verPerfiles = function (item) {
        getPerfiles(item.UserName);
    }

    function getPerfiles(username) {
        var servicio = PerfilesService.GetPermisosAsignables(username);
        servicio.then(function (success) {
            $scope.Perfiles = success.data;
        }, byaPage.errorPeticion);
    };
    _init();
    function _init() {

        servicioGeneral._nombreTitle("Administración de Usuarios");
        servicioGeneral._verificarPermiso("SEGURIDAD_GestionUsuarios");
        //$rootScope.stateLabel = "Administración de Usuarios";
        _dibujarTablaUsuarios();
        _traerUsuarios();
        $('#modalNuevoUsuario').on('shown.bs.modal', function () {
            $("#username-input").focus()
        });
    };
    function _traerUsuarios() {
        $scope.Peticion = "false";
        var promesa = UsuarioService.Obtener($scope.Filtro);
        promesa.then(function (pl) {
            $scope.Usuarios = pl.data;
            $scope.tableUsuarios.reload();
            $scope.Peticion = "true";
        }, byaPage.errorPeticion);
    };
    $scope.mostrarFecha = function (fecha) {
        $scope.Usuario.FechaDesactivacion = fecha;
    };
    function _dibujarTablaUsuarios() {
        $scope.tableUsuarios = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Usuarios.filter(function (a) {
                    return (a.UserName + "").toLowerCase().indexOf(c) > -1 ||
                           (a.Email + "").toLowerCase().indexOf(c) > -1
                })) : f = $scope.Usuarios, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
            }
        });
    };
}]);
'use strict';
app.controller('RegistroRolesUsuariosCtrl', ['$rootScope', '$scope', 'UsuarioService', 'AccountService', '$filter', 'MenuService', 'toastr', 'servicioGeneral',
    function ($rootScope, $scope, UsuarioService, AccountService, $filter, MenuService, toastr, servicioGeneral) {
        // obj´s del modal para buscar
        $scope.Usuarios = [];
        $scope.alerts = [];
        $scope.Roles = [];
        $scope.Lista = [];
        $scope.UserName = undefined;
        $scope.Menu = { Todos: false };
        // FIN obj´s del modal para buscar

        // Obj cargar usuarios
        $scope.cargado = false;

        $scope.UsuarioOptions = {
            data: [],
            aoColumns: [
                { mData: 'UserName' },
                { mData: 'FirstName' },
                { mData: 'JoinDate' },
                { mData: 'BtnRoles' }
            ],
            "searching": true,
            "iDisplayLength": 25,
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": " No hay órdenes registradas ",
                "infoFiltered": "(filtro de _MAX_ registros totales)",
                "search": " Filtrar : ",
                "oPaginate": {
                    "sPrevious": "Anterior",
                    "sNext": "Siguiente"
                }
            },
            "aaSorting": []
        };
        // FIN Obj cargar usuarios

        _init();
        function _init() {
            servicioGeneral._nombreTitle("Registro de Roles");
            servicioGeneral._verificarPermiso("SEGURIDAD_GestionUsuarios");
            _cargarUsuarios();
        };
        
        function _cargarUsuarios() {
            var user = JSON.parse(localStorage.getItem("UserRoles"));
            if (user != null && user != "") {
                $scope.Usuario = user;
                _obtenerMenusPadres();
            }
        };
        function _obtenerMenusPadres() {
            byaPage.Loading("Buscando datos", "Por favor, espere...");
            var serUsuario = MenuService.getMenuPadres();
            serUsuario.then(function (pl) {
                byaPage.EndLoading();
                $scope.MenusPadres = pl.data;
            }, byaPage.errorPeticion);
        };

        $scope.actualizarPerfiles = function () {
            actualizarPerfiles();
        };

        $scope.actualizarSeleccionados = function (check) {
            for (var i in $scope.Lista) {
                var item = $scope.Lista[i];
                item.hasRol = check;
            }
        };
        $scope.buscarMenus = function () {
            $scope.Menu.Todos = false;
            byaPage.Loading("Buscando datos", "Por favor, espere...");
            var serUsuario = MenuService.ObtenerMenus($scope.Usuario.UserName, $scope.Menu.Padre);
            serUsuario.then(function (pl) {
                byaPage.EndLoading();
                $scope.Lista = pl.data;
                $scope.Menu.Todos = seleccioarTodos($scope.Lista);
            }, byaPage.errorPeticion);
        };
        function seleccioarTodos(lista) {
            if (!lista.find(buscarSeleccionados)) {
                return true;
            }
            return false;
        }
        function buscarSeleccionados(item)
        {
            return item.hasRol == false;
        };
        $scope.actualizarRoles = function () {
            actualizarRoles();
        };

        function actualizarRoles() {
            $scope.peticion = true;
            if ($scope.Usuario.UserName != undefined) {
                var lista = $scope.Lista;
                if (lista.length > 0) {
                    byaPage.Loading("Guardando cambios", "Por favor, espere...");
                    var serUsuario = UsuarioService.GuardarRoles($scope.Usuario.UserName, lista);
                    serUsuario.then(function (pl) {
                        byaPage.EndLoading();
                        servicioGeneral.addAlert(pl.data.Error == true ? "error" : "success", pl.data.Mensaje);
                        $scope.Menu.Todos = seleccioarTodos($scope.Lista);
                    }, byaPage.errorPeticion);

                }
                else {
                    servicioGeneral.addAlert(pl.data.Error == true ? "error" : "success", "El modulo no contiene roles asignadad o no ha escogido un modulo");
                }
            }
            else {
                servicioGeneral.addAlert(pl.data.Error == true ? "error" : "success", "No ha seleccionado un usuario valido");
            }
            $scope.peticion = false;
        };

        $scope.ShowRoles = function (UserName) {
            $scope.Roles = [];
            $scope.UserName = UserName;
            var promiseGet = RolService.getRoles(UserName);
            promiseGet.then(function (data) {
                $scope.Roles = data.data;
                //console.log(data.data);
            }, byaPage.errorPeticion);
        };

        $scope.AddUserToRol = function (o) {
            $scope.Roles = [];
            o.UserName = $scope.UserName;
            var promisePost = RolService.addRolToUserName(o);
            promisePost.then(function (d) {
                $scope.ShowRoles(o.UserName);
            }, byaPage.errorPeticion);
        };
        $scope.DeleteUserFromRol = function (o) {
            //console.log("borrar");
            $scope.Roles = [];
            o.UserName = $scope.UserName;
            var promisePost = RolService.removeRolToUserName(o);
            promisePost.then(function (d) {
                $scope.ShowRoles(o.UserName);
            }, byaPage.errorPeticion);
        };

    }]);

'use strict';
app.controller('SoporteCtrl', ['$scope', '$stateParams', '$state', 'servicioGeneral', 'UsuarioService',
    function ($scope, $stateParams, $state, servicioGeneral, UsuarioService) {
        var img = document.getElementById("imgCanvas");
        var canvas = document.getElementById("canvasPan");
        $scope.TituloApp = byaSite.getAplicacion() + " - " + byaSite.getAplicacionDescripcion();
        $scope.Foto = function () {
            var canvas = document.querySelector("#canvasPan");
            html2canvas(document.querySelector("body"), { canvas: canvas }).then(function (canvas) {
                img.src = canvas.toDataURL("image/png");
            });
            setTimeout(function () {
                $("#modalSoporte").modal("show");
            }, 1500);
        };
    $scope.Enviar = function (obj) {
        var e = {};
        e.Url = window.location.href;
        e.Asunto = obj.Asunto;
        e.Mensaje = obj.Mensaje;
        e.Imagen = canvas.toDataURL("image/png");
        e.IdentificacionRemitente = byaSite.getUsuario();
        e.NombreAplicacion = byaSite.aplicacion;
        e.NombreEmpresa = "ENTIDAD";
        $.ajax({
            type: "POST",
            url: "api/Soporte",
            data: JSON.stringify(e),
            async: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#modalSoporte").modal("hide");
                if (result.Error == false) {
                    servicioGeneral.addAlert('success', 'Su mensaje ha sido enviado a soporte satisfactoriamente.');
                    $("#txtAsuntoSoporte").val("");
                    $("#txtMensajeSoporte").val("");
                } else {
                    servicioGeneral.addAlert('danger', result.Mensaje + ' Porfavor intente mas tarde');
                }
            },
            error: byaPage.errorPeticionAjax
        });
    }
   
}]);
app.controller('DatepickerCtrl', ["$scope", "servicioGeneral", function ($scope, servicioGeneral) {

    function isValidDate(s) {
        // format D(D)/M(M)/(YY)YY
        var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

        if (dateFormat.test(s)) {
            // remove any leading zeros from date values
            s = s.replace(/0*(\d*)/gi,"$1");
            var dateArray = s.split(/[\.|\/|-]/);
      
            // correct month value
            dateArray[1] = dateArray[1]-1;

            // correct year value
            if (dateArray[2].length<4) {
                // correct year value
                dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
            }

            var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
            if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.validate = function ($event) {
        if (!isValidDate($event.target.value))
            $event.target.value = '';
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

}]);