﻿app.controller("GestionArchivoCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "ActasService", "$modal", "servicioGeneral", "EnvioDatosService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, ActasService, $modal, servicioGeneral, EnvioDatosService, $filter) {
        $scope.Filtro = {};
        $scope.Terceros = [];
        $scope.tableTerceros = {};
        $scope.Identificacion = "";

        $scope.eliminar = function (item) {
            if (confirm("Seguro que desea eliminar el registro?")) {
                _eliminar(item);
            }
        };
        $scope.ConsultarPorFecha = function () {
            _traerActas();
        };
        $scope._CambiarEstado = function (item) {
            if (item.Estado === "IN") {
                item.Estado = "AC";
            } else {
                item.Estado = "IN";
            }
            $scope.Tercero = item;
            _guardarTerceroModificado();
        };
        $scope.verDetalles = function (item) {
            byaPage.Loading();
            var servicio = ActasService.ConsultarRegistros(item.Id);
            servicio.then(function (success) {
                byaPage.EndLoading();
                $scope.Resultado = success.data;
                $timeout(function () {
                    $("#modalRegistros").modal("show");
                }, 500);
            }, function (error) {
                byaPage.EndLoading();
                byaPage.errorPeticion(error);
            });
        };
    function _eliminar(item) {
        var send = ActasService.PostTercerosEliminar(item);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerActas();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert('danger', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    }
    _init();
    function _init() {
        servicioGeneral._nombreTitle("Cargar archivo");
        servicioGeneral._verificarPermiso("ARCHIVO_Consulta");
        _dibujarTablaTerceros();
        fechasEstablecer();
    }
    function fechasEstablecer() {
        var hoy = new Date();
        $scope.Filtro.FechaInicio = $filter("date")(new Date(hoy.getFullYear(), 0, 1), "yyyy-MM-dd", "-0500");
        $scope.Filtro.FechaFinal = $filter("date")(new Date(hoy.getFullYear(), 12, 31), "yyyy-MM-dd", "-0500");
        _traerActas();
    }
    function _guardarTerceroModificado() {
        var send = ActasService.EstadoTercero($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerActas();
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
            } else {
                _traerActas();
                servicioGeneral.addAlert('error', 'Error!', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    }
    function _traerActas() {
        byaPage.Loading();
        var servicio = ActasService.ConsultarActas($scope.Filtro);
        servicio.then(function (success) {
            byaPage.EndLoading();
            $scope.Terceros = success.data;
            $scope.tableTerceros.reload();
        }, function (error) {
            byaPage.EndLoading();
            byaPage.errorPeticion(error);
        });
    }
    function _dibujarTablaTerceros() {
        $scope.tableTerceros = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Terceros.filter(function (a) {
                    return (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                        (a.TipoIdentidad + "").toLowerCase().indexOf(c) > -1 ||
                        (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1;
                })) : f = $scope.Terceros, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()));
            }
        });
    }
}]);