﻿app.controller("ImportarArchivoCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ActasService", "funcionService", "ByATimeDate", "servicioGeneral", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ActasService, funcionService, ByATimeDate, servicioGeneral, $filter) {
        $scope.Archivo = {};
        $scope.Departamentos = [];
        $scope.CausasMotivos = [];
        $scope.Cargando = true;
        $scope.Ruta = ""; // Ruta a la cual debe ir  desde otros impuestos.
        $scope.OcultarCampos = false;
        $scope.Origen = "Tercero";
        $scope.Direccion = {};
        $scope.MensajeOb = {};

        $scope.Boton = { "Guardar": false, "Siguiente": true, "Aceptar": false };

        $scope.regresar = function () {
            if ($.isEmptyObject($location.search())) {
                window.history.back();
            } else {
                location.href = "/#/app/archivo/excel/consulta";
            }
        };
        $scope.ConsultarConductor = function (busqueda) {
            ConsultarConductor(busqueda);
        };
        $scope.ConsultarSubClase = function (item, indice) {
            var encontrados = $filter("filter")($scope.SubClases, item.SubClase);
            if (encontrados.length === 0) {
                $scope.SubClases.push(item.SubClase);
            }
            $scope.ConsultarProductos(item, indice);
        };

        $scope.ConsultarDescripciones = function (item, indice) {
            var encontrados = $filter("filter")($scope.Descripciones, item.DescripcionMercancia);
            console.log(encontrados);
            if (encontrados.length === 0) {
                $scope.Descripciones.push(item.DescripcionMercancia);
            }
            $scope.ConsultarProductos(item, indice);
        };
        $scope.ConsultarCausal = function (item, indice) {
            var encontrados = $filter("filter")($scope.CausasMotivos, item.CausalMotivo);
            if (encontrados.length === 0) {
                $scope.CausasMotivos.push(item.CausalMotivo);
            }
            ValidarNuevo(item, indice);
        };
        $scope.ConsultarProductos = function (item, indice) {
            ValidarNuevo(item, indice);
            ConsultarProductos(item);
        };
        $scope.ConsultarResponsable = function (busqueda) {
            ConsultarResponsable(busqueda);
        };
        $scope.ConsultarEstablecimiento = function (busqueda) {
            ConsultarEstablecimiento(busqueda);
        };
        $scope.ConsultarVehiculo = function (busqueda) {
            ConsultarVehiculo(busqueda);
        };
        $scope.ConsultarFuncionario = function (busqueda) {
            ConsultarFuncionario(busqueda);
        };
        $scope.Guardar = function (borrador) {
            $scope.Archivo.Borrador = borrador;
            if ($scope.Archivo.Nueva === 'SI') guardarActa();
            else guardarActaModificada();
        };

        $scope.calculaDigitoVerificador = function () {
            if (!$scope.Archivo.Identificacion) return;
            $scope.Archivo.DVerificacion = funcionService.DigitoVerificacion($scope.Archivo.Identificacion);
        };

        $scope._volver = function () {
            history.back();
        };
        $scope._limpiar = function () {
            bootbox.confirm({
                message: "¿Seguro que desea limpiar todos los campos?",
                buttons: {
                    confirm: {
                        label: 'Aceptar',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-default'
                    }
                },
                callback: function (result) {
                    if (result === true)
                        nuevaActa();
                }
            });

        };

        $scope.ValidarNuevo = function (item, indice) {
            ValidarNuevo(item, indice);
        };
        $scope.eliminarItem = function (indice) {
            eliminarItem(indice);
        };
        $scope.ValidarNumeroActa = function () {
            ValidarNumeroActa();
        };
        $scope.actualizarArchivo = function () {
            actualizarArchivo();
        };
        init();
        function init() {
            servicioGeneral._nombreTitle("Cargar Archivo");
            servicioGeneral._verificarPermiso("ARCHIVO_Importar");
            setTimeout(function () { $(".sidebar-toggler").click(); }, 800);
            obtenerDatos();
            var datos = $location.search();
            if (!datos.Id) {
                nuevaActa();
            } else {
                var _id = datos.Id;
                consultarActa(_id);
            }
            $scope.Cargando = false;
        }

        function obtenerDatos() {
            var send = ActasService.ConsultarDatosFormulario();
            send.then(function (pl) {
                $scope.EstadoActas = pl.data.EstadoActas;
                $scope.TipoOperativos = pl.data.TipoOperativos;
                $scope.TipoEstablecimientos = pl.data.TipoEstablecimientos;
                //
                $scope.Clases = pl.data.Clases;
                $scope.Origenes = pl.data.Origenes;
                $scope.SubClases = pl.data.SubClases;
                $scope.Descripciones = pl.data.Descripciones;
                $scope.EstadosProductos = pl.data.EstadosProductos;
                $scope.EmpresaTransporte = pl.data.EmpresaTransporte;

            }, byaPage.errorPeticion);
        }

        function consultarActa(id) {
            var send = ActasService.Consultar(id);
            send.then(function (pl) {
                $scope.Archivo = pl.data;
                $scope.Archivo.Fecha = new Date($scope.Archivo.Fecha);
                $scope.Archivo.Nueva = 'NO';
                if ($.isArray($scope.Archivo.Productos)) {
                    $scope.Archivo.Productos.push({});
                } else {
                    $scope.Archivo.Productos = [{}];
                }
            }, byaPage.errorPeticion);
        }
        function ConsultarFuncionario(id) {
            var send = ActasService.ConsultarFuncionario(id);
            send.then(function (pl) {
                if (pl.data.FuncionarioIdentificacion === id) {
                    $scope.Archivo.FuncionarioIdentificacion = pl.data.FuncionarioIdentificacion;
                    $scope.Archivo.FuncionarioNombre = pl.data.FuncionarioNombre;
                    $scope.Archivo.FuncionarioCargo = pl.data.FuncionarioCargo;
                }
            }, byaPage.errorPeticion);
        }

        function ValidarNumeroActa() {
            var send = ActasService.ValidarNumero($scope.Archivo.NumeroActa);
            send.then(function (pl) {
                if (pl.data === "SI")
                    servicioGeneral.addAlert("warning", "Este número de acta ya esta registrado en el sistema");
            }, byaPage.errorPeticion);
        }

        function ConsultarVehiculo(id) {
            var send = ActasService.ConsultarVehiculo(id);
            send.then(function (pl) {
                if (pl.data.VehiculoPlaca === id) {
                    $scope.Archivo.VehiculoPlaca = pl.data.VehiculoPlaca;
                    $scope.Archivo.VehiculoMarca = pl.data.VehiculoMarca;
                    $scope.Archivo.VehiculoAfiliadoEmpresaTransporte = pl.data.VehiculoAfiliadoEmpresaTransporte;
                    $scope.Archivo.VehiculoEmpresaTransporte = pl.data.VehiculoEmpresaTransporte;
                }
            }, byaPage.errorPeticion);
        }

        function ConsultarEstablecimiento(id) {
            var send = ActasService.ConsultarEstablecimiento(id);
            send.then(function (pl) {
                if (pl.data.EstablecimientoNit === id) {
                    $scope.Archivo.EstablecimientoNit = pl.data.EstablecimientoNit;
                    $scope.Archivo.EstablecimientoNombreRazonSocial = pl.data.EstablecimientoNombreRazonSocial;
                    $scope.Archivo.EstablecimientoTipo = pl.data.EstablecimientoTipo;
                    $scope.Archivo.EstablecimientoDireccion = pl.data.EstablecimientoDireccion;
                    $scope.Archivo.EstablecimientoTelefono = pl.data.EstablecimientoTelefono;
                    $scope.Archivo.EstablecimientoCiudad = pl.data.EstablecimientoCiudad;
                    $scope.Archivo.EstablecimientoPropietarioIdentificacion = pl.data.EstablecimientoPropietarioIdentificacion;
                    $scope.Archivo.EstablecimientoPropietarioNombre = pl.data.EstablecimientoPropietarioNombre;
                    $scope.Archivo.EstablecimientoRepresentanteLegalIdentificacion = pl.data.EstablecimientoRepresentanteLegalIdentificacion;
                    $scope.Archivo.EstablecimientoRepresentanteLegalNombre = pl.data.EstablecimientoRepresentanteLegalNombre;
                }
            }, byaPage.errorPeticion);
        }

        function ConsultarResponsable(id) {
            var send = ActasService.ConsultarResponsable(id);
            send.then(function (pl) {
                if (pl.data.ResponsableTenedorNit === id) {
                    $scope.Archivo.ResponsableTenedorNit = pl.data.ResponsableTenedorNit;
                    $scope.Archivo.ResponsableTenedorNombre = pl.data.ResponsableTenedorNombre;
                    $scope.Archivo.ResponsableTenedorDireccion = pl.data.ResponsableTenedorDireccion;
                    $scope.Archivo.ResponsableTenedorTelefono = pl.data.ResponsableTenedorTelefono;
                    $scope.Archivo.ResponsableTenedorDepartamento = pl.data.ResponsableTenedorDepartamento;
                    $scope.Archivo.ResponsableTenedorCiudad = pl.data.ResponsableTenedorCiudad;
                }
            }, byaPage.errorPeticion);
        };
        function ConsultarConductor(id) {
            var send = ActasService.ConsultarConductor(id);
            send.then(function (pl) {
                if (pl.data.ConductorIdentificacion === id) {
                    $scope.Archivo.ConductorIdentificacion = pl.data.ConductorIdentificacion;
                    $scope.Archivo.ConductorNombre = pl.data.ConductorNombre;
                    $scope.Archivo.ConductorDireccion = pl.data.ConductorDireccion;
                    $scope.Archivo.ConductorTelefono = pl.data.ConductorTelefono;
                    $scope.Archivo.ConductorDepartamento = pl.data.ConductorDepartamento;
                    $scope.Archivo.ConductorCiudad = pl.data.ConductorCiudad;
                }
            }, byaPage.errorPeticion);
        }
        function ConsultarProductos(producto) {
            var send = ActasService.ConsultarProductos(producto.Clase, producto.SubClase, producto.DescripcionMercancia);
            send.then(function (pl) {
                if (!ItemVacio(pl.data)) {
                    console.log(!ItemVacio(pl.data));
                    console.log(pl.data);
                    if (pl.data.DescripcionMercancia === producto.DescripcionMercancia && pl.data.SubClase === producto.SubClase &&
                        pl.data.Clase === producto.Clase && pl.data.Origen === producto.Origen) {
                        producto.Clase = pl.data.Clase;
                        producto.SubClase = pl.data.SubClase;
                        producto.Origen = pl.data.Origen;
                        producto.DescripcionMercancia = pl.data.DescripcionMercancia;
                        producto.UnidadMedida = pl.data.UnidadMedida;
                        producto.Capacidad = pl.data.Capacidad;
                        producto.Alcohol = pl.data.Alcohol;
                        producto.Cantidad = pl.data.Cantidad;
                        producto.Estado = pl.data.Estado;
                        producto.ValorUnitario = pl.data.ValorUnitario;
                        producto.CausalMotivo = pl.data.CausalMotivo;
                    }
                }
            }, byaPage.errorPeticion);
        }
        function armarArchivo() {
            var _form = new FormData();
            _form.append('Fecha', $filter('date')($scope.Archivo.Fecha, 'YYYY-MM-DD'));
            _form.append('Hora', $filter('date')($scope.Archivo.Hora, 'HH:mm:ss'));
            _form.append('Archivo', angular.element("#Archivo").get(0).files[0]); 
            return _form;
        }
    function guardarActa() {
        $scope.Boton.Guardar = true;
        byaPage.Loading();
        var send = ActasService.Guardar(armarArchivo());
        send.then(function (pl) {
            $scope.Boton.Guardar = false;
            byaPage.EndLoading();
            if (!pl.data.Error) {
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
                $scope.Archivo.Nueva = 'NO';
                $scope.Archivo.Id = pl.data.Id;
                if (!$scope.Archivo.Borrador)
                    setTimeout(function () { history.back(); }, 500);

            } else {
                servicioGeneral.addAlert('error','Error!', pl.data.Mensaje);
            }
        }, function (error) {
            byaPage.EndLoading();
            byaPage.errorPeticion(error);
        });
    }
    function guardarActaModificada() {
        $scope.procesando = true;
        byaPage.Loading();
        var _acta = angular.copy($scope.Archivo);
        _acta.HoraAprehension = $filter("date")(_acta.HoraAprehension, 'HH:mm:dd', '-0500');
        var send = ActasService.Modificar(_acta);
        send.then(function (pl) {
            if (!pl.data.Error) {
                nuevaActa();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                $scope.Boton.Siguiente = false;
                setTimeout(function () {
                    window.location.href = "/#/app/archivo/excel/consulta";
                }, 500);

            } else {
                servicioGeneral.addAlert('error', pl.data.Mensaje);
            }
            byaPage.EndLoading();
            $scope.Boton.Guardar = false;
        }, function (error) {
            byaPage.EndLoading();
            byaPage.errorPeticion(error);
        });
    }

    function nuevaActa() {
        var hoy = new Date();
        $scope.Archivo = {
            Fecha: hoy,
            Hora: new Date(0,0,0,hoy.getHours(),hoy.getMinutes(),hoy.getSeconds()),
            Nueva: 'SI',
            Productos: [{}, {}, {}, {}, {}, {}, {}]
        };
    }
    function CampoTextoVacio(campo) {
        if (campo) {
            if (campo !== null) {
                if (campo.length > 0) {
                    if (campo.toString().trim() !== "")
                        return false;
                    else return true;
                } else {
                    return true;
                }
            }
            return true;
        }
        return true;
    }

    function CampoNumeroVacio(campo) {
        if (campo) {
            if ($.isNumeric( campo )) {
                var numero = parseFloat(campo);
                if (numero > 0) {
                    return false;
                } else {
                    return true;
                }
            }
            return true;
        }
        return true;
    }
    function ValidarNuevo(item, indice) {
        var _ultimo = $scope.Archivo.Productos.length - 1;
        if (_ultimo === indice) {
            if (!$.isEmptyObject(item)) {
                if (!ItemVacio(item)) {
                    $scope.Archivo.Productos.push({});
                }
            }
        }
    }
    function eliminarItem(indice) {
        var _ultimo = $scope.Archivo.Productos.length - 1;
        if (_ultimo !== indice) {
            $scope.Archivo.Productos.splice(indice, 1);
        }
    }
    function ItemVacio(item) {
        return CampoTextoVacio(item.Clase) && CampoTextoVacio(item.SubClase) && CampoTextoVacio(item.Origen) && CampoTextoVacio(item.DescripcionMercancia) &&
                    CampoTextoVacio(item.UnidadMedida) && CampoTextoVacio(item.Capacidad) && CampoTextoVacio(item.Alcohol) &&
                    CampoNumeroVacio(item.Cantidad) && CampoNumeroVacio(item.ValorUnitario) &&
                    CampoTextoVacio(item.Estado) && CampoTextoVacio(item.CausalMotivo);
        }
        function actualizarArchivo() {
            if (angular.element("#Archivo").get(0).files.length > 0) {
                console.log(angular.element("#Archivo").get(0).files[0]['name']);
                $scope.Archivo.ArchivoNombre = angular.element("#Archivo").get(0).files[0]['name'];
                angular.element("#ArchivoNombre").val(angular.element("#Archivo").get(0).files[0]['name']);
            }
        }
}]);
