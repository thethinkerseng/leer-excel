﻿app.controller("GestionTerceroCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "TerceroService", "$modal", "servicioGeneral", "EnvioDatosService",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, TerceroService, $modal, servicioGeneral, EnvioDatosService) {
    $scope.Filtro = "";
    $scope.Terceros = [];
    $scope.tableTerceros = {};
    $scope.Identificacion = "";

    $scope.exportData = function () {
        alasql('SELECT *  INTO XLSX("Terceros.xlsx",{headers:true}) FROM ?', [$scope.Terceros]);
    };

    $scope.agregar = function () {
        window.location.href = "/#/app/datos_basicos/terceros/registro_terceros";
    };

    $scope.editar = function (item) {
        window.location.href = "/#/app/datos_basicos/terceros/registro_terceros?tercero_editar=" + Base64.encode(item.Identificacion);
    };

    $scope.eliminar = function (item) {
        if (confirm("Seguro que desea eliminar el registro?")) {
            _eliminar(item);
        }
    };
    $scope._CambiarEstado = function (item) {
        if (item.Estado === "IN") {
            item.Estado = "AC"
        } else {
            item.Estado = "IN"
        }
        $scope.Tercero = item;
        _guardarTerceroModificado()
    };
    function _eliminar(item) {
        var send = TerceroService.PostTercerosEliminar(item);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerTerceros();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert('danger', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    };
    _init();
    function _init() {
        servicioGeneral._nombreTitle("Gestión Terceros");
        servicioGeneral._verificarPermiso("DATOSBASICOS_GestionTerceros");
        _dibujarTablaTerceros();
        _traerTerceros();
        sessionStorage.removeItem("ruta");
    };
    function _guardarTerceroModificado() {
        var send = TerceroService.EstadoTercero($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerTerceros();
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
            } else {
                _traerTerceros();
                servicioGeneral.addAlert('error', 'Error!', pl.data.Mensaje);
            }
        }, byaPage.errorPeticion);
    };
    function _traerTerceros() {
        var send = TerceroService.ObtenerTodos();
        send.then(function (pl) {
            $scope.Terceros = pl.data;
            $scope.tableTerceros.reload();
        }, byaPage.errorPeticion);
    };
    function _dibujarTablaTerceros() {
        $scope.tableTerceros = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Terceros.filter(function (a) {
                    return (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                           (a.TipoIdentidad + "").toLowerCase().indexOf(c) > -1 ||
                           (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1
                })) : f = $scope.Terceros, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
            }
        });
    };
}]);