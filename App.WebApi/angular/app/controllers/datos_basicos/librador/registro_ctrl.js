﻿app.controller("RegistroLibradorCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "servicioGeneral", "RegistroLibradorService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, servicioGeneral, RegistroLibradorService, $filter) {
        $scope.Tercero = {};
        $scope.DatosInicialesFormulario = {};
        $scope.botonesHabilitados = {};
        
        $scope.volverAtras = function () {
            history.back();
        };
        $scope.buscarTercero = function () {
            if ($scope.Tercero.Identificacion != null && $scope.Tercero.Identificacion != "") {
                $scope.botonesHabilitados.todos = false;
                var send = RegistroLibradorService.ObtenerTercero($scope.Tercero.Identificacion);
                send.then(function (pl) {
                    $scope.botonesHabilitados.todos = true;
                    $scope.Tercero = pl.data;
                }, function (error) {
                    $scope.botonesHabilitados.todos = true;
                    byaPage.errorPeticion(error);
                });
            }
        };
        $scope.cambiarCuentaLibrador = function (index, item) {
            if (item.EsCuentaLibrador) {
                $.each($scope.Tercero.CuentasBancarias, function (index2, item2) { 
                    if (index != index2) item2.EsCuentaLibrador = false;
                });
            }
        };
        $scope.esValidoGuardarTercero = function () {
            if ($scope.Tercero.CuentasBancarias != null && $scope.Tercero.CuentasBancarias.length > 0) {
                console.log($scope.Tercero.CuentasBancarias);
                var cuentasSeleccionadas = $scope.Tercero.CuentasBancarias.filter(function (o) { return o.EsCuentaLibrador; });
                if (cuentasSeleccionadas.length == 1) return true;
                else return false;
            } else return false;
        };
        $scope.guardarTercero = function () {
            $scope.botonesHabilitados.todos = false;
            $scope.Tercero.EsLibrador = true;
            var send = RegistroLibradorService.GuardarLibrador($scope.Tercero);
            send.then(function (pl) {
                $scope.botonesHabilitados.todos = true;
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                $scope.buscarTercero();
            }, function (error) {
                $scope.botonesHabilitados.todos = true;
                byaPage.errorPeticion(error);
            });
        };

        _init();
        function _init() {
            servicioGeneral._nombreTitle("Registro librador");
            servicioGeneral._verificarPermiso("DATOSBASICOS_RegistroDeLibrador");
            _traerDatosInicialesFormulario();
            _identificarOperacionARealizar();
        };
        function _inicializarBotonesHabilitados() {
            $scope.botonesHabilitados.todos = true;
        };
        function _traerDatosInicialesFormulario() {
            $scope.botonesHabilitados.todos = false;
            var send = RegistroLibradorService.ObtenerDatosInicialesFormulario();
            send.then(function (pl) {
            $scope.botonesHabilitados.todos = true;
                $scope.DatosInicialesFormulario = pl.data;
            }, function (error) {
                $scope.botonesHabilitados.todos = true;
                byaPage.errorPeticion(error);
            });
        };
        function _identificarOperacionARealizar() {
            var identificacion = localStorage.getItem("IdentificacionRegistroLibrador");
            if (identificacion != null || identificacion != "") {
                $scope.Tercero.Identificacion = identificacion;
                $scope.buscarTercero();
            }
        };
    }]);