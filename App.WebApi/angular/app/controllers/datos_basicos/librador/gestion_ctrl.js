﻿app.controller("GestionLibradoresCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "servicioGeneral", "GestionLibradorService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, servicioGeneral, GestionLibradorService, $filter) {
        $scope.tableLibradores = {};
        $scope.libradores = [];
        $scope.irAAgregarLibrador = function () {
            localStorage.removeItem("IdentificacionRegistroLibrador");
            window.location.href = "/#/app/datos_basicos/librador/registro";
        };
        $scope.editarLibrador = function (item) {
            localStorage.setItem("IdentificacionRegistroLibrador", item.Identificacion);
            window.location.href = "/#/app/datos_basicos/librador/registro";
        };
        $scope.eliminarLibrador = function (item) {
            item.EsLibrador = false;
            var send = GestionLibradorService.GuardarLibrador(item);
            send.then(function (pl) {
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                _traerLibradores();
            }, byaPage.errorPeticion);
        };

        _init();
        function _init() {
            servicioGeneral._nombreTitle("Gestión de libradores");
            servicioGeneral._verificarPermiso("DATOSBASICOS_RegistroDeLibrador");
            _dibujarTablaLibradores();
            _traerLibradores();
        };
        function _traerLibradores() {
            var send = GestionLibradorService.ObtenerLibradores();
            send.then(function (pl) {
                $scope.libradores = pl.data;
                $scope.tableLibradores.reload();
            }, byaPage.errorPeticion);
        };
        function _dibujarTablaLibradores() {
            $scope.tableLibradores = new ngTableParams({
                page: 1,
                count: 100
            }, {
                filterDelay: 50,
                getData: function (a, b) {
                    var c = b.filter().Filtro,
                        f = [];
                    c ? (c = c.toLowerCase(), f = $scope.libradores.filter(function (a) {
                        
                        return (a.TipoIdentificacion + "").toLowerCase().indexOf(c) > -1 ||
                               (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                               (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1 ||
                               (a.Telefono + "").toLowerCase().indexOf(c) > -1 
                    })) : f = $scope.libradores, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
                }
            });
        };
    }
]);