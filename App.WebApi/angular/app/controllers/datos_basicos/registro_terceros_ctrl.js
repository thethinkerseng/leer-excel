﻿app.controller("RegistroTercerosCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "TerceroService","funcionService","ByATimeDate", "servicioGeneral",
    function ($scope, $rootScope, $timeout, $window, $location, TerceroService, funcionService, ByATimeDate, servicioGeneral) {
    $scope.Tercero = {};
    $scope.Departamentos = [];
    $scope.Cargando = true;
    $scope.Ruta = ""; // Ruta a la cual debe ir  desde otros impuestos.
    $scope.OcultarCampos = false;
    $scope.Origen = "Tercero";
    $scope.Direccion = {};
    $scope.MensajeOb = {};


    $scope.Boton = { "Guardar": false, "Siguiente": true , "Aceptar" : false};

    $scope.regresar = function () {
        if ($.isEmptyObject($location.search())) {
            window.history.back();
        } else {
            location.href = "/#/app/datos_basicos/terceros/gestion_terceros";
        }
    };

    $scope._CambioTipoPersona = function(){
        if ($scope.Tercero.TipoPersona === "JURIDICA"){
            $scope.Tercero.PrimerNombre = "";
            $scope.Tercero.SegundoNombre = "";
            $scope.Tercero.PrimerApellido = "";
            $scope.Tercero.SegundoApellido = "";
            $scope.Tercero.TipoIdentidad = "NIT";
        }else{
            $scope.Tercero.RazonSocial = "";
            $scope.Tercero.NombreContacto = "";
            $scope.Tercero.TipoIdentidad = "CC";
        }
    };

    $scope._guardar = function () {

        if (!$scope.Tercero.Identificacion) {
            servicioGeneral.addAlert("danger", "Ingrese su identificación.");
            return;
        }

        if (!$scope.Tercero.Email) {
            servicioGeneral.addAlert("danger", "Por favor ingrese el Email ");
            return;
        }

        if (!validarTercero()) return;

        if ($scope.Origen === "OtroImpuestos") {
            if (!$scope.Tercero.Direccion) {
                servicioGeneral.addAlert("danger", "Por favor ingrese la Dirección ");
                return;
            }
        }
        if ($scope.Tercero.Accion === "NUEVO") guardarNuevoTercero();
        else guardarTerceroModificado();
    };

    $scope.calculaDigitoVerificador = function () {
        if (!$scope.Tercero.Identificacion) return;
        $scope.Tercero.DVerificacion = funcionService.DigitoVerificacion($scope.Tercero.Identificacion);
    };

    $scope._volver = function () {
        history.back();
    };
    $scope._limpiar = function () {
        bootbox.confirm({
            message: "¿Seguro que desea limpiar todos los campos?",
            buttons: {
                confirm: {
                    label: 'Aceptar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result == true)
                    nuevoTercero();
            }
        });

    };



    init();
    function init() {

        servicioGeneral._nombreTitle("Registro Terceros");
        servicioGeneral._verificarPermiso("DATOSBASICOS_RegistroTerceros");
        var datos = $location.search();
        if (datos.tercero_editar == null) {
            nuevoTercero();
        } else {
            var identificacion = Base64.decode(datos.tercero_editar);
            getTercero(identificacion);
        }
        $scope.Cargando = false;
    };

    function initRuta() {
        var datosLocal = sessionStorage.getItem("ruta");
        if (datosLocal) {
            var obj = JSON.parse(Base64.decode(datosLocal));
            $scope.Ruta = obj.Ruta;
            $scope.OcultarCampos = true;
            $scope.Origen = "OtroImpuestos";
        }
    }
    $rootScope.$on("iniciarRuta", function (event, data) {
        initRuta();
    });


    $scope.Editar = true;;
    function inhabilitarcampos() {
        $scope.Editar = false;
    };
    function habilitarcampos() {
        $scope.Editar = true;
    };

    $scope.CerrarModal = function () {
        $('#mdMensaje').modal('hide');
        inhabilitarcampos();
    };



    function validarTercero() {
        if ($scope.Tercero.TipoIdentidad === "NIT") {
            if (!$scope.Tercero.RazonSocial || $scope.Tercero.RazonSocial === "") {
                servicioGeneral.addAlert("warning", "Ingrese la razón social.");
                return false;
            }
            if (!$scope.Tercero.NombreComercial || $scope.Tercero.NombreComercial === "") {
                servicioGeneral.addAlert("warning", "Ingrese el nombre comercial.");
                return false;
            }
            if (!$scope.Tercero.NombreContacto || $scope.Tercero.NombreContacto === "") {
                servicioGeneral.addAlert("warning", "Ingrese el nombre de contacto.");
                return false;
            }

            if (!$scope.Tercero.Email || $scope.Tercero.Email === "") {
                servicioGeneral.addAlert("warning", "Ingrese la razón social.");
                return false;
            }
        } else {
            if (!$scope.Tercero.PrimerNombre || $scope.Tercero.PrimerNombre === "") {
                servicioGeneral.addAlert("warning", "Ingrese el primer nombre.");
                return false;
            }
            if (!$scope.Tercero.PrimerApellido || $scope.Tercero.PrimerApellido === "") {
                servicioGeneral.addAlert("warning", "Ingrese el primer apellido.");
                return false;
            }
        }
        return true;
    }
   function getTercero(identificacion) {
        var send = TerceroService.Obtener(identificacion);
        send.then(function (pl) {
            if (pl.data) {
                $scope.Tercero = pl.data;
                $scope.Tercero.FechaExpedicion = moment(ByATimeDate.Fecha(pl.data.FechaExpedicion)).format(''); //new Date(pl.data.FechaExpedicion);
                $scope.Tercero.Accion = "EDITAR";
            }
        }, byaPage.errorPeticion);
    }
    function guardarNuevoTercero() {
        $scope.Boton.Guardar = true;
        byaPage.Loading();
        var send = TerceroService.Guardar($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                nuevoTercero();
                servicioGeneral.addAlert('success', 'Guardado!', pl.data.Mensaje);
                if ($scope.Origen === "OtroImpuestos") {
                    $scope.$parent.steps.percent = 97;
                    $scope.Boton.Aceptar = true;
                    $scope.MensajeOb.Nota1 = "Estimado Usuario(a), sus datos han sido guardados correctamente," +
                        "  por favor haga click en el botón aceptar para continuar.";
                    $("#mdCargando").modal("show");
                    $scope.Boton.Siguiente = false;

                } else {
                    setTimeout(function () { history.back(); }, 500);
                }

            } else {
                servicioGeneral.addAlert('error','Error!', pl.data.Mensaje);
            }

            $scope.Boton.Guardar = false;
            byaPage.EndLoading();
        }, byaPage.errorPeticion);
    };
    function guardarTerceroModificado() {
        $scope.procesando = true;
        byaPage.Loading();
        var send = TerceroService.Modificar($scope.Tercero);
        send.then(function (pl) {
            if (!pl.data.Error) {
                nuevoTercero();
                servicioGeneral.addAlert('success', pl.data.Mensaje);
                $scope.Boton.Siguiente = false;
                setTimeout(function () {
                    window.location.href = "/#/app/datos_basicos/terceros/gestion_terceros";
                }, 500);

            } else {
                servicioGeneral.addAlert('error', pl.data.Mensaje);
            }
            byaPage.EndLoading();
            $scope.Boton.Guardar = false;
        }, byaPage.errorPeticion);
    };

    function nuevoTercero() {
        $scope.Tercero = {
            TipoIdentidad: "CC",
            TipoPersona: "NATURAL",
            Pais: "COLOMBIA",
            Beneficiarios: "NO",
            Regimen: "COMUN",
            GranContribuyente: "NO",
            Accion: "NUEVO"
        };
        habilitarcampos();
    };
    $scope.CambioDepartamento = function () {
        $scope.Departamentos.Seleccionado.Municipios.Seleccionado = $scope.Departamentos.Seleccionado.Municipios[0];
        $scope.Tercero.Departamento = $scope.Departamentos.Seleccionado.NombreDepartamento;
        $scope.Tercero.Ciudad = $scope.Departamentos.Seleccionado.Municipios.Seleccionado.NombreMunicipio;
    };
    $scope.CambioMunicipio = function () {
        $scope.Tercero.Ciudad = $scope.Departamentos.Seleccionado.Municipios.Seleccionado.NombreMunicipio;
    };

    $scope.mostrarDireccion = function (objetoContenedor) {
        $("#modalDireccion").modal("show");
        nuevaDireccion();
    };

    $scope.ocultarDireccion = function () {
        $("#modalDireccion").modal("hide");
    };

    function nuevaDireccion() {
        $scope.Direccion.Generada = "";
        $scope.Direccion.Complemento = "";
        $scope.Direccion.tipoViaPrimaria = "";
        $scope.Direccion.numeroViaPrincipal = "";
        $scope.Direccion.letraViaPrincipal = "";
        $scope.Direccion.bis = "";
        $scope.Direccion.letraBis = "";
        $scope.Direccion.cuadranteViaPrincipal = "";
        $scope.Direccion.numeroViaGeneradora = "";
        $scope.Direccion.letraViaGeneradora = "";
        $scope.Direccion.numeroPlaca = "";
        $scope.Direccion.cuadranteViaGeneradora = "";
        $scope.Direccion.componenteComplemento = "";
        $scope.Direccion.valorComplemento = "";
    }
    $scope.DireccionPrincipal = function () {
        if ($scope.Direccion.tipoViaPrimaria == undefined)
            $scope.Direccion.tipoViaPrimaria = "";

        if ($scope.Direccion.numeroViaPrincipal == undefined) {
            $scope.Direccion.numeroViaPrincipal = "";
        }
        if ($scope.Direccion.letraViaPrincipal == undefined) {
            $scope.Direccion.letraViaPrincipal = "";
        }
        if ($scope.Direccion.bis == undefined) {
            $scope.Direccion.bis = "";
        }
        if ($scope.Direccion.letraBis == undefined) {
            $scope.Direccion.letraBis = "";
        }
        if ($scope.Direccion.cuadranteViaPrincipal == undefined) {
            $scope.Direccion.cuadranteViaPrincipal = "";
        }
        if ($scope.Direccion.numeroViaGeneradora == undefined) {
            $scope.Direccion.numeroViaGeneradora = "";
        }
        if ($scope.Direccion.letraViaGeneradora == undefined) {
            $scope.Direccion.letraViaGeneradora = "";
        }
        if ($scope.Direccion.numeroPlaca == undefined)
            $scope.Direccion.numeroPlaca = "";

        if ($scope.Direccion.cuadranteViaGeneradora == undefined) {
            $scope.Direccion.cuadranteViaGeneradora = "";
        }
        if ($scope.Direccion.Complemento == undefined) {
            $scope.Direccion.Complemento = "";
        }


        $scope.Direccion.Generada = $scope.Direccion.tipoViaPrimaria;

        if ($scope.Direccion.tipoViaPrimaria !== "AV") {

            if ($scope.Direccion.numeroViaPrincipal !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.numeroViaPrincipal;
            }
            if ($scope.Direccion.letraViaPrincipal !== "") {
                $scope.Direccion.Generada += $scope.Direccion.letraViaPrincipal;
            }

            if ($scope.Direccion.bis !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.bis;
            }

            if ($scope.Direccion.letraBis !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.letraBis;
            }


            if ($scope.Direccion.cuadranteViaPrincipal !== "") {
                $scope.Direccion.Generada += " " + $scope.Direccion.cuadranteViaPrincipal;
            }

        }
        if ($scope.Direccion.numeroViaGeneradora != "") {
            $scope.Direccion.Generada += " " + $scope.Direccion.numeroViaGeneradora;
        }

        if ($scope.Direccion.letraViaGeneradora != "") {
            $scope.Direccion.Generada += $scope.Direccion.letraViaGeneradora;
        }

        if ($scope.Direccion.numeroPlaca != "") {
            var cero = ""
            /*if (this.document.frmDireccionNotificacion.numeroPlaca.value<10)
            {
            cero="0";
            }*/
            $scope.Direccion.Generada += " " + cero + $scope.Direccion.numeroPlaca;
        }
        if ($scope.Direccion.cuadranteViaGeneradora != "") {
            $scope.Direccion.Generada += " " + $scope.Direccion.cuadranteViaGeneradora;
        }

    };

    $scope.ValidarCuadranteViaGeneradora = function () {
        if ($scope.Direccion.cuadranteViaPrincipal == $scope.Direccion.cuadranteViaGeneradora) {
            $scope.Direccion.cuadranteViaPrincipal = "";
            $scope.Direccion.cuadranteViaGeneradora = "";
        }
        $scope.DireccionPrincipal();
    }

    $scope.ValidarCuadrantePrincipal = function () {
        if ($scope.Direccion.tipoViaPrimaria == "AC" || $scope.Direccion.tipoViaPrimaria == "CL" || $scope.Direccion.tipoViaPrimaria == "DG") {
            if ($scope.Direccion.cuadranteViaPrincipal != "SUR") {
                $scope.Direccion.cuadranteViaPrincipal = "";
            }
        } else {
            if ($scope.Direccion.cuadranteViaPrincipal != "ESTE") {
                $scope.Direccion.cuadranteViaPrincipal = "";
            }
        }
        $scope.DireccionPrincipal();
    }

    $scope.BorrarDireccion = function () {
        $scope.Direccion.Generada = "";
        $scope.Direccion.Complemento = "";
    }

    $scope.SetFocusDireccion = function () {
        $("#txtTerceroDireccion").focus();
    }
    $scope.GenerarDireccion = function () {
        $scope.Tercero.Direccion = $scope.Direccion.Generada;
        $scope.ocultarDireccion();
        $("#txtTerceroDireccion").focus();
    }

    $scope.ComplementoDireccion = function () {
        $scope.Direccion.Generada += " " + $scope.Direccion.componenteComplemento + " " + $scope.Direccion.valorComplemento;
        $scope.Direccion.valorComplemento = "";
    };
}]);
