﻿'use strict';
app.controller('SoporteCtrl', ['$scope', '$stateParams', '$state', 'servicioGeneral', 'UsuarioService',
    function ($scope, $stateParams, $state, servicioGeneral, UsuarioService) {
        var img = document.getElementById("imgCanvas");
        var canvas = document.getElementById("canvasPan");
        $scope.TituloApp = byaSite.getAplicacion() + " - " + byaSite.getAplicacionDescripcion();
        $scope.Foto = function () {
            var canvas = document.querySelector("#canvasPan");
            html2canvas(document.querySelector("body"), { canvas: canvas }).then(function (canvas) {
                img.src = canvas.toDataURL("image/png");
            });
            setTimeout(function () {
                $("#modalSoporte").modal("show");
            }, 1500);
        };
    $scope.Enviar = function (obj) {
        var e = {};
        e.Url = window.location.href;
        e.Asunto = obj.Asunto;
        e.Mensaje = obj.Mensaje;
        e.Imagen = canvas.toDataURL("image/png");
        e.IdentificacionRemitente = byaSite.getUsuario();
        e.NombreAplicacion = byaSite.aplicacion;
        e.NombreEmpresa = "ENTIDAD";
        $.ajax({
            type: "POST",
            url: "api/Soporte",
            data: JSON.stringify(e),
            async: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#modalSoporte").modal("hide");
                if (result.Error == false) {
                    servicioGeneral.addAlert('success', 'Su mensaje ha sido enviado a soporte satisfactoriamente.');
                    $("#txtAsuntoSoporte").val("");
                    $("#txtMensajeSoporte").val("");
                } else {
                    servicioGeneral.addAlert('danger', result.Mensaje + ' Porfavor intente mas tarde');
                }
            },
            error: byaPage.errorPeticionAjax
        });
    }
   
}]);