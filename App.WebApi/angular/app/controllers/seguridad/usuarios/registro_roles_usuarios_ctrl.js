﻿'use strict';
app.controller('RegistroRolesUsuariosCtrl', ['$rootScope', '$scope', 'UsuarioService', 'AccountService', '$filter', 'MenuService', 'toastr', 'servicioGeneral',
    function ($rootScope, $scope, UsuarioService, AccountService, $filter, MenuService, toastr, servicioGeneral) {
        // obj´s del modal para buscar
        $scope.Usuarios = [];
        $scope.alerts = [];
        $scope.Roles = [];
        $scope.Lista = [];
        $scope.UserName = undefined;
        $scope.Menu = { Todos: false };
        // FIN obj´s del modal para buscar

        // Obj cargar usuarios
        $scope.cargado = false;

        $scope.UsuarioOptions = {
            data: [],
            aoColumns: [
                { mData: 'UserName' },
                { mData: 'FirstName' },
                { mData: 'JoinDate' },
                { mData: 'BtnRoles' }
            ],
            "searching": true,
            "iDisplayLength": 25,
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": " No hay órdenes registradas ",
                "infoFiltered": "(filtro de _MAX_ registros totales)",
                "search": " Filtrar : ",
                "oPaginate": {
                    "sPrevious": "Anterior",
                    "sNext": "Siguiente"
                }
            },
            "aaSorting": []
        };
        // FIN Obj cargar usuarios

        _init();
        function _init() {
            servicioGeneral._nombreTitle("Registro de Roles");
            servicioGeneral._verificarPermiso("SEGURIDAD_GestionUsuarios");
            _cargarUsuarios();
        };
        
        function _cargarUsuarios() {
            var user = JSON.parse(localStorage.getItem("UserRoles"));
            if (user != null && user != "") {
                $scope.Usuario = user;
                _obtenerMenusPadres();
            }
        };
        function _obtenerMenusPadres() {
            byaPage.Loading("Buscando datos", "Por favor, espere...");
            var serUsuario = MenuService.getMenuPadres();
            serUsuario.then(function (pl) {
                byaPage.EndLoading();
                $scope.MenusPadres = pl.data;
            }, byaPage.errorPeticion);
        };

        $scope.actualizarPerfiles = function () {
            actualizarPerfiles();
        };

        $scope.actualizarSeleccionados = function (check) {
            for (var i in $scope.Lista) {
                var item = $scope.Lista[i];
                item.hasRol = check;
            }
        };
        $scope.buscarMenus = function () {
            $scope.Menu.Todos = false;
            byaPage.Loading("Buscando datos", "Por favor, espere...");
            var serUsuario = MenuService.ObtenerMenus($scope.Usuario.UserName, $scope.Menu.Padre);
            serUsuario.then(function (pl) {
                byaPage.EndLoading();
                $scope.Lista = pl.data;
                $scope.Menu.Todos = seleccioarTodos($scope.Lista);
            }, byaPage.errorPeticion);
        };
        function seleccioarTodos(lista) {
            if (!lista.find(buscarSeleccionados)) {
                return true;
            }
            return false;
        }
        function buscarSeleccionados(item)
        {
            return item.hasRol == false;
        };
        $scope.actualizarRoles = function () {
            actualizarRoles();
        };

        function actualizarRoles() {
            $scope.peticion = true;
            if ($scope.Usuario.UserName != undefined) {
                var lista = $scope.Lista;
                if (lista.length > 0) {
                    byaPage.Loading("Guardando cambios", "Por favor, espere...");
                    var serUsuario = UsuarioService.GuardarRoles($scope.Usuario.UserName, lista);
                    serUsuario.then(function (pl) {
                        byaPage.EndLoading();
                        servicioGeneral.addAlert(pl.data.Error == true ? "error" : "success", pl.data.Mensaje);
                        $scope.Menu.Todos = seleccioarTodos($scope.Lista);
                    }, byaPage.errorPeticion);

                }
                else {
                    servicioGeneral.addAlert(pl.data.Error == true ? "error" : "success", "El modulo no contiene roles asignadad o no ha escogido un modulo");
                }
            }
            else {
                servicioGeneral.addAlert(pl.data.Error == true ? "error" : "success", "No ha seleccionado un usuario valido");
            }
            $scope.peticion = false;
        };

        $scope.ShowRoles = function (UserName) {
            $scope.Roles = [];
            $scope.UserName = UserName;
            var promiseGet = RolService.getRoles(UserName);
            promiseGet.then(function (data) {
                $scope.Roles = data.data;
                //console.log(data.data);
            }, byaPage.errorPeticion);
        };

        $scope.AddUserToRol = function (o) {
            $scope.Roles = [];
            o.UserName = $scope.UserName;
            var promisePost = RolService.addRolToUserName(o);
            promisePost.then(function (d) {
                $scope.ShowRoles(o.UserName);
            }, byaPage.errorPeticion);
        };
        $scope.DeleteUserFromRol = function (o) {
            //console.log("borrar");
            $scope.Roles = [];
            o.UserName = $scope.UserName;
            var promisePost = RolService.removeRolToUserName(o);
            promisePost.then(function (d) {
                $scope.ShowRoles(o.UserName);
            }, byaPage.errorPeticion);
        };

    }]);
