﻿app.controller('GestionUsuariosCtrl', ['$scope', '$rootScope', 'UsuarioService', 'ngTableParams', 'ByATimeDate', 'servicioGeneral', 'TerceroService',
    function ($scope, $rootScope, UsuarioService, ngTableParams, ByATimeDate, servicioGeneral, TerceroService) {
        $scope.opened = false;
    $scope.peticion = false;
    $scope.Filtro = "";
    $scope.Usuarios = [];
    $scope.tableUsuarios = {};

    $scope.tercerosCambioContraseña = {};
    $scope.Usuario = {};
    $scope.Usuario.FechaDesactivacion = ByATimeDate.Now();
    
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.cambiarPassword = function (usuario) {
        $scope.terceroCambioPassword = usuario;
        $("#modalCambiarContraseña").modal("show");
    };

    $scope.buscarTercero = function () {
        if ($scope.Usuario && $scope.Usuario.UserName)
            buscarTercero($scope.Usuario.UserName);
    };
    function buscarTercero(username) {
        var send = TerceroService.Obtener(username);
        send.then(function (pl) {
            $scope.TerceroEncontrado = pl.data;
            if ($scope.TerceroEncontrado != null) {
                $scope.Usuario.Tercero = pl.data.NombreCompleto;
                $scope.Usuario.Email = pl.data.Email;
                UsuarioRegistrado($scope.Usuario);
            } else {
                $scope.Usuario.Tercero = "";
            }
        }, byaPage.errorPeticion);
    };
    $scope.enviarCambioPassword = function () {
        $("#modalCambiarContraseña").modal("hide");
        byaPage.Loading("Cambiando contraseña", "Por favor, espere...");
        var serUsuario = UsuarioService.PostForzar($scope.terceroCambioPassword);
        serUsuario.then(function (pl) {
            if (!pl.data.Error) {
                $("#modalCambiarContraseña").modal("hide");
                byaPage.EndLoading();
                servicioGeneral.addAlert("success", pl.data.Mensaje);
            } else {
                byaPage.EndLoading();
                $("#modalCambiarContraseña").modal("show");
            }
        }, byaPage.errorPeticion);
    };
    $scope.nuevoTercero = function () {
        window.location.href = "/#/app/Datosbasicos/terceros/registro_terceros";
    };
    $scope.VerificarEmail = function () {
        UsuarioRegistrado($scope.Usuario);
    };
    $scope.nuevoUsuario = function () {
        var FechaActual = ByATimeDate.Now();
        $scope.Usuario = { FechaDesactivacion: FechaActual.getFullYear() + "-12-31" };
        $("#modalNuevoUsuario").modal("show");
    };

    $scope._cambiarFechaDesactivar = function (item) {
        $scope.peticion = true;
    };
    $scope._guardarNuevo = function () {
        $scope.peticion = true;
        if ($scope.TerceroEncontrado != null) {
            if ($scope.Usuario.Password && $scope.Usuario.Password != null) {
                if ($scope.Usuario.Password != $scope.Usuario.PasswordConfirm) {
                    servicioGeneral.addAlert("danger", "Las contraseñas no coinciden.");
                } else if ($scope.Usuario.Password.length < 6) {
                    servicioGeneral.addAlert("danger", "Las contraseñas debe poseer por lo menos 6 caracteres.");
                } else {
                    guardarNuevoUsuario();
                }
            } else {
                servicioGeneral.addAlert("danger", "No ha ingresado una contraseña válida.");
            }
        } else {
            servicioGeneral.addAlert("danger", "La identificación que ha indicado no corresponde a ningún tercero registrado en el sistema.");
        }
        $scope.peticion = false;
    };

    function UsuarioRegistrado(usuario) {
        var send = UsuarioService.UsuarioRegistrado(usuario);
        send.then(function (pl) {
            if (pl.data.Error) {
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
            } else {
                usuario.Deshabilitar = false;
            }
        }, byaPage.errorPeticion)
    };
    function guardarNuevoUsuario() {
        $("#modalNuevoUsuario").modal("hide");
        byaPage.Loading("Guardando", "Por favor, espere...");
        $scope.peticion = true;
        var serUsuario = UsuarioService.Guardar($scope.Usuario);
        serUsuario.then(function (pl) {
            if (pl.data.Error == false) {
                $("#modalNuevoUsuario").modal("hide");
                byaPage.EndLoading();
                servicioGeneral.addAlert("success", pl.data.Mensaje);
            } else {
                $scope.peticion = false;
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
                byaPage.EndLoading();
                setTimeout(function () { $("#modalNuevoUsuario").modal("show"); }, 500);
            }
            _traerUsuarios();
        }, byaPage.errorPeticion);
    }
    $scope.mostrarFecha = function (fecha, op) {
        $scope.FechaFinal = fecha;
    };
    $scope._updateSesion = function (item) {
        byaPage.Loading("Cambiando estado", "Por favor, espere...");
        var send = UsuarioService.UpdateSesionUsuario(item.UserName);
        send.then(function (pl) {
            byaPage.EndLoading();
            _traerUsuarios();
            servicioGeneral.addAlert("success", pl.data.Mensaje);
        }, byaPage.errorPeticion)
    }
    $scope.ReSendConfirmationEmail = function (item) {
        byaPage.Loading("Enviando email", "Por favor, espere...");
        var send = UsuarioService.ReSendConfirmationEmail(item.UserName);
        send.then(function (pl) {
            byaPage.EndLoading();
            if (!pl.data.Error) {
                servicioGeneral.addAlert("sussess", pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
            }
        }, byaPage.errorPeticion)
    };
    $scope._irCambiarPassword = function (item) {
        $scope.Usuario = {};
        $scope.Usuario.UserName = item.UserName;
        $scope.Usuario.Tercero = (item.Tercero ? (item.Tercero.trim().lenght == 0 ? 'No Asignado' : item.Tercero) : 'No Asignado');
        $("#modalCambiarPassword").modal("show");
    };
    $scope._irRolesUsuario = function (item) {
        localStorage.setItem("UserRoles", JSON.stringify(item));
        window.location.href = "/#/app/seguridad/usuarios/registro_roles_usuarios";
    };

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.CambiarPassword = function () {
        $scope.peticion = true;
        $("#modalCambiarPassword").modal("hide");
        byaPage.Loading("Cambiando contraseña", "Por favor, espere...");
        if ($scope.Usuario.Password != $scope.Usuario.PasswordConfirm) {
            servicioGeneral.addAlert("danger", "Las contraseñas no coinciden.");
            byaPage.EndLoading();
            setTimeout(function () { $("#modalCambiarPassword").modal("show"); }, 500);
        } else if ($scope.Usuario.Password.length < 6) {
            servicioGeneral.addAlert("danger", "Las contraseñas debe poseer por lo menos 6 caracteres.");
            byaPage.EndLoading();
            setTimeout(function () { $("#modalCambiarPassword").modal("show"); }, 500);
        } else {
            var serUsuario = UsuarioService.PostForzar($scope.Usuario);
            
            serUsuario.then(function (pl) {
                if (pl.data.Error == false) {
                    servicioGeneral.addAlert("success", pl.data.Mensaje);
                    byaPage.EndLoading();
                } else {
                    servicioGeneral.addAlert("danger", pl.data.Mensaje);
                    byaPage.EndLoading();
                    setTimeout(function () { $("#modalCambiarPassword").modal("show"); }, 500);
                }
            }, byaPage.errorPeticion);
        }

        $scope.peticion = false;
    };


    $scope.AceptarValor = function (item) {
        byaPage.Loading("Cambiando fecha de desactivación", "Por favor, espere...");
        var serUsuario = UsuarioService.PostCambiarFechaDesactivar(item);
        serUsuario.then(function (pl) {
            if (pl.data.Error == false) {
                delete item.FechaActual;
                item.Editar = false;
                servicioGeneral.addAlert("success", pl.data.Mensaje);
            } else {
                servicioGeneral.addAlert("danger", pl.data.Mensaje);
                $scope.CancelarCambioValor(item);
            };
            byaPage.EndLoading();
        }, byaPage.errorPeticion);

    };

    $scope.CancelarCambioValor = function (item) {
        item.FechaDesactivacion = item.FechaActual;
        delete item.FechaActual;
        item.Editar = false;
    };
    $scope._verPerfiles = function (item) {
        getPerfiles(item.UserName);
    }

    function getPerfiles(username) {
        var servicio = PerfilesService.GetPermisosAsignables(username);
        servicio.then(function (success) {
            $scope.Perfiles = success.data;
        }, byaPage.errorPeticion);
    };
    _init();
    function _init() {

        servicioGeneral._nombreTitle("Administración de Usuarios");
        servicioGeneral._verificarPermiso("SEGURIDAD_GestionUsuarios");
        //$rootScope.stateLabel = "Administración de Usuarios";
        _dibujarTablaUsuarios();
        _traerUsuarios();
        $('#modalNuevoUsuario').on('shown.bs.modal', function () {
            $("#username-input").focus()
        });
    };
    function _traerUsuarios() {
        $scope.Peticion = "false";
        var promesa = UsuarioService.Obtener($scope.Filtro);
        promesa.then(function (pl) {
            $scope.Usuarios = pl.data;
            $scope.tableUsuarios.reload();
            $scope.Peticion = "true";
        }, byaPage.errorPeticion);
    };
    $scope.mostrarFecha = function (fecha) {
        $scope.Usuario.FechaDesactivacion = fecha;
    };
    function _dibujarTablaUsuarios() {
        $scope.tableUsuarios = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Usuarios.filter(function (a) {
                    return (a.UserName + "").toLowerCase().indexOf(c) > -1 ||
                           (a.Email + "").toLowerCase().indexOf(c) > -1
                })) : f = $scope.Usuarios, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
            }
        });
    };
}]);