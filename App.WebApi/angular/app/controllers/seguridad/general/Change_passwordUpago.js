﻿'use strict';

angular.module('app')
    .controller('ChangePasswordCtrlUpagos', ['$scope', 'AccountService', 'servicioGeneral', function ($scope, AccountService, servicioGeneral) {
        $scope.cpassword = '';
        $scope.cuenta = {
            OldPassword: undefined,
            NewPassword: undefined,
            ConfirmPassword: undefined,
            userName: byaSite.getUsuarioPagos()
        };


        $scope.EnviarCuenta = function () {
            var promiseGet = AccountService.ChangePassword($scope.cuenta);
            promiseGet.then(function (m) {
                servicioGeneral.addAlert("success", "Se ha cambiado la contraseña con exito!!.");
                console.log(m.data);
            }, byaPage.errorPeticion);
        };
    }]);