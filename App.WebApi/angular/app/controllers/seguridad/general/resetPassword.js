﻿'use strict';

angular.module('app')
    .controller('ResetPasswordCtrl', ['$scope', 'AccountService', 'toastr','servicioGeneral', function ($scope, AccountService, toastr,servicioGeneral) {
        $scope.Restablecido = false;
        $scope.cpassword = '';
        $scope.cuenta = {
            UserId: undefined,
            Code: undefined,
            Password: undefined,
            Email: undefined,
            ConfirmPassword: undefined,
        };
        

        $scope.EnviarCuenta = function () {
            $scope.Restablecido = true;
            var promiseGet = AccountService.ResetPassword($scope.cuenta);
            promiseGet.then(function (m) {
                servicioGeneral.addAlert("success", "Se ha restablecido la contraseña con exito!!.");
                console.log(m.data);
            }, byaPage.errorPeticion);
        };

        init();

        function init() {
            $scope.cuenta.UserId = $.urlParam('user');
            $scope.cuenta.Code = $.urlParam('code');
        }
}]);