﻿'use strict';
app.controller('LoginCtrl', ['$rootScope', '$scope', 'loginService', 'UsuarioService', 'servicioGeneral', 'AccountService',
    function ($rootScope, $scope, loginService, UsuarioService, servicioGeneral, AccountService) {
    $scope.User = {};
    $scope.alerts = [];

    $scope.login = function () {
        if (_esValido()) {
            var send = loginService.getToken($scope.User.Identificacion, $scope.User.Password);
            send.then(function (pl) {
                if (pl.data.access_token != null && pl.data.access_token != "") {
                    byaSite.setUsuario($scope.User.Identificacion);
                    byaSite.setTokenAdm(pl.data.access_token);
                    if (pl.data.Name) {
                        byaSite.setNombreUsuario(pl.data.Name);
                        byaSite.setTipoIdentificacion(pl.data.TipoIdentificacion);
                        
                    }
                    $scope.User = {};
                    _traerRolesUsuario();
                } else {
                    servicioGeneral.addAlert("danger", "Usuario o Contraseña incorrecto!!");
                }
            },  byaPage.errorPeticion);
        }
    };
    $scope.EnviarEmail = function () {
        $("#enviarReset").hide();
        var promiseGet = AccountService.ForgotPassword($scope.ForgotPasswordViewModel);
        promiseGet.then(function (m) {
            $("#enviarReset").hide();
            servicioGeneral.addAlert("success", "Se ha enviado un mensaje para reestablecer su contraseña. Por favor verifique la bandeja de entrada de su correo electrónico.");
        },  byaPage.errorPeticion);
    };
    function _esValido() {
        if ($scope.User.Identificacion != null && $scope.User.Identificacion != "" && $scope.User.Password != null && $scope.User.Password != "") {
            return true;
        } else {
            servicioGeneral.addAlert("danger", "Los campos marcados con '*' son obligatorios");
            return false;
        }
    };

    function irAOpcionPRincipal(opciones) {
        var opcion = opciones[0];
        if (opcion.Habilitado == 1) {
            return opcion.Url;
        } else {
            return "/#/app/inicio";
        }
    };

    function _traerRolesUsuario() {
        var ser = UsuarioService.GetRolesPersona(byaSite.getUsuario());
        ser.then(function (pl) {
            servicioGeneral.Roles = pl.data;
            $rootScope.lRolesPersona = pl.data;
            if ($rootScope.lRolesPersona.length > 0) {
                window.location.href = irAOpcionPRincipal($rootScope.lRolesPersona);
            } else {
                servicioGeneral.addAlert("danger","El usuario no tiene autorización para entrar");
                $rootScope.lRolesPersona = [];
            }
        }, byaPage.errorPeticion);
    };
}]);