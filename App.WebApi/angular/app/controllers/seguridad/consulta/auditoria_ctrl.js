﻿app.controller("ConsultaAuditoriaCtrl", ["$scope", "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "EstadosSistema", "servicioGeneral", "AuditoriaService", "$filter",
    function ($scope, $rootScope, $timeout, $window, $location, ngTableParams, EstadosSistema, servicioGeneral, AuditoriaService, $filter) {
    $scope.Auditorias = [];
    $scope.Estados = {};
    $scope.filtros = {};
    $scope.tableAuditoria = {};
    $scope.ConsultarAuditorias = function () {
        ConsultarAuditorias($scope.filtros);
    };
    $scope.MostrarDetalles = function (item) {
        ConsultarDetalles(item.TableName, item.RecordID);
    };
    $scope.cambiarEspaciosEnBlanco = function (cadena) {
        cadena = $filter("uppercase")(cadena);
        while (cadena.indexOf(' ') > -1)
            cadena = cadena.replace(' ', '&nbsp;');
        return cadena;
    };
    $scope.mostrarObjeto = function (item) {
        $scope.objeto = { TableName: item.TableName, RecordID: item.RecordID, detalles: JSON.parse(item.NewValue) };
        $("#modalDetalles").modal("show");
    }
    $scope.verDetalles = function (item) {
        $scope.objeto.objetoActual = JSON.parse(item.NewValue);
        $scope.objeto.Ver = 'SI';
    };
    _init();
    function _init() {
        _dibujarTablaAnotaciones();
        ConsultarDatosFormulario();
        servicioGeneral._nombreTitle("Consulta de registro de auditoría");
        servicioGeneral._verificarPermiso("SEGURIDAD_ConsultaAuditoria");
    };

    //////////////////////////////////////////METODOS/////////////////////////////////////////////////////////////////////
    function ConsultarDatosFormulario() {
        var send = AuditoriaService.ObtenerDatos();
        send.then(function (pl) {
            $scope.Modulos = pl.data.Modulos;
            $scope.Acciones = pl.data.Acciones;
            $scope.Tablas = pl.data.Tablas;
        }, function (error) {
            byaPage.errorPeticion(error);
        });
    };
    function ConsultarAuditorias(Request) {
        $scope.Auditorias = [];
        var send = AuditoriaService.Consultar(Request);
        send.then(function (pl) {
            if (!pl.data.Error) {
                $scope.Auditorias = pl.data;
                $scope.tableAuditoria.reload();
            } else {
                servicioGeneral.addAlert('danger', pl.data.Mensaje);
            }
            byaPage.EndLoading();
        }, function (error) {
            $scope.tableAuditoria.reload();
            byaPage.errorPeticion(error);
        });
    }
    function ConsultarDetalles(Tabla, RecorID) {
        $scope.Detalles = [];
        var send = AuditoriaService.Detalles(Tabla, RecorID);
        send.then(function (pl) {
            $scope.objeto = { TableName: Tabla, RecordID: RecorID, Detalles: [], Ver: 'NO' };
            $("#modalDetallesRegistro").modal("show");
            $scope.objeto.detalles = pl.data;
            console.log(pl.data);
        }, function (error) {
            byaPage.errorPeticion(error);
        });
    }
    function _dibujarTablaAnotaciones() {
        $scope.tableAuditoria = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Auditorias.filter(function (a) {
                    return (a.Id + "").toLowerCase().indexOf(c) > -1 ||
                        (a.IdentificacionContratista + "").toLowerCase().indexOf(c) > -1 ||
                        (a.NombreContratista + "").toLowerCase().indexOf(c) > -1 ||
                        (a.FechaInicio + "").toLowerCase().indexOf(c) > -1 ||
                        (a.ValorLibranza + "").toLowerCase().indexOf(c) > -1 ||
                        (a.NumeroCuotas + "").toLowerCase().indexOf(c) > -1 ||
                        (a.SaldoDisponible + "").toLowerCase().indexOf(c) > -1;
                })) : f = $scope.Auditorias, b.total(f.length), a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()));
            }
        });
    };
}]);