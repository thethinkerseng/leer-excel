﻿'use strict';
app.controller('NavBarCtrl', ['$scope', 'servicioGeneral', '$filter', function ($scope, servicioGeneral, $filter) {
    _init();
    function _init() {
        var usernameGeneral = byaSite.getUsuario();
        servicioGeneral.establecerNombre = establecerNombre;
        if (usernameGeneral != "admin") {
            var tercero = servicioGeneral.GetTercero();
            establecerNombre(tercero.NombreCompleto ? tercero.NombreCompleto : usernameGeneral);
        } else {
            establecerNombre(usernameGeneral);
        }
    };
    function establecerNombre(nombre) {
        $scope.usernameGeneral = nombre;
    }
    $scope.colocarEspaciosEnBlanco = function (cadena) {
        while (cadena.length < 15) cadena = " " + cadena + " ";
        return cambiarEspaciosEnBlanco(cadena);
    }
    function cambiarEspaciosEnBlanco(cadena) {
        cadena = $filter("uppercase")(cadena);
        while (cadena.indexOf(' ') > -1) cadena = cadena.replace(' ', '&nbsp;');
        return cadena;
    };

}]);