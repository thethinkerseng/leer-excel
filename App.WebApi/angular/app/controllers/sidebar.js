﻿'use strict';
app.controller('SideBarCtrl', ['$rootScope', '$scope', 'menuService', function ($rootScope, $scope, menuService) {
    _init();
    function _init() {
        _cargarMenu();
    };
    function _cargarMenu() {
        var promiseGet = menuService.getMenu();
        promiseGet.then(function (pl) {
            $scope.MenuApp = pl.data;
        }, byaPage.errorPeticion);
    };

}]);