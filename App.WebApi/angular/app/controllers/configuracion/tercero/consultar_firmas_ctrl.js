﻿app.controller("ConsultarFirmasCtrl", ["$scope", 'TercerosService', 'ContribuyentesService', "$rootScope", "$timeout", "$window", "$location", "ngTableParams", "TercerosService", "toaster", function ($scope, TercerosService, ContribuyentesService, $rootScope, $timeout, $window, $location, ngTableParams, TercerosService, toaster) {
    $scope.Filtro = "";
    $scope.Peticion = "false";
    $scope.Firmas = [];
    $scope.tableTerceros = {};
    $scope.Identificacion = "";

    $scope.agregar = function () {
        $scope.Firma = {};
        $('#firmaTercero').removeAttr("src");
        $("#firma-tercero input[type=text]").val('');
        $("#textArchivos").val("");
        $("#mdMensaje").modal("show");

    }
    $scope.eliminar = function (item) {
        if (confirm("Seguro que desea eliminar el registro?")) {
            _eliminar(item.TerceroFirmaID);
        }
    };
    $scope.guardar = function () {
        _guardarFirmaTercero($scope.Firma);
    }
    $scope.addImage = function (files) {
        if (!window.FileReader) {
            toaster.pop('error', '¡Aviso!', 'El navegador no soporta la lectura de archivos');
        }
        var file = files[0],
       imageType = /image.*/;
        if (!file.type.match(imageType)) {
            toaster.pop('error', '¡Aviso!', 'El archivo a adjuntar no es una imagen');
            return;
        }
        var img = new Image();
        var error = false;
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    };
    $scope.buscarTercero = function (firma) {
        //var filtro = (firma.NombreCompleto ? firma.NombreCompleto : "") + "," + (firma.Identificacion ? firma.Identificacion : "");
        var filtro = "," + (firma.Identificacion ? firma.Identificacion : "");
        _traerTercerosFiltro(filtro);
    }
    function fileOnload(e) {
        var result = e.target.result;
        $scope.Firma.Firma = result;
        $('#firmaTercero').attr("src", result);
        
    };
    function _eliminar(id) {
        var send = TercerosService.PostEliminarFirmaTerceros(id);
        send.then(function (pl) {
            if (!pl.data.Error) {
                _traerFirmasTerceros();
                toaster.pop('success', '¡Eliminado!', pl.data.Mensaje);
            } else {
                toaster.pop('error', '¡Error!', pl.data.Mensaje);
            }
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    _init();
    function _init() {
        $rootScope.Page.Title = "Gestión Firmas de Terceros";
        $rootScope._verificarPermiso("CONFFirmaTerceros");
        _dibujarTablaTerceros();
        _traerFirmasTerceros();
        inicializarValidaAltoAnchoImagen();
    };
    function inicializarValidaAltoAnchoImagen() {
        $('#firmaTercero').load(function () {
            if (this.width > 300 || this.width < 200) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                toaster.pop('error', '¡Aviso!', 'El ancho debe estar en el rango de 200px a 250px');
                this.removeAttribute('src');
                $scope.$apply();
            }
            else if (this.height > 160 || this.height < 70) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                toaster.pop('error', '¡Aviso!', 'El alto debe estar en el rango de 90px a 120px');
                this.removeAttribute('src');
                $scope.$apply();
            }
        });
    }
    function _guardarFirmaTercero() {
        var send = TercerosService.PostCrearTercerosFirma($scope.Firma);
        send.then(function (pl) {
            toaster.pop(!pl.data.Error ? 'success' : 'error', !pl.data.Error ? 'Guardado!' : 'Error!', pl.data.Mensaje);
            _traerFirmasTerceros();
            if (!pl.data.Error)
                $("#mdMensaje").modal("hide");
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    function _traerFirmasTerceros() {
        $scope.Peticion = "false";
        var send = TercerosService.GetFirmaTercero();
        send.then(function (pl) {
            $scope.Firmas = pl.data;
            $scope.tableTerceros.reload();
            $scope.Peticion = "true";
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    function _traerTercerosFiltro(filtro) {
        $scope.Peticion = "false";
        var send = TercerosService.GetFiltroTercero(filtro);
        send.then(function (pl) {
            asignarTerceroEnDormulario(pl.data);
            console.log($scope.Terceros)
            $scope.Peticion = "true";
        }, function (pl) {
            byaSite.console(pl);
        });
    };
    function asignarTerceroEnDormulario(terceros) {
        if (terceros.length > 0) {
            $scope.Firma.NombreCompleto = terceros[0].NombreCompleto;
            $scope.Firma.Identificacion = terceros[0].Identificacion;
            $scope.Firma.TerceroID = terceros[0].TerceroID;
        } else {
            toaster.pop('error', 'Información!', "El tercero no ha sido encontrado en el sistema");
        }
    }
    function _dibujarTablaTerceros() {
        $scope.tableTerceros = new ngTableParams({
            page: 1,
            count: 100
        }, {
            filterDelay: 50,
            total: 1000,
            getData: function (a, b) {
                var c = b.filter().Filtro,
                    f = [];
                c ? (c = c.toLowerCase(), f = $scope.Firmas.filter(function (a) {
                    return (a.Identificacion + "").toLowerCase().indexOf(c) > -1 ||
                           (a.NombreCompleto + "").toLowerCase().indexOf(c) > -1
                })) : f = $scope.Firmas, f = f, a.resolve(f.slice((b.page() - 1) * b.count(), b.page() * b.count()))
            }
        });
    };

    function _muestraMensaje(titulo, mensaje, tipo) {
        PNotify.prototype.options.styling = "fontawesome";
        //alert(PNotify.prototype.options.styling);
        new PNotify({
            title: titulo,
            text: mensaje,
            type: tipo
        });
    };
    
}]);