﻿app.controller("ActualizarFirmaCtrl", ["$scope", "$timeout", "FirmaTerceroService", "servicioGeneral", "EnvioDatosService",
    function ($scope, $timeout, FirmaTerceroService, servicioGeneral, EnvioDatosService) {

        $scope.guardarFirmaTercero = function () {
            guardarFirmaTercero($scope.Firma);
        };
        $scope.buscarTercero = function () {

        };
        $scope.addImage = function (files) {
            if (!window.FileReader) {
                servicioGeneral.addAlert('danger', 'El navegador no soporta la lectura de archivos');
            }
            var file = files[0],
                imageType = /image.*/;
            if (!file.type.match(imageType)) {
                servicioGeneral.addAlert('warning', 'El archivo a adjuntar no es una imagen');
                return;
            }
            var img = new Image();
            var error = false;
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
        };
        $scope.cerrarModal = function () {
            servicioGeneral.cerrarModal();
        };
    function fileOnload(e) {
        var result = e.target.result;
        $scope.Firma.Firma = result;
        $('#firmaTercero').attr("src", result);
    };
    _init();
    function _init() {
        EnvioDatosService.data.inicializar = inicializar;
    };
    function inicializar() {
        servicioGeneral._verificarPermiso("CONFIGURACION_ActualizarFirma", regresar);
        //inicializarValidaAltoAnchoImagen();
        verificarTercero();
    }
    function inicializarValidaAltoAnchoImagen() {
        $('#firmaTercero').load(function () {
            if (this.width > 300 || this.width < 200) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                servicioGeneral.addAlert('warning', 'El ancho debe estar en el rango de 200px a 250px');
                this.removeAttribute('src');
                $scope.$apply();
            }
            else if (this.height > 160 || this.height < 70) {
                $scope.Firma.Firma = "";
                $("#textArchivos").val("");
                $("#firma-tercero input[type=text]").val('');
                servicioGeneral.addAlert('error', '¡Aviso!', 'El alto debe estar en el rango de 90px a 120px');
                this.removeAttribute('src');
                $scope.$apply();
            }
        });
    }
    function GetObjeto() {
        var formData = new FormData();
        formData.append("Identificacion", $scope.Firma.Identificacion);
        formData.append("Firma", $("#textArchivos").get(0).files[0]);
        return formData;
    }
    function guardarFirmaTercero() {
        var send = FirmaTerceroService.Guardar(GetObjeto());
        send.then(function (pl) {
            regresar();
            servicioGeneral.addAlert('success', pl.data.Mensaje);
        }, byaPage.errorPeticion);
    };

    function buscarTercero(identificacion) {
        var send = FirmaTerceroService.ObtenerTercero(identificacion);
        send.then(function (pl) {
            $scope.Firma.Identificacion = pl.data.Identificacion;
            $scope.Firma.NombreCompleto = pl.data.NombreCompleto;
        }, byaPage.errorPeticion);
    };
    function verificarTercero() {
        var tercero = EnvioDatosService.data.Tercero;
        $scope.Firma = {};
        if (tercero) {
            $scope.Firma.Identificacion = tercero.Identificacion;
            $scope.Firma.NombreCompleto = tercero.NombreCompleto;
            $scope.Firma.Deshabilitar = true;
        } else {
            regresar();
        }
    }
    function regresar() {
        servicioGeneral.cerrarModal();
    }
    
}]);