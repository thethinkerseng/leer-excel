﻿

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify');

/*
* Configuración de la tarea 'demo'
*/
gulp.task('controllers', function () {
    gulp.src(['angular/app/services/*.js', 'angular/app/factory/*.js', 'angular/app/controllers/*.js', 'angular/app/controllers/**/*.js', 'angular/app/controllers/**/**/*.js'])
        .pipe(concat('vendors.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('angular/app/dist/'))
});
   
gulp.task('watch', function () {
    gulp.watch([
        'angular/app/services/*.js',
        'angular/app/factory/*.js',
        'angular/app/controllers/*.js', 
        'angular/app/controllers/**/*.js',
        'angular/app/controllers/**/**/*.js',
        ], ['controllers']);
});


gulp.task('default', ['controllers', 'watch']);