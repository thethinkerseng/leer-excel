﻿using MediatR;
using App.Core.UseCase;
using App.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using App.Core.UseCase.Contracts;
using App.Infraestructura.System;
using App.WebApi.Extensiones;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.IO;

namespace App.WebApi.Controllers
{
 //   [Authorize(Roles = "DATOSBASICOS_RegistroTerceros,DATOSBASICOS_GestionTerceros")]
    [Authorize]
    [RoutePrefix("api/excel")]
    public class ExcelController : ApiController
    {
        private readonly IMediator _mediator = null;
        private readonly IMapper _mapper = null;
        public ExcelController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Post()
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.ToText());
            ImportarExcelRequest archivo = new ImportarExcelRequest();
            var request = HttpContext.Current.Request;
            string[] extencion = request.Files[0].FileName.Split(new char[] { '.' });
            archivo.ArchivoExtension = extencion[extencion.Count() - 1].ToLower();
            archivo.ArchivoBytes = new byte[request.Files[0].ContentLength];
            request.Files[0].InputStream.Read(archivo.ArchivoBytes, 0, request.Files[0].ContentLength);
            archivo.ArchivoNombre = request.Files[0].FileName;
            archivo.ArchivoLong = request.Files[0].ContentLength;
            archivo.ArchivoTipo = GetMimeType(request.Files[0].FileName);
            archivo.Fecha = ObtenerFecha(request.Params.Get("Fecha"));
            archivo.Hora = ObtenerHora(request.Params.Get("Hora"));
            Task<ImportarExcelResponse> _resultado = _mediator.Send(archivo);
            if (_resultado.Result.ValidationResult.IsValid)
            {
                ByARtpaViewModel resultado = new ByARtpaViewModel();
                _mapper.Map<ImportarExcelResponse, ByARtpaViewModel>(_resultado.Result, resultado);
                return Ok(resultado);
            }
            else return BadRequest(_resultado.Result.ValidationResult.ToText());
        }
        [Route("{Id}/Eliminar")]
        [HttpPost]
        public IHttpActionResult Eliminar(int Id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.ToText());
            Task<EliminarExcelResponse> _resultado = _mediator.Send(new EliminarExcelRequest() { Id = Id });
            if (_resultado.Result.ValidationResult.IsValid)
            {
                ByARtpaViewModel resultado = new ByARtpaViewModel();
                _mapper.Map<EliminarExcelResponse, ByARtpaViewModel>(_resultado.Result, resultado);
                return Ok(resultado);
            }
            else return BadRequest(_resultado.Result.ValidationResult.ToText());
        }
        private DateTime ObtenerFecha(string campo)
        {
            DateTime fecha = DateTime.Now;
            if (DateTime.TryParse(campo, out fecha))
                return fecha;
            else return DateTime.Now;
        }
        private TimeSpan ObtenerHora(string campo)
        {
            TimeSpan hora = DateTime.Now.TimeOfDay;
            if (TimeSpan.TryParse(campo, out hora))
                return hora;
            else return DateTime.Now.TimeOfDay;
        }

        [Route("Modificar")]
        [HttpPost]
        public IHttpActionResult PostModificar(ExcelViewModelsResquest actasViewModels)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.ToText());

            EditarExcelRequest actaRequest = new EditarExcelRequest();
            _mapper.Map<ExcelViewModelsResquest, EditarExcelRequest>(actasViewModels, actaRequest);
            Task<EditarExcelResponse> _resultado = _mediator.Send(actaRequest);

            if (_resultado.Result.ValidationResult.IsValid)
            {
                ByARtpaViewModel resultado = new ByARtpaViewModel();
                _mapper.Map<EditarExcelResponse, ByARtpaViewModel>(_resultado.Result, resultado);
                return Ok(resultado);
            }
            else return BadRequest(_resultado.Result.ValidationResult.ToText());
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            Task<ConsultarExcelResponse> resultado = _mediator.Send(new ConsultarExcelRequest() { });
            List<ExcelViewModels> actasViewsModels = new List<ExcelViewModels>();
            _mapper.Map(resultado.Result.Resultados, actasViewsModels);
            return Ok(actasViewsModels);
        }

        [Route("Filtrar")]
        [HttpGet]
        public IHttpActionResult FiltrarPorFecha(DateTime? FechaInicio = null, DateTime? FechaFinal = null)
        {
            Task<FiltrarExcelResponse> resultado = _mediator.Send(new FiltrarExcelRequest() { FechaFinal = FechaFinal, FechaInicio = FechaInicio });
            List<ExcelViewModels> actasViewsModels = new List<ExcelViewModels>();
            _mapper.Map(resultado.Result.Actas, actasViewsModels);
            return Ok(actasViewsModels);
        }


        [Route("Items/Filtrar")]
        [HttpGet]
        public IHttpActionResult FiltrarItemsPorFecha(DateTime? FechaInicio = null, DateTime? FechaFinal = null)
        {
            Task<ConsultarRegistrosResponse> resultado = _mediator.Send(new ConsultarRegistrosRequest() { FechaFinal = FechaFinal, FechaInicio = FechaInicio });
            return Ok(resultado.Result.Resultado);
        }
        [Route("{Id}/Items/Filtrar")]
        [HttpGet]
        public IHttpActionResult FiltrarPorID(int Id)
        {
            Task<ConsultarRegistrosExcelResponse> resultado = _mediator.Send(new ConsultarRegistrosExcelRequest() { Id = Id });
            return Ok(resultado.Result.Resultado);
        }
        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
    }
}