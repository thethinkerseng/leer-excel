﻿using MediatR;
using App.Core.UseCase;
using App.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using App.Core.UseCase.Contracts;
using App.Infraestructura.System;
using App.WebApi.Extensiones;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;


namespace App.WebApi.Controllers
{
 //   [Authorize(Roles = "DATOSBASICOS_RegistroTerceros,DATOSBASICOS_GestionTerceros")]
    [AllowAnonymous]
    [RoutePrefix("api/tercero")]
    public class TerceroController : ApiController
    {
        private readonly IMediator _mediator = null;
        private readonly IMapper _mapper = null;
        public TerceroController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [Route("")]
        public IHttpActionResult Post(TerceroViewModel terceroARegistrar)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.ToText());

            AgregarTerceroRequest terceroARegistrarRequest = new AgregarTerceroRequest();
            _mapper.Map<TerceroViewModel, AgregarTerceroRequest>(terceroARegistrar, terceroARegistrarRequest);

            Task<AgregarTerceroResponse> resultadoAgregarTercero = _mediator.Send(terceroARegistrarRequest);

            if (resultadoAgregarTercero.Result.ValidationResult.IsValid)
            {
                ByARtpaViewModel resultadoAgregarTerceroViewModel = new ByARtpaViewModel();
                _mapper.Map<AgregarTerceroResponse, ByARtpaViewModel>(resultadoAgregarTercero.Result, resultadoAgregarTerceroViewModel);
                return Ok(resultadoAgregarTerceroViewModel);
            }
            else return BadRequest(resultadoAgregarTercero.Result.ValidationResult.ToText());
        }
        [Route("Modificar")]
        public IHttpActionResult PostModificar(TerceroViewModel terceroAModificar)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.ToText());

            ModificarTerceroRequest terceroAModificarRequest = new ModificarTerceroRequest();
            _mapper.Map<TerceroViewModel, ModificarTerceroRequest>(terceroAModificar, terceroAModificarRequest);

            Task<ModificarTerceroResponse> resultadoModificarTercero = _mediator.Send(terceroAModificarRequest);

            if (resultadoModificarTercero.Result.ValidationResult.IsValid)
            {
                ByARtpaViewModel resultadoAgregarTerceroViewModel = new ByARtpaViewModel();
                _mapper.Map<ModificarTerceroResponse, ByARtpaViewModel>(resultadoModificarTercero.Result, resultadoAgregarTerceroViewModel);
                return Ok(resultadoAgregarTerceroViewModel);
            }
            else return BadRequest(resultadoModificarTercero.Result.ValidationResult.ToText());
        }
        [Route("Estado")]
        public IHttpActionResult PostEstado(TerceroEstadoViewModel terceroAModificar)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.ToText());

            EstadoTerceroRequest terceroAModificarRequest = new EstadoTerceroRequest();
            _mapper.Map<TerceroEstadoViewModel, EstadoTerceroRequest>(terceroAModificar, terceroAModificarRequest);

            Task<EstadoTerceroResponse> resultadoModificarTercero = _mediator.Send(terceroAModificarRequest);

            if (resultadoModificarTercero.Result.ValidationResult.IsValid)
            {
                ByARtpaViewModel resultadoAgregarTerceroViewModel = new ByARtpaViewModel();
                _mapper.Map<EstadoTerceroResponse, ByARtpaViewModel>(resultadoModificarTercero.Result, resultadoAgregarTerceroViewModel);
                return Ok(resultadoAgregarTerceroViewModel);
            }
            else return BadRequest(resultadoModificarTercero.Result.ValidationResult.ToText());
        }

        [Route("{Identificacion}")]
        public IHttpActionResult Get(string Identificacion)
        {
            Task<ConsultarTerceroResponse> tercero = _mediator.Send(new ConsultarTerceroRequest() { Identificacion = Identificacion });

            TerceroViewModel terceroViewModel = new TerceroViewModel();
            if (tercero.Result.ValidationResult.IsValid)
            {
                _mapper.Map<ConsultarTerceroResponse, TerceroViewModel>(tercero.Result, terceroViewModel);

                return Ok(terceroViewModel);
            }
            else return BadRequest(tercero.Result.ValidationResult.ToText());
        }
        [Route("")]
        public IHttpActionResult Get()
        {
            Task<ConsultarTodosLosTercerosResponse> tercero = _mediator.Send(new ConsultarTodosLosTercerosRequest() { });
            List<TerceroResumenViewModel> tercerosViewModel = new List<TerceroResumenViewModel>();
            _mapper.Map(tercero.Result.TercerosResponse, tercerosViewModel);
            return Ok(tercerosViewModel);
        }
    }
}