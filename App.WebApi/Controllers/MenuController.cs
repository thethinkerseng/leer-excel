﻿using ByA.Infraestructura.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Menu")]
    public class MenuController : ApiController
    {

        public MenuController()
        {

        }
        
        [Route("Padres")]
        public List<ModuloRoles> GetMenuPadres()
        {
            GesUsuarios mg = new GesUsuarios();
            return mg.GetModulosPadres();
        }
        [Route("Usuario/{usuario}/Modulo/{modulo}")]
        public List<ModuloRoles> GetMenuPadres(string usuario, string modulo)
        {
            GesUsuarios mg = new GesUsuarios();
            return mg.GetModulos(usuario, modulo);
        }
        [Route("{usuario}")]
        public List<Menu> GetMenus(string usuario)
        {
            GetMenuAngular mg = new GetMenuAngular();
            return mg.getOpciones(usuario);
        }

    }
}
