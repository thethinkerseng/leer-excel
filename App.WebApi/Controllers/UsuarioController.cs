﻿using ByA;
using ByA.Infraestructura.Security;
using ByA.Infraestructura.Security.User;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace App.WebApi.Controllers
{
    [RoutePrefix("api/Usuarios")]
    public class UsuarioController : ApiController
    {
        private ByA.Infraestructura.Security.ApplicationUserManager _userManager;

        public ByA.Infraestructura.Security.ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ByA.Infraestructura.Security.ApplicationUserManager>();
            }
            protected set
            {
                _userManager = value;
            }
        }
        [Route("{Username}/Roles")]
        public List<ModuloRolesConUrl> GetRoles(string UserName)
        {
            GesUsuarios gu = new GesUsuarios();
            return gu.GetRoles(UserName);
        }


        [Route("")]
        public Task<ByARpt> Post(Usuario Reg)
        {
            GesUsuarios gu = new GesUsuarios(this.UserManager);
            var url = new Uri(Url.Link("ConfirmEmailRoute", new { userId = "user.Id", code = "codigo" }));
            string URL_CONFIRMAR = url.AbsoluteUri;
            return gu.InsUsuarios(Reg, URL_CONFIRMAR);
        }

        [Route("Filtro")]
        public List<ViewUser> Gets()
        {
            GesUsuarios gu = new GesUsuarios();
            return gu.GetUsuarios("");
        }

        [Route("Filtro/{filtro}")]
        public List<ViewUser> Gets(string filtro)
        {
            GesUsuarios gu = new GesUsuarios();
            return gu.GetUsuarios(filtro);
        }

        [Route("UsuarioRegistrado")]
        public ByARpt PostUsuarioRegistrado(Usuario usuario)
        {
            GesUsuarios gu = new GesUsuarios(this.UserManager);
            return gu.UsuarioRegistrado(usuario);
        }
        [Route("UpdateSesionUsuario/{username}")]
        public ByARpt PostUpdateSesionUsuario(string username)
        {
            GesUsuarios gu = new GesUsuarios();
            return gu.ConfirmarEmailUsuario(username);
        }

        [Route("{Username}/Roles")]
        public ByARpt PostRoles(string UserName, List<ModuloRoles> lst)
        {
            GesUsuarios gu = new GesUsuarios();
            return gu.GuardarRoles(lst, UserName);
        }

        [Route("Password")]
        public ByARpt PostForzar(Usuario Reg)
        {
            GesUsuarios gu = new GesUsuarios(this.UserManager);
            return gu.Forzar_Cambio_Clave(Reg);
        }

        [HttpPost]
        [Route("~/public/api/Usuarios/ForgotPassword", Name = "ForgotPassword")]
        public async Task<IHttpActionResult> PostForgotPassword()
        {
            var request = HttpContext.Current.Request;
            var email = request.Params.Get("email");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            GesUsuarios gu = new GesUsuarios(this.UserManager);
            var url = new Uri(Url.Link("BeforeResetPasswordRoute", new { userId = "user.Id", code = "codigo" }));
            IdentityResult result = await gu.ForgotPassword(email, url.AbsoluteUri);

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return GetErrorResult(result);
            }

        }

        [HttpGet]
        [Route("~/public/api/Usuarios/BeforeResetPassword", Name = "BeforeResetPasswordRoute")]
        public RedirectResult BeforeResetPassword(string userId = "", string code = "")
        {
            string Host = HttpContext.Current.Request.Url.Host;
            int Port = HttpContext.Current.Request.Url.Port;
            return Redirect(string.Format("http://{0}:{1}", Host, Port) + "/angular/views/Accounts/reset_password.html?user=" + userId + "&code=" + code);
        }

        [HttpPost]
        [Route("~/public/api/Usuarios/ResetPassword")]
        public IHttpActionResult ResetPassword(ResetPasswordDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            GesUsuarios gs = new GesUsuarios();
            IdentityResult result = gs.ResetPass(model);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            else
            {
                return Ok();
            }
        }
        [Route("ReSendConfirmationEmail/{username}")]
        public async Task<ByARpt> getReSendConfirmationEmail(string username)
        {
            GesUsuarios gu = new GesUsuarios(this.UserManager);
            var url = new Uri(Url.Link("ConfirmEmailRoute", new { userId = "user.Id", code = "codigo" }));
            string URL_CONFIRMAR = url.AbsoluteUri;
            return await gu.ReSendConfirmationEmail(username, URL_CONFIRMAR);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("~/public/api/Usuarios/ConfirmEmail", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId = "", string code = "")
        {

            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {
                ModelState.AddModelError("", "User Id and Code are required");
                return BadRequest(ModelState);
            }
            GesUsuarios gu = new GesUsuarios(this.UserManager);
            IdentityResult result = await gu.ConfirmedEmail(userId, code);
            string Host = HttpContext.Current.Request.Url.Host;
            int Port = HttpContext.Current.Request.Url.Port;
            if (result.Succeeded)
            {
                return Redirect(string.Format("http://{0}:{1}", Host, Port) + "/angular/views/Accounts/Respuesta.html?id=valid");
            }
            else if (!result.Succeeded)
            {
                return Redirect(string.Format("http://{0}:{1}", Host, Port) + "/angular/views/Accounts/Respuesta.html?id=invalidtoken");
            }
            else
            {
                return GetErrorResult(result);
            }
        }
        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
