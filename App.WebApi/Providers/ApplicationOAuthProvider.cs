﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using App.WebApi.Models;
using App.Infraestructura.Data;
using App.Core.UseCase;
using ByA.Infraestructura.Security;

namespace App.WebApi.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private const string USUARIO_ADMIN = "admin";
        private readonly string _publicClientId;
        private readonly ITerceroRepository _terceroRepository;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public ApplicationOAuthProvider(string publicClientId, ITerceroRepository terceroRepository) : this(publicClientId)
        {
            this._terceroRepository = terceroRepository;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ByA.Infraestructura.Security.ApplicationUserManager>();

            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "El nombre de usuario o la contraseña no son correctos.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = null;
            string NombreCompleto = String.Empty, TipoIdentificacion = String.Empty;
            if (context.UserName != USUARIO_ADMIN)
            {
                var tercero = this._terceroRepository.FindBy(t => t.Identificacion == context.UserName).FirstOrDefault();
                if (tercero != null)
                {
                    NombreCompleto = tercero.NombreCompleto;
                    TipoIdentificacion = tercero.TipoIdentidad;
                }
            }

            if (string.IsNullOrEmpty(user.Tercero))
                properties = CreateProperties(user.UserName);
            else
            {
                properties = CreateProperties(user.UserName, NombreCompleto, TipoIdentificacion);
                cookiesIdentity.Label = user.Tercero;
            }

            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // La credenciales de la contraseña del propietario del recurso no proporcionan un id. de cliente.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }

        public static AuthenticationProperties CreateProperties(string userName, string nombre, string tipoIdentificacion)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "Name", nombre },
                { "TipoIdentificacion", tipoIdentificacion }
            };
            return new AuthenticationProperties(data);
        }
    }
}