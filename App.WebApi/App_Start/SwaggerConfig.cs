using System.Web.Http;
using WebActivatorEx;
using App.WebApi;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace App.WebApi
{
    public class SwaggerConfig
    {
        protected SwaggerConfig(){ }

        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            const string URL = "http://www.byasystems.com.co";
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "App.WebApi").Description("Api RestFull para la Aplicación Apfis 2.0")
                    .TermsOfService("Términos del Servicio")
                    .Contact(cc => cc
                        .Name("B&A Systems")
                        .Url(URL)
                        .Email("soporte@byasystems.com.co"))
                    .License(lc => lc
                        .Name("Private")
                        .Url(URL));
                    c.PrettyPrint();
                })
                .EnableSwaggerUi(c =>
                {
                    c.InjectJavaScript(thisAssembly, "App.WebApi.Scripts.swagger-api-auth.js");
                });
        }
    }
}
