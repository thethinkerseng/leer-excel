﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using App.WebApi.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;


namespace App.WebApi
{
    public class AutoFacConfig
    {
        protected AutoFacConfig() { }

        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new EfModule());

            builder.RegisterModule(new UseCaseModule());

            builder.RegisterModule(new ContractsModule());

            var config = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

        }
    }
}