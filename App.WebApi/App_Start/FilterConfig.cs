﻿using System.Web;
using System.Web.Mvc;

namespace App.WebApi
{
    public class FilterConfig
    {
        protected FilterConfig()
        {

        }
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
