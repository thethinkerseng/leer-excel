﻿using Autofac;
using App.Core.Entities.Contracts;
using App.Core.UseCase.Contracts;
using App.Infraestructura.System;
using App.WebApi.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Core.UseCase;


namespace App.WebApi.Modules
{
    public class ContractsModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(Logger)).As(typeof(ILogger)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(ByAMapper)).As(typeof(IMapper)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(ByAHtmlToPdf)).As(typeof(IHtmlToPdf)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(Sistema)).As(typeof(ISistema)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(ManejoCadena)).As(typeof(IUtilidadCadena)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(ExcelDatos)).As(typeof(IExcelDatos)).InstancePerLifetimeScope();
        }
    }
}