﻿using Autofac;
using App.Core.UseCase;
using App.Core.UseCase.Base;
using App.Infraestructura.Data;
using App.Infraestructura.Data.Base;
using App.WebApi.Providers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace App.WebApi.Modules
{
    public class EfModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterType(typeof(Infraestructura.Data.AppContext)).As(typeof(IDbContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();
            builder.RegisterGeneric(typeof(GenericRepository<>)).As(typeof(IGenericRepository<>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(GenericRepositoryAuditLog<>)).As(typeof(IGenericRepositoryNoAudit<>)).InstancePerRequest();
        }
    }
}