﻿using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace App.WebApi.Modules
{
    public class Configuracion: IConfig
    {
        readonly int addHora = 0;
        public Configuracion()
        {
            addHora = 0;
            int.TryParse(GetKey("horaZonaHoraria"), out addHora);

        }
        private HttpRequest _contexto { get; set; }

        public DateTime FechaYHora
        {
            get
            {
                return DateTime.UtcNow.AddHours(addHora);
            }
        }

        public Configuracion(HttpRequest contexto)
        {
            _contexto = contexto;
        }
        public string GetUsuario()
        {
            return _contexto.Params.Get(GetKey("usuario"));
        }

        public string GetUrl()
        {
            return _contexto.Url.AbsoluteUri.Replace(_contexto.Url.PathAndQuery, "/");
        }
        public string GetKey(string key)
        {
            string value = ConfigurationManager.AppSettings[key].ToString();
            return value;
        }
    }
}