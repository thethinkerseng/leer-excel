﻿// add a new ApiKeyAuthorization when the api-key changes in the ui.
$('#input_apiKey').change(function () {
    var key = $('#input_apiKey')[0].value;
    if (key && key.trim() != "") {
        swaggerUi.api.clientAuthorizations.add("key", new SwaggerClient.ApiKeyAuthorization("Authorization", "bearer " + key, "header"));
    }
});