﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System.Email
{
    public interface IDatosCorreo
    {
        string Email { get; set; }
        string Clave { get; set; }
        string EmailDestino { get; set; }
        string Nombre { get; set; }
        string Host { get; set; }
        int Port { get; set; }
    }
}
