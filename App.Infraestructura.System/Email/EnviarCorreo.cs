﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System.Email
{
    public class EnviarCorreo : IEmail
    {
        public EnviarCorreo()
        {
            _correo = new CorreoSoporte()
            {
                Email = "byasoporte@outlook.com",
                Host = "smtp-mail.outlook.com",//smtp.live.com
                Port = 587,
                Clave = "soportebya123#",
                EmailDestino = "bgonzales@byasystems.com.co",
                Nombre = "SIRCC ELC - SOPORTE",
            };
        }
        public EnviarCorreo(IDatosCorreo datos)
        {
            _correo = datos;
        }
        public IDatosCorreo _correo { get; set; }
        
        public void SendEmail(string Asunto, string Mensaje, string ImagenAdjunta)
        {
            if (_correo != null)
            {
                using (MailMessage MailSetup = new MailMessage())
                {
                    NetworkCredential loginInfo = new NetworkCredential(_correo.Email, _correo.Clave);
                    MailSetup.Subject = Asunto;
                    MailSetup.To.Add(_correo.EmailDestino);
                    MailSetup.From = new MailAddress(_correo.Email);
                    MailSetup.Body = Mensaje;
                    MailSetup.IsBodyHtml = true;

                    var bytes = Convert.FromBase64String(ImagenAdjunta);
                    Attachment att = new Attachment(new MemoryStream(bytes), "imageSoporte.png");
                    MailSetup.Attachments.Add(att);
                    using (SmtpClient SMTP = new SmtpClient(_correo.Host))
                    {
                        SMTP.Port = _correo.Port;
                        SMTP.EnableSsl = true;
                        SMTP.Credentials = loginInfo;
                        SMTP.Send(MailSetup);
                    }
                }
            }
            else
            {
                throw new NotImplementedException("No esta inicializada el envio de correo");
            }

        }
    }
}
