﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System.Email
{
    public class CorreoSoporte : IDatosCorreo
    {
        public string Email { get; set; }
        public string Clave { get; set; }
        public string EmailDestino { get; set; }
        public string Nombre { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
