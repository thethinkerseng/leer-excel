﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System.Email
{
    public interface IEmail
    {
        void SendEmail(string Asunto, string Mensaje, string ImagenAdjunta);
    }
}
