﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System
{
    public class ConfiguracionGlobal
    {
        protected ConfiguracionGlobal()
        {

        }
        public static string RutaArchivosInicializacion { get { return AppDomain.CurrentDomain.BaseDirectory; } } 
    }
}
