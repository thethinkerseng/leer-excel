﻿using App.Core.UseCase.Contracts;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System
{
    public class ExcelDatos: IExcelDatos
    {
        public string MensajeError { get; set; }
        
        public DataTable GetDataExcel(string fileName)
        {
            string extencion = Path.GetExtension(fileName);
            FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            DataTable dt = MapToDataTable(extencion, stream);
            return dt;
        }
        public DataTable GetDataExcel(byte[] archivo, string extension)
        {
            DataTable dt = MapToDataTable(extension, new MemoryStream(archivo));
            return dt;
        }
        public List<DataTable> GetListDataExcel(string fileName)
        {
            string extencion = Path.GetExtension(fileName);
            FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            List<DataTable> dt = MapToListDataTable(extencion, stream);
            return dt;
        }
        public List<DataTable> GetListDataExcel(byte[] archivo, string extencion)
        {
            List<DataTable> dt = MapToListDataTable(extencion, new MemoryStream(archivo));
            return dt;
        }

        private List<DataTable> MapToListDataTable(string extencion, Stream stream)
        {
            IExcelDataReader excelReader;
            List<DataTable> dt = new List<DataTable>();
            using (excelReader = GetIExcelDataReader(stream, extencion))
            {
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet();
                if (result.Tables.Count > 0)
                    for (int i = 0; i < result.Tables.Count; i++)
                        dt.Add(result.Tables[i]);
                else
                    throw new ArgumentException("No se encontraron hojas en el excel", "stream");
            }
            return dt;
        }
        private DataTable MapToDataTable(string extencion, Stream stream)
        {
            IExcelDataReader excelReader;
            DataTable dt = null;
            using (excelReader = GetIExcelDataReader(stream, extencion))
            {
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet();
                if (result.Tables.Count > 0)
                    dt = result.Tables[0];
                else
                    throw new ArgumentException("No se encontraron hojas en el excel", "stream");

            }
            return dt;
        }

        private IExcelDataReader GetIExcelDataReader(Stream stream, string extension)
        {
            switch (extension)
            {
                case "xls":
                    return ExcelReaderFactory.CreateBinaryReader(stream);
                case "xlsx":
                    return ExcelReaderFactory.CreateOpenXmlReader(stream);
                default:
                    throw new ArgumentException("Extensión de archivo no válido, permitidos (.xls, .xlsx)", "extension");
            }
        }

    }
}
