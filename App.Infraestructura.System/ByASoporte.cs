﻿using ByA;
using NLog;
using App.Infraestructura.System.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.System
{
    public class ByASoporte
    {
        private readonly IEmail _correo;
        private readonly ILogger _log;

        public ByASoporte(IEmail correo)
        {
            _correo = correo;
        }
        public ByASoporte(IEmail correo, ILogger log)
        {
            _log = log;
        }
        
        public ByARpt EnviarCorreo(MensajeSoporte message)
        {
            //Mapear el Tercero
            string Asunto = "SOPORTE " + message.NombreAplicacion + " - " + message.NombreEmpresa + ": " + message.Asunto;
            string Body = ArmarHTMLCorreoSoporte(message);
            string ImagenAdjunta = message.Imagen.Replace("data:image/png;base64,", "");

            _correo.SendEmail(Asunto, Body, ImagenAdjunta);
            if(_log!=null)
                _log.Error("Se envio el correo a soporte");
            return new ByARpt() { Error = false, Mensaje = "Se envio el correo a soporte" };
        }

        private string ArmarHTMLCorreoSoporte(MensajeSoporte objMensaje)
        {
            string strHtml = "";
            strHtml += @"<!DOCTYPE html>
                        <html lang='es'>
                        <head>
                            <title>MENSAJE SOPORTE</title>
                            <meta charset='utf-8' />
                        </head>
                        <body>
                            <div style='width:100%;text-align:center;height:40px;'>
                                <h2>MENSAJE SOPORTE</h2>
                            </div>
                            <div style='width:100%;text-align:justify;margin:0px;'>
                                <p><strong>PROGRAMA: </strong>" + objMensaje.NombreAplicacion + @"</p>
                                <p><strong>EMPRESA: </strong>" + objMensaje.NombreEmpresa + @"</p>
                                <p><strong>IDENT. REMITENTE: </strong>" + objMensaje.IdentificacionRemitente + @"</p>
                                <p><strong>NOMBRE REMITENTE: </strong>" + objMensaje.NombreRemitente + @"</p>
                                <p><strong>URL: </strong>" + objMensaje.Url + @"</p>
                                <p><strong>ASUNTO: </strong>" + objMensaje.Asunto + @"</p>
                                <p><strong>MENSAJE: </strong></p>
                                <p>" + objMensaje.Mensaje + @"</p>
                             </div>
                        </body>
                        </html>";
            return strHtml;
        }
    }
    public class MensajeSoporte 
    {
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
        public string Imagen { get; set; }
        public string Url { get; set; }
        public string IdentificacionRemitente { get; set; }
        public string NombreRemitente { get; set; }
        public string CorreoAEnviar { get; set; }
        public string NombreAplicacion { get; set; }
        public string NombreEmpresa { get; set; }
    }
}
