﻿using App.Core.UseCase.Contracts;
using App.Infraestructura.System.NumLet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App.Infraestructura.System
{
    public class ManejoCadena : IUtilidadCadena
    {
        public string Decode(string cadena)
        {
            return HttpUtility.HtmlDecode(cadena);
        }

        public string Encode(string cadena)
        {
            return HttpUtility.HtmlEncode(cadena);
        }

        public string NumeroALetra(decimal numero)
        {
            return Numalet.ToCardinal(numero);
        }
    }
}
