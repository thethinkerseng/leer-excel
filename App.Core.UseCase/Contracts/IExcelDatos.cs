﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Contracts
{
    public interface IExcelDatos
    {
        DataTable GetDataExcel(string fileName);
        List<DataTable> GetListDataExcel(string fileName);

        DataTable GetDataExcel(byte[] archivo, string extension);
        List<DataTable> GetListDataExcel(byte[] archivo, string extension);
    }
}
