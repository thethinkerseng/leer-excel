﻿using App.Core.Entities.Excel;
using App.Core.UseCase.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Contracts
{
    public interface ILeerExcel
    {
        List<IFila> LeerArchivoExcel(string fileName);
        List<IFila> LeerArchivoExcel(byte[] archivo, string extension);
        ConsultaRegistroDto ArmarTabla(List<DetallesColumna> _items);
    }
    public interface IColumna
    {
        string Clave { get; set; }
        string Valor { get; set; }
        string Tipo { get; set; }
    }
    public interface IFila
    {
        int Posicion { get; set; }
        List<IColumna> Columnas { get; set; } 
    }
}
