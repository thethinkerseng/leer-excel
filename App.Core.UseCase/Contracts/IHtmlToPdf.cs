﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Contracts
{
    public interface IHtmlToPdf
    {
        byte[] GenerarPdf(string html);
    }
}
