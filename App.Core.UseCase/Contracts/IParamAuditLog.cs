﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Contracts
{
    public interface IParamAuditLog
    {
        DateTime? FechaInicial { get; set; }
        DateTime? FechaFinal { get; set; }
        string Tabla { get; set; }
        string Accion { get; set; }
        string Modulo { get; set; }
        string Evento { get; set; }
    }
}
