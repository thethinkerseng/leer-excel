﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using MediatR;
using App.Core.Entities.Base;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using App.Core.UseCase.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    //Commando
    public class EstadoTerceroRequest : IRequest<EstadoTerceroResponse>
    {
        public int Id { get; set; }
        public string Estado { get; set; }
    }
    //Response
    public class EstadoTerceroResponse
    {
        public ValidationResult ValidationResult { get; }
        public int Id { get; set; }
        public string Mensaje { get; private set; }

        public EstadoTerceroResponse(ValidationResult validationResult, int? _Id = null)
        {
            Id = _Id ?? 0;
            if (Id == 0)
                Mensaje = "Operación realizada satisfactoriamente";
            else
                Mensaje = "El estado no pudo ser actualizado";
            ValidationResult = validationResult;
        }
    }

    public class EstadoTerceroInteractor : IRequestHandler<EstadoTerceroRequest, EstadoTerceroResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITerceroRepository _repository;
        private readonly IValidator<EstadoTerceroRequest> _validator;
        private readonly ILogger _log;
        private readonly IMapper _mapper;
        public EstadoTerceroInteractor(IValidator<EstadoTerceroRequest> validator, IUnitOfWork unitOfWork, ITerceroRepository repository, ILogger log, IMapper mapper)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
            _log = log;
            _mapper = mapper;
        }

        public EstadoTerceroResponse Handle(EstadoTerceroRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new EstadoTerceroResponse(validationResult);
            var entity = _repository.FindBy(x => x.Id == message.Id).FirstOrDefault();
            _mapper.Map<EstadoTerceroRequest, Tercero>(message, entity);
            entity.Estado = message.Estado;
            _repository.Edit(entity);
            _unitOfWork.Commit(TypeAction.Estado.Value, Module.DatosBasicos.Value);
            _log.Info("Se actualizó el tercero");
            return new EstadoTerceroResponse(validationResult, entity.Id);
        }
    }

    public class EstadoTerceroValidator : AbstractValidator<EstadoTerceroRequest>
    {
        public EstadoTerceroValidator(ITerceroRepository repository)
        {
            RuleFor(r => repository.FindBy(t =>t.Id == r.Id).FirstOrDefault()).
                NotNull().WithMessage("El tercero no esta registrdao en el sistema");
        }
    }
   
}