﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    public class ConsultarTodosLosTercerosRequest : IRequest<ConsultarTodosLosTercerosResponse>
    {
    }
    public class ConsultarTodosLosTercerosResponse
    {
        public ValidationResult ValidationResult { get; }
        public List<TerceroResponse> TercerosResponse { get; }

        public class TerceroResponse
        {
            public int Id { get; set; }
            public string TipoIdentidad { get; set; }
            public string Identificacion { get; set; }
            public string DigitoVerificacion { get; set; }
            public string PrimerNombre { get; set; }
            public string SegundoNombre { get; set; }
            public string PrimerApellido { get; set; }
            public string SegundoApellido { get; set; }
            public string NombreContacto { get; set; }
            public string RazonSocial { get; set; }
            public string Email { get; set; }
            public string NombreCompleto { get { return TipoIdentidad == "NIT" ? RazonSocial : string.Join(" ", PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido); } }
            public string Estado { get; set; }
            public string Telefono { get; set; }
        }

        public ConsultarTodosLosTercerosResponse(ValidationResult validationResult, List<Tercero> terceros, IMapper mapper)
        {
            ValidationResult = validationResult;
            this.TercerosResponse = new List<TerceroResponse>();
            mapper.Map<Tercero, TerceroResponse>(terceros, this.TercerosResponse);
        }
        public ConsultarTodosLosTercerosResponse(ValidationResult validationResult)
        {
            ValidationResult = validationResult;
        }
    }
    public class ConsultarTodosLosTercerosInteractor: IRequestHandler<ConsultarTodosLosTercerosRequest, ConsultarTodosLosTercerosResponse>
    {        
        private readonly ITerceroRepository _repository;
        private readonly IValidator<ConsultarTodosLosTercerosRequest> _validator;
        private readonly IMapper _mapper;
        public ConsultarTodosLosTercerosInteractor(IValidator<ConsultarTodosLosTercerosRequest> validator, ITerceroRepository repository, IMapper mapper)
        {
            _validator = validator;
            _repository = repository;
            _mapper = mapper;
        }
        public ConsultarTodosLosTercerosResponse Handle(ConsultarTodosLosTercerosRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new ConsultarTodosLosTercerosResponse(validationResult);

            List<Tercero> terceros = _repository.GetAll().ToList();
            return new ConsultarTodosLosTercerosResponse(validationResult, terceros, _mapper);
        }
    }
    public class ConsultarTodosLosTercerosRequestValidator : AbstractValidator<ConsultarTodosLosTercerosRequest>
    {
        public ConsultarTodosLosTercerosRequestValidator()
        {
        }
    }
}
