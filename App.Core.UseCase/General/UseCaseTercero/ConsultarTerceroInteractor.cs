﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    public class ConsultarTerceroRequest : IRequest<ConsultarTerceroResponse>
    {
        public string Identificacion { get; set; }
    }

    public class ConsultarTerceroResponse
    {
        public ValidationResult ValidationResult { get; }

        public int Id { get; set; }
        public string TipoIdentidad { get; set; }
        public string Identificacion { get; set; }
        public string DigitoVerificacion { get; set; }
        public DateTime? FechaExpedicion { get; set; }
        public string TipoPersona { get; set; }
        public string Tipo { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NombreContacto { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }
        public string Movil1 { get; set; }
        public string Movil2 { get; set; }
        public string Movil3 { get; set; }
        public string Direccion { get; set; }
        public string Pais { get; set; }
        public string Departamento { get; set; }
        public string Ciudad { get; set; }
        public string Email { get; set; }
        public string Regimen { get; set; }
        public string CodDane { get; set; }
        public string Imagen { get; set; }
        public string Estado { get; set; }
        public string NombreCompleto { get { return TipoIdentidad == "NIT" ? RazonSocial : string.Join(" ", PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido); } }

        public ConsultarTerceroResponse(ValidationResult validationResult, Entities.General.Tercero entidad, IMapper mapper)
        {
            ValidationResult = validationResult;

            mapper.Map<Entities.General.Tercero, ConsultarTerceroResponse>(entidad, this);
        }
        public ConsultarTerceroResponse(ValidationResult validationResult)
        {
            ValidationResult = validationResult;
        }
    }

    public class ConsultarTerceroInteractor : IRequestHandler<ConsultarTerceroRequest, ConsultarTerceroResponse>
    {
        private readonly ITerceroRepository _repository;
        private readonly IValidator<ConsultarTerceroRequest> _validator;
        private readonly IMapper _mapper;
        public ConsultarTerceroInteractor(IValidator<ConsultarTerceroRequest> validator, ITerceroRepository repository, IMapper mapper)
        {
            _validator = validator;
            _repository = repository;
            _mapper = mapper;
        }
        public ConsultarTerceroResponse Handle(ConsultarTerceroRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new ConsultarTerceroResponse(validationResult);

            Tercero entidad = _repository.FindBy(t => t.Identificacion == message.Identificacion).FirstOrDefault();
            return new ConsultarTerceroResponse(validationResult, entidad, _mapper);
        }
    }

    public class ConsultarTerceroRequestValidator : AbstractValidator<ConsultarTerceroRequest>
    {
        public ConsultarTerceroRequestValidator(ITerceroRepository repository)
        {
            RuleFor(r => r.Identificacion).NotEmpty().WithMessage("Debe especificar una identificación");
            RuleFor(r => repository.FindBy(t => t.Identificacion == r.Identificacion).FirstOrDefault()).
                NotNull().WithMessage("La identificación del tercero no esta registrado en el sistema");
        }
    }
}