﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using MediatR;
using App.Core.Entities.Base;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using App.Core.UseCase.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    //Commando
    public class AgregarTerceroRequest : IRequest<AgregarTerceroResponse>
    {
        public string TipoIdentidad { get; set; }
        public string Identificacion { get; set; }
        public string DigitoVerificacion { get; set; }
        public DateTime? FechaExpedicion { get; set; }
        public string TipoPersona { get; set; }
        public string Tipo { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NombreContacto { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }
        public string Movil1 { get; set; }
        public string Movil2 { get; set; }
        public string Movil3 { get; set; }
        public string Direccion { get; set; }
        public string Pais { get; set; }
        public string Departamento { get; set; }
        public string Ciudad { get; set; }
        public string Email { get; set; }
        public string Regimen { get; set; }
        public string CodDane { get; set; }
        public string Imagen { get; set; }
        public string Estado { get; set; }
    }
    //Response
    public class AgregarTerceroResponse
    {
        public ValidationResult ValidationResult { get; }
        public int Id { get; set; }
        public string Mensaje { get; private set; }

        public AgregarTerceroResponse(ValidationResult validationResult, int? _Id = null)
        {
            Id = _Id ?? 0;
            if (Id == 0)
                Mensaje = "Operación realizada satisfactoriamente";
            else
                Mensaje = "El tercero no fue registrado";
            ValidationResult = validationResult;
        }
    }

    public class AgregarTerceroInteractor : IRequestHandler<AgregarTerceroRequest, AgregarTerceroResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITerceroRepository _repository;
        private readonly IValidator<AgregarTerceroRequest> _validator;
        private readonly ILogger _log;
        private readonly IMapper _mapper;
        public AgregarTerceroInteractor(IValidator<AgregarTerceroRequest> validator, IUnitOfWork unitOfWork, ITerceroRepository repository, ILogger log, IMapper mapper)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
            _log = log;
            _mapper = mapper;
        }

        public AgregarTerceroResponse Handle(AgregarTerceroRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid)return new AgregarTerceroResponse(validationResult);
            var entity = new Tercero();
            _mapper.Map<AgregarTerceroRequest, Tercero>(message, entity);
            entity.Estado = "AC";
            _repository.Add(entity);
            _unitOfWork.Commit(TypeAction.Crear.Value, Module.DatosBasicos.Value);
            _log.Info("Se guardó el tercero");
            return new AgregarTerceroResponse(validationResult, entity.Id);
        }
    }

    public class AgregarTerceroValidator : AbstractValidator<AgregarTerceroRequest>
    {
        public AgregarTerceroValidator(ITerceroRepository repository)
        {
            //Por Ejemplo
            RuleFor(r => TipoIdentificacion.Is(r.TipoIdentidad)).Equal(true).WithMessage("Tipo identificación del tercero no es válido");

            RuleFor(r => (r.TipoIdentidad == TipoIdentificacion.Nit.Value && !string.IsNullOrEmpty(r.RazonSocial))|| r.TipoIdentidad != TipoIdentificacion.Nit.Value).Equal(true).WithMessage("La rzón social no puede estar vacio");

            RuleFor(r => (r.TipoIdentidad != TipoIdentificacion.Nit.Value && !string.IsNullOrEmpty(r.PrimerNombre))|| r.TipoIdentidad == TipoIdentificacion.Nit.Value).Equal(true).WithMessage("El primer nombre no puede estar Vacio");
            RuleFor(r => (r.TipoIdentidad != TipoIdentificacion.Nit.Value && !string.IsNullOrEmpty(r.PrimerApellido))|| r.TipoIdentidad == TipoIdentificacion.Nit.Value).Equal(true).WithMessage("El primer apellido no puede estar Vacio");


            RuleFor(r => r.Email).EmailAddress().WithName("El correo electronico del tercero no es válido");
            RuleFor(r => repository.FindBy(t => t.Identificacion == r.Identificacion).FirstOrDefault()).
                Null().WithMessage("La Identificación del Tercero ya esta registrada");
        }
    }
   
}