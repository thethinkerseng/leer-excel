﻿using MediatR;
using App.Core.Entities.Base;
using System.Collections.Generic;
using System.Linq;

namespace App.Core.UseCase.Auditoria
{
    public class ConfiguracionAuditoriaInteractor : IRequestHandler<ConfiguracionAuditoriaRequest, ConfiguracionAuditoriaResponse>
    {
        private readonly IAuditLogRepository _repositorio;

        public ConfiguracionAuditoriaInteractor(IAuditLogRepository repositorio)
        {
            _repositorio = repositorio;
        }
        public ConfiguracionAuditoriaResponse Handle(ConfiguracionAuditoriaRequest message)
        {
            List<string> tableName = _repositorio.GetNameTable();
            return new ConfiguracionAuditoriaResponse(tableName);
        }
    }
    public class ConfiguracionAuditoriaRequest : IRequest<ConfiguracionAuditoriaResponse>
    {

    }
    public class ConfiguracionAuditoriaResponse
    {
        public List<string> TableName { get; }
        public List<EnumerationObject> Modulos { get; }
        public List<EnumerationObject> Acciones { get; }

        public ConfiguracionAuditoriaResponse(List<string> _tableName)
        {
            Acciones = TypeAction.GetAll().Select(x => new EnumerationObject() { DisplayName = x.DisplayName, Value = x.Value }).ToList();
            Modulos = Module.GetAll().Select(x => new EnumerationObject() { DisplayName = x.DisplayName, Value = x.Value }).ToList();
            TableName = _tableName;
        }
    }
}
