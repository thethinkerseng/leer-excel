﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Auditoria
{
    public class EnumerationObject
    {
        public string DisplayName { get; set; }
        public string Value { get; set; }
    }
}
