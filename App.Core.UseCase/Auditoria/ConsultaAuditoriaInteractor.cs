﻿using App.Core.Entities.Base;
using App.Core.UseCase.Contracts;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Core.UseCase.Auditoria
{
    public class ConsultaAuditoriaInteractor : IRequestHandler<ConsultaAuditoriaRequest, ConsultaAuditoriaResponse>
    {
        private readonly IMapper _mapper;
        private readonly ITerceroRepository _tercero;
        private readonly IAuditLogRepository _auditoria;
        private readonly IValidator<ConsultaAuditoriaRequest> _validator;

        public ConsultaAuditoriaInteractor(
            IValidator<ConsultaAuditoriaRequest> validator, 
            IMapper mapper, 
            ITerceroRepository tercero,
            IAuditLogRepository auditoria)
        {
            _tercero = tercero;
            _mapper = mapper;
            _auditoria = auditoria;
            _validator = validator;
        }
        public ConsultaAuditoriaResponse Handle(ConsultaAuditoriaRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new ConsultaAuditoriaResponse(validationResult);
            List<AuditLog> resultados = _auditoria.FindBy(message);
            List<AuditoriaResponse> _resultados = new List<AuditoriaResponse>();
            _mapper.Map(resultados, _resultados);
            _resultados.ForEach(x => x.NombreUsuario = _tercero.FindBy(y => y.Identificacion == x.UserID).FirstOrDefault()?.NombreCompleto ?? x.UserID);
            return new ConsultaAuditoriaResponse(validationResult, _resultados);
        }
    }
    public class ConsultaAuditoriaRequest : IRequest<ConsultaAuditoriaResponse>, IParamAuditLog
    {
        public DateTime? FechaInicial { get; set; }
        public DateTime? FechaFinal { get; set; }
        public string Tabla { get; set; }
        public string Accion { get; set; }
        public string Modulo { get; set; }
        public string Evento { get; set; }
    }
    public class ConsultaAuditoriaResponse
    {
        public ValidationResult ValidationResult { get; }
        public List<AuditoriaResponse> Encontrados { get; set; }
        public ConsultaAuditoriaResponse(ValidationResult validationResult,List<AuditoriaResponse> resultados = null)
        {
            ValidationResult = validationResult;
            Encontrados = resultados;
        }
    }
    public class AuditoriaResponse
    {
        public string UserID { get; set; }
        public string NombreUsuario { get; set; }
        public DateTime EventDateUTC { get; set; }
        public DateTime EventDateLocalTime { get; set; }
        public string EventType { get; set; }
        public string Evento
        { get
            {
                return Entities.Base.EventType.GetDisplayValue(EventType);
            }
        }
        public string TableName { get; set; }
        public string RecordID { get; set; }
        public string ColumnName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public string Action { get; set; }
        public string Accion
        {
            get
            {
                return TypeAction.GetDisplayValue(Action);
            }
        }
        public string Module { get; set; }
        public string Modulo
        {
            get
            {
                return Entities.Base.Module.GetDisplayValue(Action);
            }
        }
    }
    public class ConsultaAuditoriaValidator : AbstractValidator<ConsultaAuditoriaRequest>
    {

        public ConsultaAuditoriaValidator()
        {
            RuleFor(r => ValidarSiExisteAlMenosUnParametroAConsultar(r)).Equal(true).WithName("Parametros").WithMessage("Debe especificar al menos un parametro para la consulta");
            RuleFor(r => r.FechaFinal.HasValue && r.FechaInicial.HasValue).Equal(true).WithMessage("debe especificar la fecha inicial y la fecha final").When(t => t != null).When(x => x.FechaFinal.HasValue || x.FechaInicial.HasValue);
        }

        public bool ValidarFecha(DateTime? fecha)
        {
            if (!fecha.HasValue) return false;
            return fecha.Value.Year > 1;
        }
        public bool ValidarSiExisteAlMenosUnParametroAConsultar(IParamAuditLog parametro)
        {
            if (string.IsNullOrEmpty(parametro.Accion) &&
                string.IsNullOrEmpty(parametro.Modulo) &&
                string.IsNullOrEmpty(parametro.Tabla) &&
                !ValidarFecha(parametro.FechaInicial) &&
                !ValidarFecha(parametro.FechaFinal))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

}
