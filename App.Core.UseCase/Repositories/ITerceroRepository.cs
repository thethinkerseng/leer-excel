﻿using App.Core.Entities.General;
using App.Core.UseCase.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    public interface ITerceroRepository : IGenericRepository<Tercero>
    {
        
    }
}