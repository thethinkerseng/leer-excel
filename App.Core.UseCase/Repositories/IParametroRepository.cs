﻿using App.Core.Entities.Base;
using App.Core.UseCase.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    public interface IParametroRepository : IGenericRepository<Parametro>
    {
      
    }
}
 