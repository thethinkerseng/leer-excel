﻿using App.Core.Entities.Base;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.UseCase.Contracts;

namespace App.Core.UseCase
{
    public interface IAuditLogRepository : IGenericRepositoryNoAudit<AuditLog>
    {
        List<AuditLog> FindBy(IParamAuditLog param);
        List<string> GetNameTable();
    }
}