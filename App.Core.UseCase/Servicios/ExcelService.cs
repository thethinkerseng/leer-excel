﻿using App.Core.Entities.Excel;
using App.Core.UseCase.Contracts;
using App.Core.UseCase.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Servicios
{
    public class ExcelService : ILeerExcel
    {
        private readonly IExcelDatos _excel;
        public ExcelService(IExcelDatos excel)
        {
            _excel = excel;
        }
        List<IFila> ILeerExcel.LeerArchivoExcel(string fileName)
        {
            DataTable tabla = _excel.GetDataExcel(fileName);
            return new List<IFila>();
        }

        List<IFila> ILeerExcel.LeerArchivoExcel(byte[] archivo, string extension)
        {
            DataTable tabla = _excel.GetDataExcel(archivo, extension);
            List<IFila> resultado = new List<IFila>();
            foreach (DataRow fila in tabla.Rows)
            {
                DetallesDto _fila = new DetallesDto();
                _fila.Columnas = new List<IColumna>();
                foreach (DataColumn column in tabla.Columns)
                {
                    if (!string.IsNullOrEmpty(column.ColumnName))
                    {
                        AgregarItem(fila, _fila, column);
                    }

                }
                resultado.Add(_fila);
            }
            return resultado;
        }

        private void AgregarItem(DataRow fila, DetallesDto _fila, DataColumn column)
        {
            fila[column] = fila[column].ToString().Replace('"', ' ').Trim();
            fila[column] = fila[column].ToString().Replace(@"\", @" ").Trim();
            _fila.Columnas.Add(new ItemDto()
            {
                Clave = column.ColumnName,
                Tipo = TipoCampo.ObtenerTipo(column.DataType),
                Valor = fila[column].ToString(),
            });
        }

        public ConsultaRegistroDto ArmarTabla(List<DetallesColumna> _items)
        {
            ConsultaRegistroDto resultado = new ConsultaRegistroDto();
            List<IGrouping<string, DetallesColumna>> _columnas = _items.GroupBy(x => x.Clave.ToUpper()).ToList();
            resultado.Encabezado = _columnas.Select(x => x.Key).ToArray();
            int Columnas = resultado.Encabezado.Length;
            List<IGrouping<int, DetallesColumna>> _filas = _items.GroupBy(x => x.RegistroId).ToList();
            resultado.Registros = new List<string[]>();
            foreach (var fil in _filas)
            {
                string[] fila = new string[Columnas];
                for (int i = 0; i < resultado.Encabezado.Length; i++)
                {
                    string valor = string.Empty;
                    string key = resultado.Encabezado[i];
                    DetallesColumna result = fil.Where(x => x.Clave.ToUpper() == key.ToUpper()).FirstOrDefault();
                    if (result != null) valor = result.Valor;
                    fila[i] = valor;
                }
                resultado.Registros.Add(fila);
            }
            return resultado;
        }
    }
}
