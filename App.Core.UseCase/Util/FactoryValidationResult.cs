﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Util
{
    internal class FactoryValidationResult
    {
        protected FactoryValidationResult()
        {
        }
        internal static ValidationResult Create(string propertyName, string error) {
            return new ValidationResult(new List<ValidationFailure>() {
                     new ValidationFailure(propertyName,error)
                });
        }
        
    }
}
