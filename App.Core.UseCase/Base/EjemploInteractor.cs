﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.UseCase;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Base
{
    public class EjemploInteractor : IRequestHandler<EjemploRequest, EjemploResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITerceroRepository _repository;
        private readonly IValidator<EjemploRequest> _validator;
        private readonly ILogger _log;
        private readonly IMapper _mapper;

        public EjemploInteractor(IValidator<EjemploRequest> validator, IUnitOfWork unitOfWork, ITerceroRepository repository, ILogger log, IMapper mapper)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
            _log = log;
            _mapper = mapper;
        }

        public EjemploResponse Handle(EjemploRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new EjemploResponse(validationResult);
            _log.Info("ejemplo");

            return new EjemploResponse(validationResult);
        }
    }

    public class EjemploRequest : IRequest<EjemploResponse>
    {

    }
    
    public class EjemploResponse
    {
        public ValidationResult ValidationResult { get; }

        public int Id { get; set; }

        public EjemploResponse(ValidationResult validationResult, int? _Id = null)
        {
            Id = (_Id.HasValue ? _Id.Value : 0);
            ValidationResult = validationResult;
        }
    }

    public class EjemploRequestValidator : AbstractValidator<EjemploRequest>
    {
        public EjemploRequestValidator()
        {

        }
    }

}
