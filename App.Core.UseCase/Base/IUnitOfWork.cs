﻿using App.Core.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Core.UseCase.Base
{
    public interface IUnitOfWork : IDisposable
    {
        ISistema Sistema { get; }
        /// <summary>
        /// Saves all pending changes
        /// </summary>
        /// <returns>The number of objects in an Added, Modified, or Deleted state</returns>
        int Commit(string Accion, string Modulo);
    }
}
