﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using MediatR;
using App.Core.Entities.Base;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using App.Core.UseCase.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;

namespace App.Core.UseCase
{
    //Commando
    public class EliminarExcelRequest : IRequest<EliminarExcelResponse>
    {

        public int Id { get; set; }
        
    }
    //Response
    public class EliminarExcelResponse
    {
        public ValidationResult ValidationResult { get; }
        public int Id { get; set; }
        public string Mensaje { get; private set; }
        public EliminarExcelResponse(ValidationResult validationResult, int id, string mensaje = "Excel no fue cargado")
        {
            Id = id;
            Mensaje = mensaje;
            ValidationResult = validationResult;
        }
    }

    public class EliminarExcelInteractor : IRequestHandler<EliminarExcelRequest, EliminarExcelResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IExcelRepository _repository;
        private readonly IValidator<EliminarExcelRequest> _validator;
        public EliminarExcelInteractor(IValidator<EliminarExcelRequest> validator, IUnitOfWork unitOfWork, IExcelRepository repository)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;

        }

        public EliminarExcelResponse Handle(EliminarExcelRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new EliminarExcelResponse(validationResult, message.Id);
            var entity = _repository.FindBy(x => x.Id == message.Id).FirstOrDefault();
            _repository.Delete(entity);
            _unitOfWork.Commit(TypeAction.Eliminar.Value, Module.Importar.Value);
            return new EliminarExcelResponse(validationResult, message.Id, "Operación realizada satisfactoriamente");
        }
    }

    public class EliminarExcelValidator : AbstractValidator<EliminarExcelRequest>
    {
        public EliminarExcelValidator(IExcelRepository repository)
        {

            ///RuleFor(r => repository.FindBy(x => x.NumeroActa == r.NumeroActa && x.Id != r.Id).FirstOrDefault()).Null().WithMessage("El número de acta ya esta registrado en el sistema");

            RuleFor(r => repository.FindBy(t =>t.Id == r.Id).FirstOrDefault()).
                NotNull().WithMessage("Excel no esta registrdao en el sistema");


        }
    }
   
}