﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;

namespace App.Core.UseCase
{
    public class FiltrarExcelRequest : IRequest<FiltrarExcelResponse>
    {
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
    }

    public class FiltrarExcelResponse
    {
        public List<ArchivoConsulta> Actas { get; }
        public FiltrarExcelResponse( List<ArchivoConsulta> actas=null)
        {
            Actas = actas ?? new List<ArchivoConsulta>();
        }
    }

    public class FiltrarExcelInteractor : IRequestHandler<FiltrarExcelRequest, FiltrarExcelResponse>
    {
        private readonly IExcelRepository _repository;
        private readonly IMapper _mapper;
        public FiltrarExcelInteractor(IExcelRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public FiltrarExcelResponse Handle(FiltrarExcelRequest message)
        {
            if (message.FechaInicio.HasValue && message.FechaFinal.HasValue)
            {
                DateTime inicio = new DateTime(message.FechaInicio.Value.Year, message.FechaInicio.Value.Month, message.FechaInicio.Value.Day, 0, 0, 0);
                DateTime final = new DateTime(message.FechaFinal.Value.Year, message.FechaFinal.Value.Month, message.FechaFinal.Value.Day, 23, 59, 59);
                List<Archivo> _actas = _repository.FindBy(x => x.Fecha >= inicio && x.Fecha<= final).ToList();
                List<ArchivoConsulta> actas = new List<ArchivoConsulta>();
                _mapper.Map(_actas, actas);
                return new FiltrarExcelResponse(actas);
            }
            return new FiltrarExcelResponse();
        }
        
    }
    
}