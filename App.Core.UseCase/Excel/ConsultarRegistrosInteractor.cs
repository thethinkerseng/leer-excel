﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;
using App.Core.UseCase.Excel;

namespace App.Core.UseCase
{
    public class ConsultarRegistrosRequest : IRequest<ConsultarRegistrosResponse>
    {
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
    }

    public class ConsultarRegistrosResponse
    {
        public ConsultaRegistroDto Resultado { get; }
        public ConsultarRegistrosResponse(ConsultaRegistroDto resultado = null)
        {
            Resultado = resultado ?? new ConsultaRegistroDto();
        }
    }

    public class ConsultarRegistrosInteractor : IRequestHandler<ConsultarRegistrosRequest, ConsultarRegistrosResponse>
    {
        private readonly IRegistrosItemsRepository _repository;
        private readonly ILeerExcel _servicio;
        private readonly IMapper _mapper;
        public ConsultarRegistrosInteractor(IRegistrosItemsRepository repository, ILeerExcel servicio, IMapper mapper)
        {
            _repository = repository;
            _servicio = servicio;
            _mapper = mapper;
        }
        public ConsultarRegistrosResponse Handle(ConsultarRegistrosRequest message)
        {
            List<DetallesColumna> _items = null;
            if (message.FechaInicio.HasValue && message.FechaFinal.HasValue)
            {
                DateTime inicio = new DateTime(message.FechaInicio.Value.Year, message.FechaInicio.Value.Month, message.FechaInicio.Value.Day, 0, 0, 0);
                DateTime final = new DateTime(message.FechaFinal.Value.Year, message.FechaFinal.Value.Month, message.FechaFinal.Value.Day, 23, 59, 59);
                _items = _repository.FindBy(x => x.Registro.Excel.Fecha >= inicio && x.Registro.Excel.Fecha <= final).ToList();
                if (!_items.Any()) return new ConsultarRegistrosResponse();
                ConsultaRegistroDto resultado = _servicio.ArmarTabla(_items);
                return new ConsultarRegistrosResponse(resultado);
            }
            return new ConsultarRegistrosResponse();
        }
    }
}