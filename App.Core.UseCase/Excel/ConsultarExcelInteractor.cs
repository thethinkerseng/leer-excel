﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;

namespace App.Core.UseCase
{
    public class ConsultarExcelRequest : IRequest<ConsultarExcelResponse>
    {
        public string Identificacion { get; set; }
    }

    public class ConsultarExcelResponse
    {
        public List<ArchivoConsulta> Resultados { get; }
        public ConsultarExcelResponse( List<ArchivoConsulta> resultados=null)
        {
            Resultados = resultados ?? new List<ArchivoConsulta>();
        }
    }

    public class ConsultarExcelInteractor : IRequestHandler<ConsultarExcelRequest, ConsultarExcelResponse>
    {
        private readonly IExcelRepository _repository;
        private readonly IMapper _mapper;
        public ConsultarExcelInteractor(IExcelRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public ConsultarExcelResponse Handle(ConsultarExcelRequest message)
        {
            List<Archivo> _resultado = _repository.FindBy().ToList();
            List<ArchivoConsulta> resultado = new List<ArchivoConsulta>();
            _mapper.Map(_resultado, resultado);
            return new ConsultarExcelResponse(resultado);
        }
        
    }
    public class ArchivoConsulta
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan Hora { get; set; }
        public string ArchivoNombre { get; set; }
        public string ArchivoExtension { get; set; }
        public string ArchivoTipo { get; set; }
        public int ArchivoLong { get; set; }
        public string Estado { get; set; }
    }
    
}