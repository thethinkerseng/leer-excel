﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;
using App.Core.UseCase.Excel;

namespace App.Core.UseCase
{
    public class ConsultarRegistrosExcelRequest : IRequest<ConsultarRegistrosExcelResponse>
    {
        public int Id { get; set; }
    }

    public class ConsultarRegistrosExcelResponse
    {
        public ConsultaRegistroDto Resultado { get; }
        public ConsultarRegistrosExcelResponse(ConsultaRegistroDto resultado =null)
        {
            Resultado = resultado ?? new ConsultaRegistroDto();
        }
    }

    public class ConsultarRegistrosExcelInteractor : IRequestHandler<ConsultarRegistrosExcelRequest, ConsultarRegistrosExcelResponse>
    {
        private readonly IRegistrosItemsRepository _repository;
        private readonly ILeerExcel _servicio;
        private readonly IMapper _mapper;
        public ConsultarRegistrosExcelInteractor(IRegistrosItemsRepository repository, ILeerExcel servicio, IMapper mapper)
        {
            _repository = repository;
            _servicio = servicio;
            _mapper = mapper;
        }
        public ConsultarRegistrosExcelResponse Handle(ConsultarRegistrosExcelRequest message)
        {
            List<DetallesColumna> _items = _repository.FindBy(x => x.Registro.ExcelId == message.Id).ToList();
            if (!_items.Any()) return new ConsultarRegistrosExcelResponse();
            ConsultaRegistroDto resultado = _servicio.ArmarTabla(_items);
            return new ConsultarRegistrosExcelResponse(resultado);
        }
    }
}