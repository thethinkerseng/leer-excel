﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase.Excel
{
    public class RegistroDto
    {
        public List<string> columnas { get; set; }
    }
    
    public class ConsultaRegistroDto
    {
        public string[] Encabezado { get; set; }
        public List<string[]> Registros { get; set; }
    }
}
