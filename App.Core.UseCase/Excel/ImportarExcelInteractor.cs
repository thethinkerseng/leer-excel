﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using MediatR;
using App.Core.Entities.Base;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using App.Core.UseCase.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;
//using App.Core.UseCase.Repositories;

namespace App.Core.UseCase
{
    //Commando
    public class ImportarExcelRequest : IRequest<ImportarExcelResponse>
    {
        public int Id { get; set; }

        public DateTime Fecha { get; set; }
        public TimeSpan Hora { get; set; }
        public byte[] ArchivoBytes { get; set; }
        public string ArchivoNombre { get; set; }
        public string ArchivoExtension { get; set; }
        public string ArchivoTipo { get; set; }
        public int ArchivoLong { get; set; }

        public string Estado { get; set; }

        public List<IFila> Productos { get; set; }
    }
    
    //Response
    public class ImportarExcelResponse
    {
        public ValidationResult ValidationResult { get; }
        public int Id { get; set; }
        public string Mensaje { get; private set; }

        public ImportarExcelResponse(ValidationResult validationResult, int? id = null, string mensaje = "Acta no pudo ser registrada")
        {
            Id = id ?? 0;
            Mensaje = mensaje;
            ValidationResult = validationResult;
        }
    }

    public class ImportarExcelInteractor : IRequestHandler<ImportarExcelRequest, ImportarExcelResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILeerExcel _servicioExcel;
        private readonly IExcelRepository _repository;
        private readonly IValidator<ImportarExcelRequest> _validator;
        private readonly IMapper _mapper;
        public ImportarExcelInteractor(IValidator<ImportarExcelRequest> validator, IUnitOfWork unitOfWork, IExcelRepository repository, IMapper mapper, ILeerExcel servicio)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _servicioExcel = servicio;
        }

        public ImportarExcelResponse Handle(ImportarExcelRequest message)
        {
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new ImportarExcelResponse(validationResult);

            message.Productos = _servicioExcel.LeerArchivoExcel(message.ArchivoBytes, message.ArchivoExtension);

            var entity = new Archivo();
            _mapper.Map<ImportarExcelRequest, Archivo>(message, entity);
            entity.Estado = EstadosGeneral.Activo.Value;
            entity.Hora = message.Hora;
            entity.Fecha = message.Fecha;
            entity.Registros = MappearProductos(entity, message);

            _repository.Add(entity);
            _unitOfWork.Commit(TypeAction.Crear.Value, Module.Importar.Value);
            return new ImportarExcelResponse(validationResult, entity.Id, "Operación realizada satisfactoriamente");
        }

        private List<DetallesRegistro> MappearProductos(Archivo entity, ImportarExcelRequest message)
        {
            List<DetallesRegistro> productos = new List<DetallesRegistro>();
            foreach(IFila item in message.Productos)
            {
                DetallesRegistro registro = new DetallesRegistro();
                registro.Items = new List<DetallesColumna>();
                foreach(IColumna col in item.Columnas)
                {
                    registro.Items.Add(new DetallesColumna() {
                        Clave = col.Clave,
                        Tipo  = col.Tipo,
                        Valor = col.Valor,
                        //Excel = entity,
                    });
                }
                productos.Add(registro);
            }
            return productos;
        }
    }
    

    public class AgregarExcelValidator : AbstractValidator<ImportarExcelRequest>
    {
        public AgregarExcelValidator(IExcelRepository _repository)
        {
            //RuleFor(r => _repository.FindBy(x => x.NumeroActa == r.NumeroActa).FirstOrDefault()).Null().WithMessage("El número de acta ya esta registrado en el sistema");            
            //RuleFor(r => (!r.Borrador && !r.Productos.Any(p => p.ValorUnitario == 0)) || r.Borrador).Equal(true).When(r => r.Productos != null).WithMessage("El campo valor unitario en la lista productos es requerido");
        }
    }
}