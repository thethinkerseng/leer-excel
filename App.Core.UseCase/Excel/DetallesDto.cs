﻿using App.Core.UseCase.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.UseCase
{
    public class DetallesDto:IFila
    {
        public DetallesDto()
        {
        }
        public int Posicion { get; set; }
        public List<IColumna> Columnas { get; set; }
        public int ArchivoId { get; set; }
    }
    public class ItemDto:IColumna
    {
        public ItemDto()
        {
        }
        public string Clave { get; set; }
        public string Valor { get; set; }
        public string Tipo { get; set; }
    }
}
