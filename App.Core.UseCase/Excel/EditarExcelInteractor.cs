﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using MediatR;
using App.Core.Entities.Base;
using App.Core.Entities.Contracts;
using App.Core.Entities.General;
using App.Core.UseCase.Base;
using App.Core.UseCase.Contracts;
using App.Core.UseCase.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Excel;

namespace App.Core.UseCase
{
    //Commando
    public class EditarExcelRequest : IRequest<EditarExcelResponse>
    {

        public int Id { get; set; }

        public DateTime Fecha { get; set; }
        public TimeSpan Hora { get; set; }

        public byte[] ArchivoBytes { get; set; }
        public string ArchivoNombre { get; set; }
        public string ArchivoExtension { get; set; }
        public string ArchivoTipo { get; set; }
        public int ArchivoLong { get; set; }
        public string Estado { get; set; }

        public List<IFila> Productos { get; set; }
    }
    //Response
    public class EditarExcelResponse
    {
        public ValidationResult ValidationResult { get; }
        public int Id { get; set; }
        public string Mensaje { get; private set; }
        public EditarExcelResponse(ValidationResult validationResult, int id, string mensaje = "Excel no fue cargado")
        {
            Id = id;
            Mensaje = mensaje;
            ValidationResult = validationResult;
        }
    }

    public class EditarExcelInteractor : IRequestHandler<EditarExcelRequest, EditarExcelResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IExcelRepository _repository;
        private readonly IValidator<EditarExcelRequest> _validator;
        private readonly IMapper _mapper;
        private readonly ILeerExcel _servicio;
        public EditarExcelInteractor(IValidator<EditarExcelRequest> validator, IUnitOfWork unitOfWork, IExcelRepository repository, IMapper mapper, ILeerExcel servicio)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _servicio = servicio;
        }

        public EditarExcelResponse Handle(EditarExcelRequest message)
        {
            message.Productos = _servicio.LeerArchivoExcel(message.ArchivoBytes,message.ArchivoExtension);
            var validationResult = _validator.Validate(message);
            if (!validationResult.IsValid) return new EditarExcelResponse(validationResult, message.Id);
            var entity = _repository.FindBy(x => x.Id == message.Id).FirstOrDefault();
            
            List<DetallesRegistro> Productos = new List<DetallesRegistro>();
            
            _repository.Edit(entity);
            _unitOfWork.Commit(TypeAction.Actualizar.Value, Module.Importar.Value);
            return new EditarExcelResponse(validationResult, entity.Id, "Operación realizada satisfactoriamente");
        }
    }

    public class EditarActaValidator : AbstractValidator<EditarExcelRequest>
    {
        public EditarActaValidator(IExcelRepository repository)
        {

            ///RuleFor(r => repository.FindBy(x => x.NumeroActa == r.NumeroActa && x.Id != r.Id).FirstOrDefault()).Null().WithMessage("El número de acta ya esta registrado en el sistema");

            RuleFor(r => repository.FindBy(t =>t.Id == r.Id).FirstOrDefault()).
                NotNull().WithMessage("Excel no esta registrdao en el sistema");


        }
    }
   
}