﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA
{
    public static class ByA_EAN
    {
        public static readonly string CodigoPais = "770";
        public static string EAN13(string Codigo) {
            return Codigo + ByA_EAN.DigitoVerificacionEAN13(Codigo);
        }
        private static string DigitoVerificacionEAN13(string EAN)
        {
            int iSum = 0;
            int iSumInpar = 0;
            int iDigit = 0;

            EAN = EAN.PadLeft(13, '0');

            for (int i = EAN.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(EAN.Substring(i - 1, 1));
                if (i % 2 != 0)
                {
                    iSumInpar += iDigit;
                }
                else
                {
                    iSum += iDigit;
                }
            }

            iDigit = (iSumInpar * 3) + iSum;

            int iCheckSum = (10 - (iDigit % 10)) % 10;
            return iCheckSum.ToString();
        }
        
    }

}
