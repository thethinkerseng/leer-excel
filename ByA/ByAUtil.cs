﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.IO;

namespace ByA
{
    public static class ByAUtil
    {
        public static DataTable convertToDataTable<T>(IList<T> data)
        {
            DataTable table = null;
            if (data != null)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                table = new DataTable();

                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }

                table.TableName = "Table";
            }
            return table;
        }

        public static string convertListToXML<T>(IList<T> data) {
            DataTable tb = convertToDataTable<T>(data);
            System.IO.StringWriter writer = new System.IO.StringWriter();
            if (tb != null)
            {
                tb.WriteXml(writer, XmlWriteMode.WriteSchema, false);
            }
            return writer.ToString();
        }

        public static void SaveJPG(System.Drawing.Image image, string szFileName)
        {
            if (System.IO.File.Exists(szFileName))
            {
                System.IO.File.Delete(szFileName);
            }
            image.Save(szFileName);
        }
        public static string Byte2ImagePath(byte[] bytes)
        {
            if (bytes == null)
                return null;
            MemoryStream ms = new MemoryStream(bytes);
            System.Drawing.Bitmap bm = null;
            string oPath = Path.ChangeExtension(Path.GetTempFileName(), ".JPG");
            try
            {
                bm = new System.Drawing.Bitmap(ms);
                SaveJPG(bm, oPath);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return oPath;
        }

        public static string Right(string param, int length)
        {

            int value = param.Length - length;
            string result = param.Substring(value, length);
            return result;
        }

        public static string Left(string param, int length)
        {

            string result = param.Substring(0, length);
            return result;
        }

        public static string Hexagesimal(string str)
        {
            StringBuilder hex = new StringBuilder();
            char[] elementos = str.ToCharArray();
            for (int i = 0; i < elementos.Length; i++)
            {
                // Get the integral value of the character.
                int value = Convert.ToInt32(elementos[i]);
                // Convert the decimal value to a hexadecimal value in string form.
                string hexOutput = String.Format("{0:X}", value);
                hex.Append(hexOutput);
            }
            return hex.ToString();
        }
        public static bool IsUrlValida(string url)
        {
            Uri uriResult;
            if (string.IsNullOrEmpty(url)) {
                return false;
            } else {
                return (Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out uriResult)
                  && uriResult.Scheme == Uri.UriSchemeHttp);
            }

        }
        public static string FormatearIdentificacionConDigitoVerificacion(string Identificacion)
        {
            long numero = 0;
            if(long.TryParse(Identificacion, out numero))
            {
                int dv = CalcularDigitoVerificacion(Identificacion);
                return Identificacion + "-" + dv.ToString();
            }else
            {
                return Identificacion;
            }
        }

        public static int CalcularDigitoVerificacion(string nit)
        {
            List<int> liPeso = new List<int>();
            liPeso.Add(71);
            liPeso.Add(67);
            liPeso.Add(59);
            liPeso.Add(53);
            liPeso.Add(47);
            liPeso.Add(43);
            liPeso.Add(41);
            liPeso.Add(37);
            liPeso.Add(29);
            liPeso.Add(23);
            liPeso.Add(19);
            liPeso.Add(17);
            liPeso.Add(13);
            liPeso.Add(7);
            liPeso.Add(3);

            int liDif = 15 - nit.Count();
            int liSuma = 0;
            for (var i = 0; i < nit.Count(); i++)
            {
                liSuma += int.Parse(nit.Substring(i, 1)) * liPeso[liDif + i];
            }
            int digitoChequeo = liSuma % 11;
            if (digitoChequeo >= 2)
            {
                digitoChequeo = 11 - digitoChequeo;
            }
            return digitoChequeo;
        }

    }
}
