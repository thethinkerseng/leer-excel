﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ByA
{
    public static class ByaExtension
    {
        static Dictionary<int, string> numeros= new Dictionary<int, string>();

        static private void InicializarNumerosFijos() {
            numeros.Clear();
            numeros.Add(0, "cero");
            numeros.Add(1, "uno");
            numeros.Add(2, "dos");
            numeros.Add(3, "tres");
            numeros.Add(4, "cuatro");
            numeros.Add(5, "cinco");
            numeros.Add(6, "seis");
            numeros.Add(7, "siete");
            numeros.Add(8, "ocho");
            numeros.Add(9, "nueve");
            numeros.Add(10, "diez");
            numeros.Add(11, "once");
            numeros.Add(12, "doce");
            numeros.Add(13, "trece");
            numeros.Add(14, "catorce");
            numeros.Add(15, "quince");
            
            numeros.Add(20, "veinte");
            
            numeros.Add(30, "treinta");
            numeros.Add(40, "cuarenta");
            numeros.Add(50, "cincuenta");
            numeros.Add(60, "sesenta");
            numeros.Add(70, "setenta");
            numeros.Add(80, "ochenta");
            numeros.Add(90, "noventa");
            numeros.Add(100, "cien");
            numeros.Add(500, "quinientos");
            numeros.Add(700, "setecientos");
            numeros.Add(900, "novecientos");
            numeros.Add(1000, "mil");
            numeros.Add(1000000, "un millón");

        }
        public static string ToText(this int value)
        {
            InicializarNumerosFijos();
            string Num2Text = "";
            if (value < 0) Num2Text = "menos " + Math.Abs(value).ToText();
            else if (numeros.ContainsKey(value)) Num2Text = numeros[value];
            else if (value < 20) Num2Text= "dieci" + (value - 10).ToText();
            else if (value < 30) Num2Text= "veinti" + (value - 20).ToText();
            else if (value < 100)
            {
                int u = value % 10;
                Num2Text = string.Format("{0} y {1}", ((value / 10) * 10).ToText(), (u == 1 ? "un" : (value % 10).ToText()));
            }
            else if (value < 200) Num2Text = "ciento " + (value - 100).ToText();
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800))
                Num2Text = ((value / 100)).ToText() + "cientos";
            else if (value < 1000) Num2Text = string.Format("{0} {1}", ((value / 100) * 100).ToText(), (value % 100).ToText());
            else if (value < 2000) Num2Text = "mil " + (value % 1000).ToText();
            else if (value < 1000000)
            {
                Num2Text = ((value / 1000)).ToText() + " mil";
                if ((value % 1000) > 0) Num2Text += " " + (value % 1000).ToText();
            }
            else if (value < 2000000) Num2Text = "un millón " + (value % 1000000).ToText();
            else if (value < int.MaxValue)
            {
                Num2Text = ((value / 1000000)).ToText() + " millones";
                if ((value - (value / 1000000) * 1000000) > 0) Num2Text += " " + (value - (value / 1000000) * 1000000).ToText();
            }
            return Num2Text;
        }

        public static string ToCapital(this string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}
