﻿using NUnit.Framework;
using App.Infraestructura.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Infraestructura.System;

namespace App.Infraestructura.System.Test
{
    [TestFixture]
    public class TestByADateTime
    {
        [Test]
        public void TestFechaReal()
        {
            ByADateTime.ResetDateTime();
            var today = ByADateTime.Now;
            Assert.AreEqual(DateTime.Now, today);
        }

        [Test]
        public void TestFechaFake()
        {
            DateTime fecha = DateTime.Now.AddYears(5);
            ByADateTime.SetDateTime(fecha);
            var fakeToday = ByADateTime.Now;
            Assert.AreEqual(fecha, fakeToday);
        }
    }
}
