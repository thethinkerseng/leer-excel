﻿using Headspring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.General
{
    public class EstadosGeneral: Enumeration<EstadosGeneral, string>
    {
        public static readonly EstadosGeneral Activo = new EstadosGeneral("AC", "Activo");
        public static readonly EstadosGeneral Inactivo = new EstadosGeneral("IN", "Inactivo");
        private EstadosGeneral(string value, string displayName) : base(value, displayName) { }
    }
}
