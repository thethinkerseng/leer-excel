﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.General
{
    public class MensajeSoporte
    {
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
        public string Imagen { get; set; }
        public string Url { get; set; }
        public string IdentificacionRemitente { get; set; }
        public string NombreRemitente { get; set; }
        public string CorreoAEnviar { get; set; }
        public string NombreAplicacion { get; set; }
        public string NombreEmpresa { get; set; }
    }
}
