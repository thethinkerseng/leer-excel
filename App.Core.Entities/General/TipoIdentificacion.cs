﻿using Headspring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.General
{
    public class TipoIdentificacion : Enumeration<TipoIdentificacion, string>
    {
        public static readonly TipoIdentificacion Cedula = new TipoIdentificacion("CC", "CÉDULA DE CIUDADANÍA");
        public static readonly TipoIdentificacion Nit = new TipoIdentificacion("NIT", "NIT");
        public static readonly TipoIdentificacion Pasaporte = new TipoIdentificacion("PAS", "PASAPORTE");
        public static readonly TipoIdentificacion CedulaExtranjera = new TipoIdentificacion("CE", "CÉDULA EXTRANJERA");
        public static readonly TipoIdentificacion TarjetaIdentidad = new TipoIdentificacion("TI", "TARJETA DE IDENTIDAD");
        public static readonly TipoIdentificacion RegistroCivil = new TipoIdentificacion("RE", "REGISTRO CIVIL");
        public static readonly TipoIdentificacion TarjetaExtranjeria = new TipoIdentificacion("TE", "TARJETA DE EXTRANJERÍA");
        private TipoIdentificacion(string value, string displayName) : base(value, displayName) { }
        public static bool Is(string Value)
        {
            return TipoIdentificacion.GetAll().Any(rr => rr.Value == Value);
        }
        
        public static string GetDisplayValue(string value)
        {
            var objeto = GetAll().FirstOrDefault(rr => rr.Value == value);
            return (objeto != null ? objeto.DisplayName : value);
        }
    }
}
