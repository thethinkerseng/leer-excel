﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.General
{
    public static class Cultures
    {
        public static readonly CultureInfo Colombia =
            CultureInfo.GetCultureInfo("es-CO");


        public static readonly DateTimeStyles SinEstilo =
            DateTimeStyles.None;


        public static DateTime ObtenerFechaDDMMAA(string _fecha)
        {
            DateTime fecha;
            DateTime.TryParse(_fecha, Cultures.Colombia, Cultures.SinEstilo, out fecha);
            return fecha;
        }

        public static NumberFormatInfo GetFormatNoney(int CurrencyDecimalDigits)
        {

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            // Displays a negative value with the default number of decimal digits (2).
            // Displays the same value with four decimal digits.
            nfi.CurrencyDecimalDigits = CurrencyDecimalDigits;
            return nfi;
        }
        public static string FormatoMoneda(decimal moneda,int NumeroDecimales,char SeparadoPor=',')
        {
            return moneda.ToString("C", Cultures.GetFormatNoney(NumeroDecimales)).Replace('.', SeparadoPor).Replace(" ", "");
        }        
    }
}
