﻿using Headspring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Base
{
    public class Module : Enumeration<Module, string>
    {
        public static readonly Module Importar = new Module("IMPORTAR", "IMPORTAR EXCEL");
        public static readonly Module DatosBasicos = new Module("DATOS_BASICOS", "DATOS BASICOS");
        public static readonly Module Configuracion = new Module("CONFIGURACION", "CONFIGURACION");
        private Module(string value, string displayName) : base(value, displayName) { }
        public static bool Is(string Value)
        {
            return Module.GetAll().Any(rr => rr.Value == Value);
        }
        public static string GetDisplayValue(string value)
        {
            var objeto = GetAll().FirstOrDefault(rr => rr.Value == value);
            return (objeto != null ? objeto.DisplayName : value);
        }
    }
}
