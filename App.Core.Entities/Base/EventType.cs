﻿using Headspring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Base
{
    public class EventType : Enumeration<EventType, string>
    {
        public static readonly EventType Add = new EventType("A", "AGREGAR");
        public static readonly EventType Update = new EventType("M", "MODIFICAR");
        public static readonly EventType Delete = new EventType("D", "ELIMINAR");
        private EventType(string value, string displayName) : base(value, displayName) { }
        public static bool Is(string Value)
        {
            return EventType.GetAll().Any(rr => rr.Value == Value);
        }
        public static string GetDisplayValue(string value)
        {
            var objeto = GetAll().FirstOrDefault(rr => rr.Value == value);
            return (objeto != null ? objeto.DisplayName : value);
        }
    }
}
