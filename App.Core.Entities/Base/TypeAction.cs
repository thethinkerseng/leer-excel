﻿using Headspring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Base
{
    public class TypeAction : Enumeration<TypeAction, string>
    {
        public static readonly TypeAction Inicializar = new TypeAction("INICIALIZAR", "INICIALIZAR");
        public static readonly TypeAction Importar = new TypeAction("IMPORTAR", "IMPORTAR");
        public static readonly TypeAction Crear = new TypeAction("CREAR", "CREAR");
        public static readonly TypeAction Actualizar = new TypeAction("ACTUALIZAR", "ACTUALIZAR");
        public static readonly TypeAction Eliminar = new TypeAction("ELIMINAR", "ELIMINAR");
        public static readonly TypeAction Cancelar = new TypeAction("CANCELAR", "CANCELAR");
        public static readonly TypeAction Estado = new TypeAction("ESTADO", "ESTADO");
        public static readonly TypeAction Reducir = new TypeAction("REDUCIR", "REDUCIR");

        public static readonly TypeAction ConsultaSinAnotaciones = new TypeAction("CONSULTA_SIN_ANOTACIONES", "CONSULTA SIN ANOTACIONES");
        public static readonly TypeAction RecibirOP = new TypeAction("RECIBIR_OP", "RECIBIR ORDENES DE PAGO"); 
        public static readonly TypeAction DevolverOP = new TypeAction("DEVOLVER_OP", "DEVOLVER ORDENES DE PAGO");
        public static readonly TypeAction AsignarBeneficiario = new TypeAction("ASIGNAR_BENEFICIARIO", "ASIGNAR BENEFICIARIO");
        public static readonly TypeAction AplicarDescuento = new TypeAction("APLICAR_DESCUENTO", "APLICAR DESCUENTO");
        public static readonly TypeAction ActualizarMultiple = new TypeAction("ACTUALIZAR_MULTIPLE", "ACTUALIZAR MULTIPLE CUENTA");
        public static readonly TypeAction AsignarCuentaBancaria = new TypeAction("ASIGNAR_CUENTA", "ASIGNAR CUENTA BANCARIA");
        public static readonly TypeAction RecibirRemision = new TypeAction("RECIBIR_REMISION", "RECIBIR REMISION");
        public static readonly TypeAction GestionTercero = new TypeAction("GESTION_TERCERO", "GESTION TERCERO");
        public static readonly TypeAction AnularDescuento = new TypeAction("ANULAR_DESCUENTO", "ANULAR DESCUENTO");
        private TypeAction(string value, string displayName) : base(value, displayName) { }
        public static bool Is(string Value)
        {
            return TypeAction.GetAll().Any(rr => rr.Value == Value);
        }
        public static string GetDisplayValue(string value)
        {
            var objeto = GetAll().FirstOrDefault(rr => rr.Value == value);
            return (objeto != null ? objeto.DisplayName : value);
        }
    }
}
