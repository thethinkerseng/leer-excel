﻿using App.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Core.Entities.Base
{
    public class Parametro:AuditableEntity<int>
    {
        public Parametro()
        {
            DetallesParametro = new HashSet<DetalleParametro>();
        }

        public string Nombre{ get; set; }
        public string TipoDato { get; set; }
        public string Descripcion { get; set; }
        public string Privacidad { get; set; }
        public string Estado { get; set; }

        public virtual ICollection<DetalleParametro> DetallesParametro { get; set; }
    }
}
