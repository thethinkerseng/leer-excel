﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using App.Core.Entities.Base;

namespace App.Core.Entities.Base
{
    public class DetalleParametro : AuditableEntity<int>
    {
        public int ParametroID { get; set; }
        public int Item { get; set; }
        public DateTime FecInicial { get; set; }
        public DateTime FecFinal { get; set; }
        public string Valor { get; set; }
        public string Estado { get; set; }
        //[ForeignKey("ParametroID")]
        public virtual Parametro Parametro { get; set; }

    }
}
