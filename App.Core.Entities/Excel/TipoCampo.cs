﻿using Headspring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Excel
{
    public class TipoCampo : Enumeration<TipoCampo, string>
    {
        public static readonly TipoCampo Numero = new TipoCampo("NUMERO", "NUMERO");
        public static readonly TipoCampo Texto = new TipoCampo("TEXTO", "TEXTO");
        public static readonly TipoCampo Fecha = new TipoCampo("FECHA", "FECHA");
        private TipoCampo(string value, string displayName) : base(value, displayName) { }
        public static string ObtenerTipo(Type type)
        {
            return type.Name;
        }
    }
}
