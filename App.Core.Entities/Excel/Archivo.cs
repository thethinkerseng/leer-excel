﻿using App.Core.Entities.Base;
using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Excel
{
    public class Archivo : AuditableEntity<int>
    {
        public Archivo()
        {
        }

        public DateTime Fecha { get; set; }
        public TimeSpan Hora { get; set; }
        public string ArchivoNombre { get; set; }
        public string ArchivoExtension { get; set; }
        public string ArchivoTipo { get; set; }
        public int ArchivoLong { get; set; }
        public string Estado { get; set; }
        public virtual ICollection<DetallesRegistro> Registros { get; set; }
        //public virtual ICollection<DetallesColumna> Columnas { get; set; }
    }
}
