﻿using App.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Excel
{
    public class DetallesColumna : AuditableEntity<int>
    {
        public DetallesColumna()
        {
        }
        public string Clave { get; set; }
        public string Valor { get; set; }
        public string Tipo { get; set; }
        public int RegistroId { get; set; }
        public virtual DetallesRegistro Registro { get; set; }
        //public int ExcelId { get; set; }
        //public virtual Archivo Excel { get; set; }
    }
}
