﻿using App.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Excel
{
    public class DetallesRegistro : AuditableEntity<int>
    {
        public DetallesRegistro()
        {
        }

        public int Posicion { get; set; }
        public int ExcelId { get; set; }
        public virtual Archivo Excel { get; set; }
        public virtual ICollection<DetallesColumna> Items { get; set; }
    }
}
