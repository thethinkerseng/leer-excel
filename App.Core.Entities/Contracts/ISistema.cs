﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Contracts
{
    public interface ISistema
    {
        string UserName { get; }
        DateTime Now { get; }
        IRecursos ObtenerRecursos();
    }
}
