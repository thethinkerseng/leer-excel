﻿using System;

namespace App.Core.Entities.Contracts
{
    public interface IRecursos
    {
        string Logo { get; }
    }
    public class Recursos : IRecursos
    {
        public string Logo { get; set; }
    }
}