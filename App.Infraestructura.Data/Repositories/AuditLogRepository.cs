﻿using App.Core.Entities.Base;
using App.Core.Entities.General;
using App.Core.UseCase;
using App.Core.UseCase.Contracts;
using App.Infraestructura.Data.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data
{
    public class AuditLogRepository : GenericRepositoryAuditLog<AuditLog>, IAuditLogRepository
    {
        public AuditLogRepository(IDbContext context)
              : base(context)
        {

        }

        public List<AuditLog> FindBy(IParamAuditLog param)
        {
            var query = this.AsQueryable();
            if (!string.IsNullOrEmpty(param.Accion))
            {
                query = query.Where(x => x.Action == param.Accion);
            }
            if (!string.IsNullOrEmpty(param.Modulo))
            {
                query = query.Where(x => x.Module == param.Modulo);
            }
            if (!string.IsNullOrEmpty(param.Tabla))
            {
                query = query.Where(x => x.TableName == param.Tabla);
            }
            if ((param.FechaInicial.HasValue && param.FechaInicial?.Year > 1) && param.FechaFinal.HasValue && param.FechaFinal?.Year > 1)
            {
                query = query.Where(x => x.EventDateLocalTime >= param.FechaInicial.Value && x.EventDateLocalTime <= param.FechaFinal.Value);
            }
            if (param.FechaFinal.HasValue && param.FechaInicial.HasValue)
            {
                DateTime FechaFinal = new DateTime(param.FechaFinal.Value.Year, param.FechaFinal.Value.Month, param.FechaFinal.Value.Day, 23, 59, 59);
                DateTime FechaInicial = new DateTime(param.FechaInicial.Value.Year, param.FechaInicial.Value.Month, param.FechaInicial.Value.Day, 0, 0, 0);
                if(FechaFinal.Year > 1 && FechaInicial.Year > 1)
                {
                    query = query.Where(x => x.EventDateLocalTime >= FechaInicial && x.EventDateLocalTime <= FechaFinal);
                }
            }
            return query.ToList();
        }

        public List<string> GetNameTable()
        {
            return this.AsQueryable().GroupBy(x => x.TableName).Select(x => x.Key).ToList();
        }
    }
}
