﻿using App.Core.Entities.Base;
using App.Core.UseCase;
using App.Core.UseCase.Base;
using App.Infraestructura.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data
{
 
    public class ParametroRepository : GenericRepository<Parametro>, IParametroRepository
    {
        public ParametroRepository(IDbContext context) : base(context)
        {

        }
    }
}
 