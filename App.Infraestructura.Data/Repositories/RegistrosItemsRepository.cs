﻿using App.Core.Entities.Excel;
using App.Core.Entities.General;
using App.Core.UseCase;
using App.Infraestructura.Data.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data
{
    public class RegistrosItemsRepository : GenericRepository<DetallesColumna>, IRegistrosItemsRepository
    {
    
        public RegistrosItemsRepository(IDbContext context)
              : base(context)
        {

        }
        
    }
}
