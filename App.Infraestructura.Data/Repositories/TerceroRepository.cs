﻿using App.Core.Entities.General;
using App.Core.UseCase;
using App.Infraestructura.Data.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data
{
    public class TerceroRepository : GenericRepository<Tercero>, ITerceroRepository
    {
    
        public TerceroRepository(IDbContext context)
              : base(context)
        {

        }

        public string ObtenerNombreCompleto(string identificacion)
        {
            return this.FindBy(t => t.Identificacion == identificacion).Select(x => x.NombreCompleto).FirstOrDefault();
        }
    }
}
