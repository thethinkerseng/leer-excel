﻿using App.Core.Entities.Excel;
using App.Core.Entities.General;
using App.Core.UseCase;
using App.Infraestructura.Data.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data
{
    public class ExcelRepository : GenericRepository<Archivo>, IExcelRepository
    {
    
        public ExcelRepository(IDbContext context)
              : base(context)
        {

        }
        
    }
}
