﻿using App.Core.Entities.Base;
using App.Infraestructura.Data.Base;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using App.Infraestructura.Data.Map;
using App.Core.Entities.General;
using App.Core.Entities.Excel;

namespace App.Infraestructura.Data
{
    public class AppContext : DbContextBase
    {
      
        public AppContext()
            : base("Name=DataContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppContext, Migrations.Configuration>("DataContext"));
        }
        
        public virtual DbSet<Parametro> Parametros { get; set; }
        public virtual DbSet<DetalleParametro> DetalleParametros { get; set; }
        public virtual DbSet<Tercero> Terceros { get; set; }
        public virtual DbSet<Archivo> Excel { get; set; }
        public virtual DbSet<DetallesRegistro> Registros { get; set; }
        public virtual DbSet<DetallesColumna> DetallesRegistros { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            MaperEntityToDataBase.MapearEntityToDataBase(modelBuilder);
        }

    }
}
