﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.InicializarDatos
{
    public class TestDbInitializer : DropCreateDatabaseAlways<AppContext>
    {
        protected override void Seed(AppContext context)
        {
            if (context.Terceros.Count() == 0)
            {
                InicializarDatos.Inicializar.Seed(context);
            }

            base.Seed(context);
        }
    }
}
