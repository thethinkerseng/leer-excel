﻿using App.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.InicializarDatos
{
    class IniciaizarSalarioMinimo
    {
        protected IniciaizarSalarioMinimo()
        {

        }
        public static void Seed(AppContext ctx)
        {
            Parametro salarioMinimo = new Parametro()
            {
                Nombre = "SALARIO MINIMO",
                Estado = "ACTIVO",
                TipoDato = "N",
                Descripcion = "Salario Minimo Mensual Legal Vigente",
                Privacidad = "",
                DetallesParametro = new List<DetalleParametro>() {
                         new DetalleParametro() { Estado="ACTIVO", FecInicial=new DateTime(2017,01,01), FecFinal=new DateTime(2017,12,31), Valor="737717" }
                    }
            };
            ctx.Parametros.Add(salarioMinimo);
            ctx.SaveChanges("admin", DateTime.Now, TypeAction.Inicializar.Value, Module.DatosBasicos.Value);
        }
    }
}
