﻿using App.Core.Entities.Base;
using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.InicializarDatos
{
    internal static class Inicializar
    {
        internal static void Seed(AppContext ctx)
        {
            InicializarTerceros.Seed(ctx);
            IniciaizarSalarioMinimo.Seed(ctx);
            ctx.SaveChanges("admin", DateTime.Now, TypeAction.Inicializar.Value, Module.DatosBasicos.Value);
        }
        
    }
}
