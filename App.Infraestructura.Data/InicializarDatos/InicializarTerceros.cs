﻿using App.Core.Entities.Base;
using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.InicializarDatos
{
    public static class InicializarTerceros
    {
        public static List<UsuarioSeed> ListUsuarios()
        {
            const string PASSWORD = "usuario2018.";
            List<UsuarioSeed> Users = new List<UsuarioSeed>()
            {
                new UsuarioSeed()
                {
                    Email="byasoporte@gmail.com",
                    Password=PASSWORD,
                    Username="usuario",
                    Rol=new List<string>()
                    {
                        "ARCHIVO_Importar",
                        "ARCHIVO_Consulta",
                    },
                    NombreContacto ="Usuario",
                    PrimerApellido="Usuario",
                    PrimerNombre="Usuario",
                    SegundoApellido="Usuario",
                    SegundoNombre="Usuario",
                    Identificacion="00000000"
                }
            };
            return Users;
        }
        public class UsuarioSeed
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public List<string> Rol { get; set; }
            public string PrimerNombre { get; set; }
            public string SegundoNombre { get; set; }
            public string PrimerApellido { get; set; }
            public string SegundoApellido { get; set; }
            public string NombreContacto { get; set; }
            public string Identificacion { get; set; }
        }
        public static void Seed(AppContext ctx)
        {
            int cont = 0;
            List<Tercero> terceros = new List<Tercero>();
            foreach (UsuarioSeed user in ListUsuarios())
            {
                Tercero tercero = new Tercero()
                {
                    Id = cont,
                    Email = user.Email,
                    Identificacion = user.Identificacion,
                    NombreContacto = user.NombreContacto,
                    PrimerNombre = user.PrimerNombre,
                    SegundoNombre = user.SegundoNombre,
                    PrimerApellido = user.PrimerApellido,
                    SegundoApellido = user.SegundoApellido,
                    FechaExpedicion = new DateTime(2010, 10, 20),
                    Ciudad = "20001",
                    TipoIdentidad = TipoIdentificacion.Cedula.Value,
                    Estado = "AC"

                };
                terceros.Add(tercero);
                cont++;
            }
            ctx.Terceros.AddRange(terceros);
            ctx.SaveChanges("admin", DateTime.Now, TypeAction.Inicializar.Value, Module.DatosBasicos.Value);
        }

    
    }
}
