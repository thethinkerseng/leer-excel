namespace App.Infraestructura.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<App.Infraestructura.Data.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(App.Infraestructura.Data.AppContext context)
        {
            if (context.Terceros.Count() == 0)
            {
                InicializarDatos.Inicializar.Seed(context);
            }
        }
    }
}
