namespace App.Infraestructura.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditLog",
                c => new
                    {
                        AuditLogID = c.Guid(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 50),
                        EventDateUTC = c.DateTime(nullable: false),
                        EventDateLocalTime = c.DateTime(nullable: false),
                        EventType = c.String(nullable: false, maxLength: 1),
                        TableName = c.String(nullable: false, maxLength: 100),
                        RecordID = c.String(nullable: false, maxLength: 100),
                        ColumnName = c.String(nullable: false, maxLength: 100),
                        OriginalValue = c.String(),
                        NewValue = c.String(),
                        Action = c.String(nullable: false),
                        Module = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AuditLogID);
            
            CreateTable(
                "dbo.DetalleParametro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParametroID = c.Int(nullable: false),
                        Item = c.Int(nullable: false),
                        FecInicial = c.DateTime(nullable: false),
                        FecFinal = c.DateTime(nullable: false),
                        Valor = c.String(),
                        Estado = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IPAddress = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parametro", t => t.ParametroID, cascadeDelete: true)
                .Index(t => t.ParametroID);
            
            CreateTable(
                "dbo.Parametro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        TipoDato = c.String(nullable: false),
                        Descripcion = c.String(nullable: false),
                        Privacidad = c.String(nullable: false),
                        Estado = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IPAddress = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DetallesColumna",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Clave = c.String(),
                        Valor = c.String(),
                        Tipo = c.String(),
                        RegistroId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IPAddress = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DetallesRegistro", t => t.RegistroId, cascadeDelete: true)
                .Index(t => t.RegistroId);
            
            CreateTable(
                "dbo.DetallesRegistro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Posicion = c.Int(nullable: false),
                        ExcelId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IPAddress = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Archivo", t => t.ExcelId, cascadeDelete: true)
                .Index(t => t.ExcelId);
            
            CreateTable(
                "dbo.Archivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Hora = c.Time(nullable: false, precision: 7),
                        ArchivoNombre = c.String(),
                        ArchivoExtension = c.String(),
                        ArchivoTipo = c.String(),
                        ArchivoLong = c.Int(nullable: false),
                        Estado = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IPAddress = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tercero",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipoIdentidad = c.String(nullable: false),
                        Identificacion = c.String(nullable: false),
                        DigitoVerificacion = c.String(),
                        FechaExpedicion = c.DateTime(),
                        TipoPersona = c.String(),
                        Tipo = c.String(),
                        PrimerNombre = c.String(),
                        SegundoNombre = c.String(),
                        PrimerApellido = c.String(),
                        SegundoApellido = c.String(),
                        NombreContacto = c.String(),
                        RazonSocial = c.String(),
                        Telefono = c.String(),
                        Movil1 = c.String(),
                        Movil2 = c.String(),
                        Movil3 = c.String(),
                        Direccion = c.String(),
                        Pais = c.String(),
                        Departamento = c.String(),
                        Ciudad = c.String(),
                        Email = c.String(nullable: false, maxLength: 50),
                        Regimen = c.String(),
                        CodDane = c.String(),
                        Imagen = c.String(),
                        Estado = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        IPAddress = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetallesColumna", "RegistroId", "dbo.DetallesRegistro");
            DropForeignKey("dbo.DetallesRegistro", "ExcelId", "dbo.Archivo");
            DropForeignKey("dbo.DetalleParametro", "ParametroID", "dbo.Parametro");
            DropIndex("dbo.DetallesRegistro", new[] { "ExcelId" });
            DropIndex("dbo.DetallesColumna", new[] { "RegistroId" });
            DropIndex("dbo.DetalleParametro", new[] { "ParametroID" });
            DropTable("dbo.Tercero");
            DropTable("dbo.Archivo");
            DropTable("dbo.DetallesRegistro");
            DropTable("dbo.DetallesColumna");
            DropTable("dbo.Parametro");
            DropTable("dbo.DetalleParametro");
            DropTable("dbo.AuditLog");
        }
    }
}
