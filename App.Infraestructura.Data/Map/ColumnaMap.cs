﻿using App.Core.Entities.Excel;
using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.Map
{
    public class ColumnaMap : EntityTypeConfiguration<DetallesColumna>
    {
        public ColumnaMap()
        {
            HasKey(t => t.Id);
            HasRequired(t => t.Registro).WithMany(x => x.Items).HasForeignKey(x => x.RegistroId).WillCascadeOnDelete(true);
            //HasRequired(t => t.Excel).WithMany(x => x.Columnas).HasForeignKey(x => x.ExcelId).WillCascadeOnDelete(true);
        }
    }
}
