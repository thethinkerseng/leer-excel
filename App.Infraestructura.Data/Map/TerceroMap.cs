﻿using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.Map
{
    public class TerceroMap: EntityTypeConfiguration<Tercero>
    {
        public TerceroMap()
        {
            HasKey(t => t.Id);
            Property(t => t.Email).IsRequired().HasMaxLength(50);
            Property(t => t.TipoIdentidad).IsRequired();
            Property(t => t.Identificacion).IsRequired();
        }
    }
}
