﻿using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.Map
{
    static class MaperEntityToDataBase
    {
        internal static void MapearEntityToDataBase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ParametroMap());
            modelBuilder.Configurations.Add(new RegistroMap());
            modelBuilder.Configurations.Add(new ColumnaMap());
            modelBuilder.Configurations.Add(new ArchivoMap());
            modelBuilder.Configurations.Add(new TerceroMap());
        }
    }
}
