﻿using App.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.Map
{

    public class ParametroMap: EntityTypeConfiguration<Parametro>
    {
        public ParametroMap()
        {
            HasKey(t => t.Id);
            Property(t => t.Nombre).IsRequired();
            Property(t => t.TipoDato).IsRequired();
            Property(t => t.Descripcion).IsRequired();
            Property(t => t.Privacidad).IsRequired();
            Property(t => t.Estado).IsRequired();
        }
    }
}
