﻿using App.Core.Entities.Excel;
using App.Core.Entities.General;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infraestructura.Data.Map
{
    public class RegistroMap : EntityTypeConfiguration<DetallesRegistro>
    {
        public RegistroMap()
        {
            HasKey(t => t.Id);
            HasRequired(t => t.Excel).WithMany(x => x.Registros).HasForeignKey(x => x.ExcelId).WillCascadeOnDelete(true);
        }
    }
}
