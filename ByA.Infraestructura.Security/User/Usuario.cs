﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security.User
{
    public class Usuario
    {
        public System.Guid ApplicationID { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string NewPassword { get; set; }
        public decimal PasswordFormat { get; set; }
        public string PasswordSalt { get; set; }
        public string MobilePin { get; set; }
        public string Email { get; set; }
        public decimal IsApproved { get; set; }
        public decimal IsLockedout { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime FechaDesactivacion { get; set; }
        public string Estado { get; set; }
        public string UserName { get; set; }
        public object UrlConfirmar { get; internal set; }
        public string Tercero { get; set; }
    }
}
