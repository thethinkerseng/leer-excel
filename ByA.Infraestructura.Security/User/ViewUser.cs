﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security.User
{
    public class ViewUser
    {
        public string Email { get; set; }
        public System.DateTime LastLogindate { get; set; }
        public System.DateTime LastPasswordChangeDate { get; set; }
        public System.DateTime FechaDesactivacion { get; set; }
        public System.DateTime LastLockoutDate { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public string Tercero { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string CorreoConfirmado { get { return EmailConfirmed ? "SI" : "NO"; } }
        public string FinBloqueo { get { return LockoutEndDateUtc == null ? "NA" : LockoutEndDateUtc.Value.AddHours((DateTime.Now.Hour - DateTime.UtcNow.Hour)).ToString(); } }
        public string BloqueoHabilitado { get { return LockoutEnabled ? "SI" : "NO"; } }
        public int AccessosFallidos { get { return AccessFailedCount; } }
        public decimal IsAnonymous { get; set; }
        public System.DateTime LastActivityDate { get; set; }
        public object UserName { get; internal set; }
        public object Estado { get; internal set; }
    }
}
