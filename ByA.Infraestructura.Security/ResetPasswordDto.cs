﻿namespace ByA.Infraestructura.Security
{
    public class ResetPasswordDto
    {
        public object Code { get; internal set; }
        public string Email { get; internal set; }
        public object Password { get; internal set; }
        public string CurrentPassword { get; internal set; }
        public string NewPassword { get; internal set; }
        public string RePassword { get; internal set; }
        public string UserId { get; internal set; }
    }
}