﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security
{
    [Serializable]
    public class SecurityException : Exception
    {
        public SecurityException() : base() { }
        public SecurityException(String message) : base(message) { }
        public SecurityException(String message, Exception innerException) : base(message, innerException) { }
        protected SecurityException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
