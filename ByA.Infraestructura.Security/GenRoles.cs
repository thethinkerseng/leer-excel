﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByA;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;


namespace ByA.Infraestructura.Security
{
    public class GenRoles
    {
        private readonly RoleManager<IdentityRole> roleMgr;

        private readonly UserManager<IdentityUser> userMgr;

        public GenRoles() {

            RoleStore<IdentityRole> roleStore = new RoleStore<IdentityRole>();
            roleMgr = new RoleManager<IdentityRole>(roleStore);

            UserStore<IdentityUser> userStore = new UserStore<IdentityUser>();
            userMgr = new UserManager<IdentityUser>(userStore);
        }

        private static string _ADMIN  { get { return "admin"; } }


        public string GenerarRolesA()
        {
            var user = userMgr.FindByName(_ADMIN);



            StringBuilder auxstrrole = new StringBuilder();
            List<Menu> lt;
            RepMenu db = new RepMenu();
            lt = db.LstMenu.ToList();

            foreach (var item in lt)
            {
                item.SubMenu = item.SubMenu.ToList();
                foreach (var itemSubMenu in item.SubMenu)
                {
                    string rol = itemSubMenu.Roles;
                    bool RolExiste = roleMgr.RoleExists(rol);
                    if ((!RolExiste))
                    {
                        roleMgr.Create(new IdentityRole(rol));
                        userMgr.AddToRole(user.Id, rol);

                        auxstrrole.Append(rol + "<br>");
                    }
                    else
                    {
                        bool adminTieneElRol = userMgr.IsInRole(user.Id, rol);
                        if (!adminTieneElRol)
                        {
                            userMgr.AddToRole(user.Id, rol);

                            auxstrrole.Append(rol + "<br>");
                        }
                    }
                }
            }
            return auxstrrole.ToString();
        }
    }
}