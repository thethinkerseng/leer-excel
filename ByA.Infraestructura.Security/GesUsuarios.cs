﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByA;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System.Web;
using System.Net;
using System.Threading.Tasks;
using ByA.Infraestructura.Security.User;
using AutoMapper;
using System.Text;

namespace ByA.Infraestructura.Security
{

    public class GesUsuarios
    {
        public ApplicationDbContext context { get; set; }
        public ApplicationUserManager manager { get; set; }
        public UserStore<ApplicationUser> userStore { get; set; }
        public bool EnviarToken { get; set; }
        public ByARpt byaRpt { get; set; }
        public GesUsuarios()
        {
            
            context = new ApplicationDbContext();
            userStore = new UserStore<ApplicationUser>(context);
            manager = new ApplicationUserManager(userStore);

            EnviarToken = false;
            byaRpt = new ByARpt();
        }
        public GesUsuarios(ApplicationUserManager _manager)
        {
            manager = _manager;
            EnviarToken = true;
            byaRpt = new ByARpt();
        }

        public async Task<ByARpt> InsUsuarios(Usuario usuario, string UrlConfirmar)
        {
            if (ByAUtil.IsUrlValida(UrlConfirmar))
            {
                CrearUsuario(usuario);
                if (!byaRpt.Error && EnviarToken)
                {
                    await EnviarCorreoDeConfirmacion(usuario, UrlConfirmar, "confirm");
                }
            }
            else
            {
                throw new SecurityException("No es una URL válida");
            }
            return byaRpt;
        }

        public List<ViewUser> GetUsuarios(string v)
        {
            List<ViewUser> lst = new List<ViewUser>();
            List<ApplicationUser> lstO = manager.Users.ToList();
            Mapper.Initialize(cfg => { cfg.CreateMap<ApplicationUser, ViewUser>(); });
            Mapper.Map(lstO, lst);
            return lst;
        }

        public ByARpt UsuarioRegistrado(Usuario usuario)
        {
            ByARpt resultado = new ByARpt();
            var resultadoUsuario = manager.Users.Where(x => x.UserName == usuario.UserName).FirstOrDefault();
            var resultadoEmail = manager.Users.Where(x => x.Email == usuario.Email).FirstOrDefault();
            if (resultadoUsuario != null && resultadoEmail != null)
            {
                resultado.Error = true;
                resultado.Mensaje = "Identificación y correo se encuentran registrados";
            }
            else
            {
                if (resultadoUsuario != null)
                {
                    resultado.Error = true;
                    resultado.Mensaje = "Esta identificación se encuentra registrada";
                }
                if (resultadoEmail != null)
                {
                    resultado.Error = true;
                    resultado.Mensaje = "El correo se encuentra registrado";
                }
            }

            return resultado;
        }


        public ByARpt GuardarRoles(List<ModuloRoles> lst, string UserName)
        {
            int contador = 0;
            var user = manager.FindByName(UserName);
            IdentityResult IdUserResult;
            foreach (ModuloRoles item in lst)
            {
                bool hasRoolAnt = manager.IsInRole(user.Id, item.Roles);
                if (item.hasRol != hasRoolAnt)//Si cambió
                {
                    try
                    {
                        if (item.hasRol)
                        {
                            IdUserResult = manager.AddToRole(user.Id, item.Roles);
                        }
                        else
                        {
                            IdUserResult = manager.RemoveFromRole(user.Id, item.Roles);
                        }
                        if (IdUserResult.Succeeded)
                        {
                            contador++;
                        }
                    }
                    catch (Exception ex)
                    {
                        byaRpt.Mensaje = ex.Message;
                    }
                }
            }
            if (contador>0)
            {
                byaRpt.Mensaje = "No realizó ningun cambio de Roles al usuario";
            }
            else
            {
                byaRpt.Mensaje = "Operación Realizada Satisfactoriamente!!";
            }

            byaRpt.Error = false;
            return byaRpt;
        }

        public ByARpt Forzar_Cambio_Clave(Usuario Reg)
        {
            try
            {
                var user = manager.FindByName(Reg.UserName);
                manager.RemovePassword(user.Id);
                manager.AddPassword(user.Id, Reg.Password);
                byaRpt.Mensaje = "Se realizó el cambio de contraseña";
                byaRpt.Error = false;
            }
            catch (System.Exception ex)
            {
                byaRpt.Mensaje = "Error de App:" + ex.Message;
                byaRpt.Error = true;
            }
            return byaRpt;

        }
        private async Task EnviarCorreoDeConfirmacion(Usuario User, string UrlConfirmar, string Tipo)
        {
            string code = manager.GenerateEmailConfirmationToken(User.UserId);
            code = WebUtility.UrlEncode(code);
            UrlConfirmar = UrlConfirmar.Replace("user.Id", User.UserId);
            var callbackUrl = UrlConfirmar.Replace("codigo", code);
            await this.manager.SendEmailAsync(User.UserId, GetAsunto(Tipo), WebUtility.HtmlDecode(GetMensaje(User, callbackUrl, Tipo)));
            byaRpt.Mensaje += string.Format("Se ha enviado un Correo a {0} para que Confirme su Cuenta de Usuario", User.Email);
        }
        public string GetAsunto(string Tipo)
        {
            if (Tipo == "forgot")
            {
                return "Cambio de Contraseña";
            }
            else
            {
                return "Confirme su Cuenta";
            }
        }
        private static string GetMensaje(Usuario usuario, string callbackUrl, string tipo)
        {
            if (tipo == "forgot")
            {
                return "Para cambiar tu contraseña has click <a href=\"" + callbackUrl + "\">aquí</a>";
            }
            else
            {
                return BodyConfirm(usuario, callbackUrl);
            }
        }
        public static string BodyConfirm(Usuario usuario, string callback)
        {
            string text = "";
            text = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'><html xmlns='http://www.w3.org/1999/xhtml' xmlns='http://www.w3.org/1999/xhtml' style='min-height: 100%; background-color: #f3f3f3;'>&#13;&#13;<head> &#13; <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>&#13; <meta name='viewport' content='width=device-width'/>&#13; <title>Title</title>&#13; &#13; &#13; &#13;</head>&#13;&#13;<body style='width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;'> <style type='text/css'> a:hover{color: #147dc2 !important;}a:active{color: #147dc2 !important;}a:visited{color: #2199e8 !important;}h1 a:visited{color: #2199e8 !important;}h2 a:visited{color: #2199e8 !important;}h3 a:visited{color: #2199e8 !important;}h4 a:visited{color: #2199e8 !important;}h5 a:visited{color: #2199e8 !important;}h6 a:visited{color: #2199e8 !important;}table.button:hover table tr td a{color: #fefefe !important;}table.button:active table tr td a{color: #fefefe !important;}table.button table tr td a:visited{color: #fefefe !important;}table.button.tiny:hover table tr td a{color: #fefefe !important;}table.button.tiny:active table tr td a{color: #fefefe !important;}table.button.tiny table tr td a:visited{color: #fefefe !important;}table.button.small:hover table tr td a{color: #fefefe !important;}table.button.small:active table tr td a{color: #fefefe !important;}table.button.small table tr td a:visited{color: #fefefe !important;}table.button.large:hover table tr td a{color: #fefefe !important;}table.button.large:active table tr td a{color: #fefefe !important;}table.button.large table tr td a:visited{color: #fefefe !important;}table.button:hover table td{background: #147dc2 !important; color: #fefefe !important;}table.button:visited table td{background: #147dc2 !important; color: #fefefe !important;}table.button:active table td{background: #147dc2 !important; color: #fefefe !important;}table.button:hover table a{border: 0 solid #147dc2 !important;}table.button:visited table a{border: 0 solid #147dc2 !important;}table.button:active table a{border: 0 solid #147dc2 !important;}table.button.secondary:hover table td{background: #919191 !important; color: #fefefe !important;}table.button.secondary:hover table a{border: 0 solid #919191 !important;}table.button.secondary:hover table td a{color: #fefefe !important;}table.button.secondary:active table td a{color: #fefefe !important;}table.button.secondary table td a:visited{color: #fefefe !important;}table.button.success:hover table td{background: #23bf5d !important;}table.button.success:hover table a{border: 0 solid #23bf5d !important;}table.button.alert:hover table td{background: #e23317 !important;}table.button.alert:hover table a{border: 0 solid #e23317 !important;}table.button.warning:hover table td{background: #cc8b00 !important;}table.button.warning:hover table a{border: 0px solid #cc8b00 !important;}.thumbnail:hover{box-shadow: 0 0 6px 1px rgba(33, 153, 232, 0.5) !important;}.thumbnail:focus{box-shadow: 0 0 6px 1px rgba(33, 153, 232, 0.5) !important;}&gt; </style>&#13; &#13; <table data-made-with-foundation='' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; background-color: #ccc; margin: 0; padding: 0;' bgcolor='#ccc'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <td align='center' valign='top' style='word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; float: none; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; margin: 0 auto; padding: 0;'> &#13; <center data-parsed='' style='width: 100%; min-width: 0 !important;'> &#13; &#13; &#13; <br/>&#13; <table align='center' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: center; width: 50% !important; float: none; background-color: #fefefe; margin: 0 auto; padding: 0;' bgcolor='#fefefe'> &#13; <tbody> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <td style='word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;' align='left' valign='top'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;'> &#13; <tbody> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;' align='left'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; background-color: #147dc2; padding: 0;' bgcolor='#147dc2'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 20px 0;' align='left'> &#13; <img src='{{logo}}' style='width: 200px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; clear: both; display: block; float: none; text-align: center; height: auto; margin: 0 auto;' mc:label='header_image' mc:edit='header_image' mc:allowdesigner='mc:allowdesigner' mc:allowtext='mc:allowtext' align='none'/>&#13; </th>&#13; </tr>&#13; </table>&#13; </th>&#13; </tr>&#13; </tbody>&#13; </table>&#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; padding: 0;'> &#13; <tbody> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='width: 80% !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; height: auto !important; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block !important; margin: 0 auto; padding: 0 16px 16px;' align='left'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 60%; margin:0 auto; padding: 0;'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; &#13; &#13; </tr>&#13; <tr style='vertical-align: top; text-align: left; padding: 0px;' align='left'> &#13; <th style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 5px;' align='left'> &#13; <br/>&#13; <p style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: justify; line-height: 1.3; font-size: 14px; margin: 0 0 10px; padding: 0;' align='justify'> <b>{{NombreCompleto}}</b> te damos la bienvenida a SIRCC LEC. Confirma tu dirección de correo electrónico para completar tu cuenta. Es fácil, solo haz clic en el botón de abajo. </p>&#13; <br/>&#13; &#13; <p style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: justify; line-height: 1.3; font-size: 14px; margin: 0px; padding: 0px;' align='justify'> &#13; </p>&#13; &#13; <center data-parsed='' style='width: 100%; min-width: 0 !important;'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: center; float: none; width: auto; margin: 0 0 16px; padding: 0;'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <td style='word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;' align='left' valign='top'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <td style='word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #fefefe; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; background-color: #2199e8; margin: 0; padding: 0; border: 2px solid #2199e8;' align='left' bgcolor='#2199e8' valign='top'><a href='{{uri}}' target='_blank' style='color: #fefefe; font-family: Helvetica, Arial, sans-serif; font-weight: bold; text-align: left; line-height: 1.3; text-decoration: none; font-size: 16px; display: inline-block; border-radius: 3px; margin: 0; padding: 8px 16px; border: 0 solid #2199e8;'>Activar Cuenta</a></td>&#13; </tr>&#13; </table>&#13; </td>&#13; </tr>&#13; </table>&#13; </center>&#13; <br/>&#13; <p style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: justify; line-height: 1.3; font-size: 14px; margin: 0px; padding: 0px;' align='justify'> &#13; El usuario para acceder a su cuenta es:{{Nombre de usuario}}<br/>&#13; <br/>&#13; </p>&#13; <p style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: justify; line-height: 1.3; font-size: 14px; margin: 0 0 10px; padding: 0;' align='justify'> &#13; Si estás teniendo problemas para hacer clic en el botón de 'Activar Cuenta', copia y pega la siguiente URL en la barra de direcciones de tu navegador web. <br/> </p><center style='text-align: center; width: 600px; margin-left: 15px; min-width: 0 !important;' align='center'> &#13; <a href='{{uri}}' style='color: #2199e8; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; text-decoration: none; margin: 0; padding: 0;'>{{uri}}</a>&#13; &#13; </center>&#13; &#13; <br/> <p style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: justify; line-height: 1.3; font-size: 14px; margin: 0 0 10px; padding: 0;' align='justify'> </p>&#13; &#13; </th>&#13; <th style='visibility: hidden; width: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;' align='left'></th>&#13; </tr>&#13; </table>&#13; </th>&#13; </tr>&#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='width: 100% !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; height: auto !important; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block !important; margin: 0 auto; padding: 0 16px 16px;' align='left'> &#13; &#13; </th>&#13; </tr>&#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;' align='left'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; padding: 0;'> &#13; <tbody> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;' align='left'> &#13; <p style='text-align: left; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 14px; margin: 0 0 -1px; padding: 23px;' align='left'> &#13; Saludos <br/>&#13; El equipo de B&amp;A Systems.&#13; </p>&#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; background-color: #147dc2; padding: 0;' bgcolor='#147dc2'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 20px 0;' align='left'> &#13; <table style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;'> &#13; <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'> &#13; <th style='width: 100% !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; display: inline-block !important; margin: 0; padding: 0px;' align='left'> &#13; <p style='color: white; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: justify; line-height: 1.3; font-size: 8px; margin: 0 0 10px; padding:0px 25px 0px 25px;' align='justify'> &#13; <b> &#13; La información enviada en este mensaje electrónico es confidencial y solo para uso de la persona/compañía identificada en el mismo. Si el receptor de este mensaje no es la persona de destino mencionada, cualquier divulgación, distribución y/o copia de la información contenida en este mensaje electrónico, se encuentra estrictamente prohibida. Si usted recibe este mensaje por error, por favor notifique al emisor del mismo de inmediato.&#13; </b>&#13; </p>&#13; <p style='text-align: center; color: white; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 6px; margin: 0 0 -17px; padding: 0;' align='center'> &#13; <em>Copyright © 2016 B&amp;A System. Todos los derechos reservados.</em>&#13; </p>&#13; </th>&#13; </tr>&#13; </table>&#13; </th>&#13; </tr>&#13; </table>&#13; </th>&#13; </tr>&#13; </tbody>&#13; </table>&#13; </th>&#13; </tr>&#13; </tbody>&#13; </table>&#13; &#13; </td>&#13; </tr>&#13; </tbody>&#13; </table>&#13; <br/>&#13; </center>&#13; </td>&#13; </tr>&#13; </table>&#13;</body>&#13;&#13;</html>";
            text = text.Replace("{{logo}}", "");
            text = text.Replace("{{uri}}", callback);
            text = text.Replace("{{URLEntidad}}", " " + ""+ " ");
            text = text.Replace("{{TelefonosEntidad}}", " " + ""+ " ");
            text = text.Replace("{{CorreoEntidad}}", " " + ""+ " ");
            text = text.Replace("{{DirEntidad}}", " " + "" + " ");
            text = text.Replace("{{HorarioEntidad}}", " " + "" + " ");
            text = text.Replace("{{Nombre de la entidad}}", " " + "" + " ");
            text = text.Replace("{{Nombre de usuario}}", " " + usuario.UserName + " ");
            text = text.Replace("{{NombreCompleto}}", " " + usuario.Tercero + " ");
            return text;
        }
        private void CrearUsuario(Usuario Reg)
        {
            ApplicationUser User = new ApplicationUser() { UserName = Reg.UserName, Email = Reg.Email, EmailConfirmed = false, FechaDesactivacion = Reg.FechaDesactivacion, Estado = "AC" , Tercero = Reg.Tercero };
            IdentityResult result = manager.Create(User, Reg.Password);
            if (result.Succeeded)
            {
                Reg.UserId = User.Id;
                byaRpt.Error = false;
                byaRpt.Mensaje = string.Format("El usuario {0} fue creado satisfactoriamente!", User.UserName);
            }
            else
            {
                GetErrorResults(result);
            }
        }

        private void GetErrorResults(IdentityResult result)
        {
            byaRpt.Error = true;
            byaRpt.Mensaje = "";
            foreach (string error in result.Errors)
            {
                byaRpt.Mensaje = " " + error;
            }
        }

        public List<ModuloRolesConUrl> GetRoles(string UserName)
        {
            ApplicationDbContext _context = new ApplicationDbContext();
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));

            var user = userMgr.FindByName(UserName);

            RepMenu Rpmenu = new RepMenu();
            List<ModuloRolesConUrl> lm = new List<ModuloRolesConUrl>();
            List<Menu> lMenu = Rpmenu.LstMenu.OrderBy(t => t.Posicion).ToList();
            foreach (Menu menu in lMenu)
            {
                List<ModuloRolesConUrl> lmAux = menu.SubMenu.Where(t => t.MenuId != t.PadreId && t.Roles != null).OrderBy(t => t.Posicion)
                    .Select(t => new ModuloRolesConUrl
                    {
                        hasRol = (userMgr.IsInRole(user.Id, t.Roles)),
                        Modulo = t.Modulo,
                        Roles = t.Roles,
                        Titulo = t.Titulo,
                        Url = t.Url,
                        Prioridad = t.Prioridad,
                        Habilitado = t.Habilitado,
                    }).Distinct().ToList();
                lm.AddRange(lmAux.Where(x => x.hasRol).ToList());
            }
            return lm.OrderBy(X=>X.Prioridad).ToList();
        }
        public List<ModuloRoles> GetModulosPadres()
        {
            RepMenu Menu = new RepMenu();
            return Menu.LstMenu.Select(MappearMenuARoles()).ToList();
        }

        public List<ModuloRoles> GetModulos(string usuario, string modulo)
        {
            RepMenu Menu = new RepMenu();
            var user = manager.FindByName(usuario);
            var permiso = Menu.LstMenu.FirstOrDefault(x => x.Roles == modulo);
            List<ModuloRoles> permisos = permiso.SubMenu.Select(MappearMenuARoles()).ToList();
            foreach (var item in permisos)
            {
                bool tieneRol = (manager.IsInRole(user.Id, item.Roles));
                item.hasRol = tieneRol;
            }
            return permisos;
        }

        private static Func<Menu, ModuloRoles> MappearMenuARoles()
        {
            return x => new ModuloRoles()
            {
                Modulo = x.Modulo,
                Roles = x.Roles,
                Titulo = x.Titulo,
                parentid = "",
            };
        }

        public ByARpt ConfirmarEmailUsuario(string username)
        {
            ByARpt bya = new ByARpt();
            ApplicationUser user = manager.FindByName(username);
            if (user != null)
            {
                if (!user.EmailConfirmed)
                {
                    
                    user.EmailConfirmed = true;
                    IdentityResult result=this.manager.Update(user);
                    StringBuilder bld = new StringBuilder();
                    foreach (string error in result.Errors)
                    {
                        bld.Append( " " + error);
                    }
                    bya.Mensaje = bld.ToString();
                    bya.Mensaje += "¡Se ha activado la cuenta satisfactoriamente!"+ result.Succeeded+" "+ username;
                }else
                {
                    bya.Mensaje = "¡La cuenta ya ha sido activada!";
                }
                
            }
            else
            {
                bya.Error = true;
                bya.Mensaje = "¡No se encontró el nombre de usuario:" + username;
            }
            return bya;
        }
        

        public IdentityResult ResetPass(ResetPasswordDto model)
        { 
            var provider = new DpapiDataProtectionProvider("Sample");
            manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("PasswordReset"));
            var user = manager.FindByEmail(model.Email);

            if (user == null)
            {
                return null;
            }
            else {
                if (user.Id == model.UserId)
                {
                    IdentityResult result = manager.ResetPassword(user.Id.ToString(), model.Code, model.Password);
                    return result;
                }
                else
                {
                    return null;
                }
            }
            
        }

        public async Task<IdentityResult> ForgotPassword(string email, string urlBefore)
        {
            var provider = new DpapiDataProtectionProvider("Sample");
            manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("PasswordReset"));
            var user = manager.FindByEmail(email);
            if(user != null)
            {
                await EnviarCorreoDeConfirmacion(new Usuario() { UserId = user.Id, UserName = user.UserName,Email=user.Email }, urlBefore, "forgot");
            }
            return IdentityResult.Success;
        }


        public Task<IdentityResult> ConfirmedEmail(string userId, string code) {
            return manager.ConfirmEmailAsync(userId, code);
        }

        public async Task<ByARpt> ReSendConfirmationEmail(string username, string URL_CONFIRMAR)
        {
            ApplicationUser apsus = manager.FindByName(username);
            if (apsus != null)
            {
                var correo = "";
                correo = apsus.Email;
                if (!apsus.EmailConfirmed)
                {
                    await EnviarCorreoDeConfirmacion(new Usuario() { UserId = apsus.Id, UserName = apsus.UserName ,Email=apsus.Email }, URL_CONFIRMAR, "confirm");
                    byaRpt.Mensaje = "Se le ha reenviado el correo de confirmación al siguiente email: " + correo;
                    byaRpt.Error = false;
                }
                else
                {
                    byaRpt.Mensaje = "Ya el correo de este usuario ha sido confirmado, no se renviara el mensaje de confirmación.";
                    byaRpt.Error = true;
                }
            }
            else
            {
                byaRpt.Mensaje = "No se encontró el usuario al que se le va a reenviar el correo.";
                byaRpt.Error = true;
            }

            return byaRpt;
        }
        

        public bool EsUsuario(string username)
        {
            ApplicationUser usr = manager.FindByName(username);
            
            if (usr == null)
            {
                return false;
            }
            else {
                return true;
            }
        }
        

        public ApplicationUser FindBy(string username)
        {
            return manager.FindByName(username);
        }

        public IdentityUser Validar(string username, string password)
        {
            ApplicationUser user = manager.Find(username, password);
            return user;
        }
        

        public string Cambio_Clave(ResetPasswordDto Reset)
        {
            ByARpt _byaRpt = new ByARpt();
            if (Reset.RePassword != Reset.NewPassword)
            {
                _byaRpt.Mensaje = "Las contraseña nueva y de confirmación deben ser iguales";
                _byaRpt.Error = true;
            }
            if(string.IsNullOrEmpty(Reset.Password.ToString()) || string.IsNullOrWhiteSpace(Reset.Password.ToString()))
            {
                _byaRpt.Mensaje = "La contraseña nueva no puede estar vacia";
                _byaRpt.Error = true;
            }
            try
            {
                var user = manager.FindByName(Reset.UserId);
                IdentityResult result = manager.ChangePassword(user.Id, Reset.CurrentPassword, Reset.NewPassword);
                if (result.Succeeded)
                {
                    _byaRpt.Mensaje = "Se realizó el cambio de contraseña";
                    _byaRpt.Error = false;
                }
                else
                {
                    _byaRpt.Mensaje = result.Errors.FirstOrDefault();
                    _byaRpt.Error = true;
                }
                
            }
            catch (Exception ex)
            {
                _byaRpt.Mensaje = "Error de App:" + ex.Message;
                _byaRpt.Error = true;
            }
            return _byaRpt;

        }
        
    }
    
}