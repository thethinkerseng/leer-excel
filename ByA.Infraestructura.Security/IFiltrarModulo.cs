﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security
{
    public interface IFiltrarModulo
    {
        List<Menu> Filtrar(List<Menu> LstMenu);
    }
}
