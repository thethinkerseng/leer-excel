﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security
{
    public class ModuloRoles
    {
        public int id { get; set; }
        public string parentid { get; set; }
        public bool hasRol { get; set; }
        public string Modulo { get; set; }
        public string Titulo { get; set; }
        public string Roles { get; set; }
    }

    public class ModuloRolesConUrl
    {
        public bool hasRol { get; set; }
        public string Modulo { get; set; }
        public string Titulo { get; set; }
        public string Roles { get; set; }
        public string Url { get; set; }
        public int Prioridad { get; set; }
        public int Habilitado { get; set; }
    }
}
