namespace ByA.Infraestructura.Security.Migrations
{
    using InicializarDatos;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ByA.Infraestructura.Security.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ByA.Infraestructura.Security.ApplicationDbContext";
        }

        protected override void Seed(ByA.Infraestructura.Security.ApplicationDbContext context)
        {
            if (context.Users.Count() <= 1)
            {
                InicializarSecurity.Seed(context);
            }
        }
    }
}
