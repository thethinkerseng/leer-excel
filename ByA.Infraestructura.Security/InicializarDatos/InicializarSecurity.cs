﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using App.Infraestructura.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security.InicializarDatos
{
    public static class InicializarSecurity
    {
        private static ApplicationDbContext _context;
        public static void Seed()
        {
            _context = new ApplicationDbContext();
            new Logger().Info("Inicializar Roles");
            InicializarRoles();
            InicializarUsuarios();
        }
        public static void Seed(ApplicationDbContext context)
        {
            _context = context;
            InicializarRoles();
            InicializarUsuarios();
        }
        public static void  InicializarRoles()
        {
            RoleActions roleActions = new RoleActions();
            roleActions.AddUserAndRole();
            GenRoles gr = new GenRoles();
            gr.GenerarRolesA();
        }
        public static void InicializarUsuarios()
        {
            List<UsuarioSeed> Usuarios;
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
            Usuarios=ListUsuarios();
            foreach (UsuarioSeed user in Usuarios)
            {
                IdentityResult IdUserResult;
                var appUser = new ApplicationUser()
                {
                    UserName =user.Username,
                    Email = user.Email,
                    EmailConfirmed = true,
                    FechaDesactivacion = ByADateTime.Now,
                    Estado = "AC",
                    Tercero=user.NombreCompleto,
                };
                IdUserResult = userMgr.Create(appUser, user.Password);
                new Logger().Info($"usuario: {user.Identificacion} y password {user.Password}");
                if (IdUserResult.Succeeded)
                {
                    foreach (string rol in user.Rol)
                    {
                        userMgr.AddToRole(appUser.Id, rol);
                    }
                }
            }
            
        }
        public static List<UsuarioSeed> ListUsuarios()
        {
            const string PASSWORD = "usuario2018.";
            List<UsuarioSeed> Users =new List<UsuarioSeed>()
            {
                new UsuarioSeed()
                {
                    Email="byasoporte@gmail.com",
                    Password= PASSWORD,
                    Username="usuario",
                    Rol=new List<string>()
                    {
                        "ARCHIVO_Importar",
                        "ARCHIVO_Consulta",
                    },
                    NombreContacto ="Usuario",
                    PrimerApellido="",
                    PrimerNombre="Usuario",
                    SegundoApellido="",
                    SegundoNombre="",
                    Identificacion="00000000"
                }
            };
            return Users;
        }
        public class UsuarioSeed
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public List<string> Rol { get; set; }
            public string PrimerNombre { get; set; }
            public string SegundoNombre { get; set; }
            public string PrimerApellido { get; set; }
            public string SegundoApellido { get; set; }
            public string NombreContacto { get; set; }
            public string NombreCompleto {
                get
                {
                    return (PrimerNombre != null ? PrimerNombre : "") + " " +
                           (SegundoNombre != null ? SegundoNombre : "") + " " +
                           (PrimerApellido != null ? PrimerApellido : "") + " " +
                           (SegundoApellido != null ? SegundoApellido : "");
                }
            }
            public string Identificacion { get; set; }
        }
    }
}
