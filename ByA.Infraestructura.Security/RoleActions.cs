﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using App.Infraestructura.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security
{
    public class RoleActions
    {
        public void AddUserAndRole()
        {
            // Access the application context and create result variables.
            ApplicationDbContext context = new ApplicationDbContext();
            IdentityResult IdUserResult;

            // Create a RoleStore object by using the ApplicationDbContext object. 
            // The RoleStore is only allowed to contain IdentityRole objects.
            var roleStore = new RoleStore<IdentityRole>(context);

            // Create a RoleManager object that is only allowed to contain IdentityRole objects.
            // When creating the RoleManager object, you pass in (as a parameter) a new RoleStore object. 
            var roleMgr = new RoleManager<IdentityRole>(roleStore);

            // Then, you create the "canEdit" role if it doesn't already exist.

             if (!roleMgr.RoleExists("admin"))
            {
                roleMgr.Create(new IdentityRole { Name = "admin" });
            }

            // Create a UserManager object based on the UserStore object and the ApplicationDbContext  
            // object. Note that you can create new objects and use them as parameters in
            // a single line of code, rather than using multiple lines of code, as you did
            // for the RoleManager object.
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var appUser = new ApplicationUser()
            {
                UserName = "admin",
                Email = "byasoporte@gmail.com",
                EmailConfirmed = true,
                FechaDesactivacion = ByADateTime.Now.AddYears(1),
                Estado = "AC",
            };
            IdUserResult = userMgr.Create(appUser, "Admin2018.");

            // If the new "Admin" user was successfully created, 
            // add the "Admin" user to the "Administrator" role. 
            if (IdUserResult.Succeeded)
            {
                Logger("No se creo el usuario admin.");
                IdUserResult = userMgr.AddToRole(appUser.Id, "admin");
                if (!IdUserResult.Succeeded)
                {
                    // Handle the error condition if there's a problem adding the user to the role.
                    Logger("No se agregaron Roles Admin");
                }
            }
            else
            {
                // Handle the error condition if there's a problem creating the new user. 
                Logger("No se creo el usuario admin");
            }
        }

        public void Logger(string mensajes) {
            new Logger().Info("No se creo el usuario admin");
        }        
    }
}
