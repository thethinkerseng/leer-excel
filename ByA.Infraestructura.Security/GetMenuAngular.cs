﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByA.Infraestructura.Security
{
    public class GetMenuAngular
    {
        public List<Menu> getOpciones(string usuario)
        {
            
            var userStore = new UserStore<IdentityUser>();
            var userMgr = new UserManager<IdentityUser>(userStore);
            var user = userMgr.FindByName(usuario);
            List<Menu> lt = new List<Menu>();
            if (user == null) return lt;
            RepMenu db = new RepMenu();
            lt = db.LstMenu.ToList();
            foreach (var item in lt) {
                item.SubMenu = item.SubMenu.Where(t => (userMgr.IsInRole(user.Id, t.Roles))).ToList();
            }
            return lt;
        }
        
        
        public List<string> getUrlxRoles(string usuario)
        {
            var userStore = new UserStore<IdentityUser>();
            var userMgr = new UserManager<IdentityUser>(userStore);
            var user = userMgr.FindByName(usuario);

            List<Menu> lt;
            RepMenu db = new RepMenu();
            lt = db.LstMenu.ToList();

            List<string> ltUrl = new List<string>();
            foreach (var item in lt)
            {
                item.SubMenu = item.SubMenu.Where(t => (userMgr.IsInRole(user.Id, t.Roles))).ToList();
                foreach(var itemSubMenu in item.SubMenu){
                    ltUrl.Add(itemSubMenu.Url);
                }
            }

            return ltUrl;
        }
        public List<string> getRoles(string usuario)
        {
            var userStore = new UserStore<IdentityUser>();
            var userMgr = new UserManager<IdentityUser>(userStore);
            var user = userMgr.FindByName(usuario);
            var roles = userMgr.GetRoles(user.Id).ToList();
            return roles;
        }
        
    }
}
