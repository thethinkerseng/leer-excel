﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ByA.Infraestructura.Security
{
    public class Menu
    {
        public Menu()
        {
            Prioridad = 100;
        }
        public string MenuId { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string PadreId { get; set; }
        public int Posicion { get; set; }
        public int Prioridad { get; set; }
        public string Icono { get; set; }
        public int Habilitado { get; set; }
        public string Url { get; set; }
        public string uiUrl { get; set; }
        public string Modulo { get; set; }
        public string Target { get; set; }
        public string Roles { get; set; }
        public List<Menu> SubMenu { get; set; }
    }
    public class RepMenu
    {
        public List<Menu> LstMenu { get; set; }
        public RepMenu()
        {
            LstMenu = new List<Menu>();
            InicializarModulos();
        }
        private void InicializarModulos()
        {
            Archivo();
            DatosBasicos();
            Seguridad();
        }
        private void Archivo()
        {
            LstMenu.Add(new Menu
            {
                MenuId = "01",
                Titulo = "ARCHIVOS",
                Descripcion = "Administrar archivos",
                Habilitado = 1,
                Icono = "",
                Modulo = "ARCHIVO",
                PadreId = "07",
                Posicion = 1,
                Roles = "ARCHIVO",
                Target = "_self",
                Prioridad = 10,
                SubMenu = new List<Menu>
                   {
                    new Menu
                           {
                               MenuId = "0201",
                               Titulo = "Consulta",
                               Descripcion = "",
                               Habilitado = 1,
                               Icono = "",
                               Modulo = "ARCHIVO",
                               PadreId = "07",
                               Posicion = 1,
                               Roles = "ARCHIVO_Consulta",
                               Target = "_self",
                               Url="/#/app/archivo/excel/consulta",
                               uiUrl = "app.general",
                               Prioridad = 41,
                           },
                    new Menu
                           {
                               MenuId = "0202",
                               Titulo = "Importar archivo",
                               Descripcion = "",
                               Habilitado = 0,
                               Icono = "",
                               Modulo = "ARCHIVO",
                               PadreId = "07",
                               Posicion = 1,
                               Roles = "ARCHIVO_Importar",
                               Target = "_self",
                               Url="/#/app/archivo/excel/importar",
                               uiUrl = "app.general"
                           }

                }
            });
        }
        private void DatosBasicos()
        {
            LstMenu.Add(new Menu
            {
                MenuId = "07",
                Titulo = "Datos básicos",
                Descripcion = "Administrar entidades básicas",
                Habilitado = 1,
                Icono = "",
                Modulo = "DATOSBASICOS",
                PadreId = "07",
                Posicion = 1,
                Roles = "DATOSBASICOS",
                Target = "_self",
                Prioridad = 40,
                SubMenu = new List<Menu>
                   {
                    new Menu
                           {
                               MenuId = "0701",
                               Titulo = "Gestión de terceros",
                               Descripcion = "",
                               Habilitado = 1,
                               Icono = "",
                               Modulo = "DATOSBASICOS",
                               PadreId = "07",
                               Posicion = 1,
                               Roles = "DATOSBASICOS_GestionTerceros",
                               Target = "_self",
                               Url="/#/app/datos_basicos/terceros/gestion_terceros",
                               uiUrl = "app.general",
                               Prioridad = 41,
                           },
                    new Menu
                           {
                               MenuId = "0702",
                               Titulo = "Registro de terceros",
                               Descripcion = "",
                               Habilitado = 0,
                               Icono = "",
                               Modulo = "DATOSBASICOS",
                               PadreId = "07",
                               Posicion = 1,
                               Roles = "DATOSBASICOS_RegistroTerceros",
                               Target = "_self",
                               Url="/#/app/datos_basicos/terceros/registro_terceros",
                               uiUrl = "app.general"
                           },
                }
            });
        }
        private void Seguridad()
        {
            LstMenu.Add(new Menu
            {
                MenuId = "08",
                Titulo = "Seguridad",
                Descripcion = "Administrar usuarios y roles",
                Habilitado = 1,
                Icono = "",
                Modulo = "SEGURIDAD",
                PadreId = "07",
                Posicion = 1,
                Roles = "SEGURIDAD",
                Target = "_self",
                SubMenu = new List<Menu>
                   {
                    new Menu
                           {
                               MenuId = "0801",
                               Titulo = "Usuarios",
                               Descripcion = "",
                               Habilitado = 1,
                               Icono = "",
                               Modulo = "SEGURIDAD",
                               PadreId = "08",
                               Posicion = 1,
                               Roles = "SEGURIDAD_GestionUsuarios",
                               Target = "_self",
                               Url="/#/app/seguridad/usuarios/gestion_usuarios",
                               uiUrl = "app.general",
                               Prioridad = 51,
                           },
                    new Menu
                           {
                               MenuId = "0802",
                               Titulo = "Roles",
                               Descripcion = "",
                               Habilitado = 0,
                               Icono = "",
                               Modulo = "SEGURIDAD",
                               PadreId = "08",
                               Posicion = 1,
                               Roles = "SEGURIDAD_RegistroRolesUsuario",
                               Target = "_self",
                               Url="/#/app/seguridad/usuarios/registro_roles_usuarios",
                               uiUrl = "app.general"
                           },
                    new Menu
                           {
                               MenuId = "0804",
                               Titulo = "Auditoria",
                               Descripcion = "",
                               Habilitado = 0,
                               Icono = "",
                               Modulo = "SEGURIDAD",
                               PadreId = "08",
                               Posicion = 1,
                               Roles = "SEGURIDAD_ConsultaAuditoria",
                               Target = "_self",
                               Url="/#/app/seguridad/consulta/auditoria",
                               uiUrl = "app.general",
                               Prioridad = 52,
                           },
                     new Menu
                           {
                               MenuId = "0805",
                               Titulo = "Servicios Externos",
                               Descripcion = "",
                               Habilitado = 0,
                               Icono = "",
                               Modulo = "SEGURIDAD",
                               PadreId = "08",
                               Posicion = 1,
                               Roles = "SEGURIDAD_ServicioExterno",
                               Target = "_self",
                               Url="/#/app/datos_basicos/general/en_construccion",
                               uiUrl = "app.general",
                               Prioridad = 42,
                           },
                }
            });
        }
    }
}
