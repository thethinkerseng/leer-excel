﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ByA;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;


namespace ByA.Infraestructura.Security
{
    public class GesMenuAdapter
    {

        public ByARpt byaRpt { get; set; }

        public List<DataTree> getOpciones() { 
               
                List<DataTree> lt;
                RepMenu db= new RepMenu();
                lt = db.LstMenu
                    .Select(t => new DataTree
                {
                    id = t.MenuId,
                    text = t.Titulo,
                    value = new ValueTree { icono = t.Icono, descripcion = t.Descripcion, target = t.Target, url = t.Url, roles = t.Roles },
                    parentid = t.MenuId == t.PadreId ? "-1" : t.PadreId,
                    roles = t.Roles
                }
                ).ToList();
                return lt;
        }
    }

    public class ValueTree
    {

        public string target { get; set; }
        public string url { get; set; }
        public string icono { get; set; }
        public string descripcion { get; set; }
        public string roles { get; set; }


    }
    
    public class DataTree
    {
        public string id { get; set; }
        public string parentid { get; set; }
        public string text { get; set; }
        public string roles { get; set; }
        public ValueTree value { get; set; }

    }
}
